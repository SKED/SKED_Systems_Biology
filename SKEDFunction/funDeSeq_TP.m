function tReport = funDeSeq_TP(strucData,g1,g2,sG1Desc,sG2Desc,nCount,sExp, sSpecies,sTPDesc,sPrint)
           
    if nargin == 9 || strcmp(sPrint, 'on')
        sPrint = 'on';
    else
        sPrint = 'off';
    end
        
    mG1 = [];
    mG2 = [];
    % populate matricies for mG1 and mG2 based on selected time points
    % that were defined by g1 and g2
    
    iNumSub = size(strucData.cSubject,2);
    cLabelsG1 = {};
    cLabelsG2 = {};
           
    for i = 1:length(g1)
        nSub = rem(i,iNumSub);
        if nSub == 0
            nSub = iNumSub;
        end
            if isempty(g1{i}) == 0
                    mG1 = [mG1 strucData.cRawCounts{nSub}(:,g1{i})];
                    cTPName = strcat(sSpecies , strucData.cSubject{nSub}, {'_TP'} ,num2str(g1{i}), {'_BL'});
                    cLabelsG1 = [cLabelsG1 cTPName];   
            end
    end
       
    for i = 1:length(g2)
        nSub = rem(i,iNumSub);
        if nSub == 0
            nSub = iNumSub;
        end

        if isempty(g2{i}) == 0
            mG2 = [mG2 strucData.cRawCounts{nSub}(:,g2{i})]; 
            cTPName = strcat(sSpecies , strucData.cSubject{nSub}, {'_TP'} ,num2str(g2{i}), {'_'}, sTPDesc);
            cLabelsG2 = [cLabelsG2 cTPName];
        end
    end
    
    
    
   % mG = [normCountsmG1 normCountsmG2];
    mG = [mG1 mG2];
     %normalize using normalized data from both groups 
    pseudoRefSample = geomean(mG,2);
    nz = pseudoRefSample > 0;
    ratios = bsxfun(@rdivide,mG(nz,:),pseudoRefSample(nz));
    sizeFactors = median(ratios,1);

    normCountsmG = bsxfun(@rdivide,mG,sizeFactors);
    
    
     lowCountThreshold = nCount;
     
     lowCountGenesmG = all(normCountsmG < lowCountThreshold, 2);

    mG1norm = normCountsmG(:,1:(0.5*size(normCountsmG,2)) );
    mG2norm = normCountsmG(:,(0.5*size(normCountsmG,2))+1:end);
    
    meanG1 = mean(mG1norm,2);
    meanG2 = mean(mG2norm,2);
    
    meanBase = (meanG1 + meanG2) / 2;
    foldChange = meanG2 ./ meanG1;
    log2FC = log2(foldChange);
    
    cGeneNames = regexprep(strucData.VarNames{1},'^x_', '');
    cGeneNames = regexprep(cGeneNames,'_$', '');
       
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %Inferring Differential Expression with a Negative Bionomial Model
    
    %Uncomment the following to see the variance graphs
%     %tIdentity = nbintest(counts(:,treated),counts(:,untreated),'VarianceLink','Identity');
%     tIdentity = nbintest(mG1,mG2,'VarianceLink','Identity');
%     %h2 = figure()
%     h = plotVarianceLink(tIdentity);
% 
%     % set custom title
%     h(1).Title.String = 'Variance Link on Group 1';
%     h(2).Title.String = 'Variance Link on Group 2';
%     
%     %tConstant = nbintest(counts(:,treated),counts(:,untreated),'VarianceLink','Constant');
%     tConstant = nbintest(mG1,mG2,'VarianceLink','Constant');
%     h3 = figure
%     h = plotVarianceLink(tConstant);
% 
%     % set custom title
%     h(1).Title.String = 'Variance Link on Group 1';
%     h(2).Title.String = 'Variance Link on Group 2';
%     
%     %tLocal = nbintest(counts(:,treated),counts(:,untreated),'VarianceLink','LocalRegression');
%     tLocal = nbintest(mG1,mG2,'VarianceLink','LocalRegression');
%     h4 = plotVarianceLink(tLocal);
% 
%     % set custom title
%     h4(1).Title.String = 'Variance Link on Group 1';
%     h4(2).Title.String = 'Variance Link on Group 2';
%     
%     h5 = plotVarianceLink(tLocal,'compare',true);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    tLocal = nbintest(mG2norm,mG1norm,'VarianceLink','LocalRegression'); 
    %tLocal = nbintest(mG2,mG1,'VarianceLink','LocalRegression');
    %         h = plotVarianceLink(tLocal);
    % % set custom title
    % h(1).Title.String = 'Variance Link on Relapse Samples';
    % h(2).Title.String = 'Variance Link on Parasitemia Samples';
        
        pValue = tLocal.pValue;
        [mFDR qValue] = mafdr(pValue); 
        FoldChange = mean(mG2norm,2)./mean(mG1norm,2);
        %GeneNames = strucData.VarNames{1};
        %tReport = table(GeneNames,qValue,FoldChange,lowCountGenesmG); 
        %tReport = table(GeneNames,qValue,FoldChange);
        %tReportLowCount = table(GeneNames(~lowCountGenesmG),qValue(~lowCountGenesmG),FoldChange(~lowCountGenesmG)); 
%         tReport = table(GeneNames(~lowCountGenesmG),qValue(~lowCountGenesmG),FoldChange(~lowCountGenesmG));
%         tReport.Properties.VariableNames = {'GeneNames','qValue','FoldChange'};
        % create table with statistics about each gene
        geneTable = table(meanBase,meanG2,meanG1,FoldChange,log2FC);
        geneTable.Properties.RowNames = cGeneNames; 
        
        summary(geneTable)
        
        % create table with statistics about each gene
         %geneTableWithoutLowCounts =  geneTable(~lowCountGenesmG); 
        geneTableWithoutLowCounts =  table(meanBase(~lowCountGenesmG),meanG2(~lowCountGenesmG),meanG1(~lowCountGenesmG),FoldChange(~lowCountGenesmG),log2FC(~lowCountGenesmG));
        geneTableWithoutLowCounts.Properties.RowNames = cGeneNames(~lowCountGenesmG); 
        geneTableWithoutLowCounts.Properties.VariableNames = {'meanBase', 'meanG2','meanG1','FoldChange','log2FC'};

        if strcmp(sPrint, 'on')
            mairplot(meanG2,meanG1,'Labels',cGeneNames,'Type','MA');
            set(get(gca,'Xlabel'),'String','mean of normalized counts')
            set(get(gca,'Ylabel'),'String','log2(fold change)')

             mairplot(meanG2(~lowCountGenesmG),meanG1(~lowCountGenesmG),'Labels',cGeneNames(~lowCountGenesmG),'Type','MA');
            set(get(gca,'Xlabel'),'String','mean of normalized counts')
            set(get(gca,'Ylabel'),'String','log2(fold change)')

            figure('units','normalized','outerposition',[0 0 1 1]);
            histogram(tLocal.pValue,100)
            title(['Histogram of P Values for ', sExp,]);

            figure('units','normalized','outerposition',[0 0 1 1]);
            histogram(tLocal.pValue(~lowCountGenesmG),100)
            title(['Histogram of P Values for ', sExp, ' without low count genes']);

            figure('units','normalized','outerposition',[0 0 1 1]);
            histogram(qValue(~lowCountGenesmG),100)
            title(['Histogram of Q Values for ', sExp,' without low count genes']);

            figure('units','normalized','outerposition',[0 0 1 1]);
            nlog2MeanG1 = log2(mean(mG1norm,2));
            nlog2FoldChange = log2(FoldChange);
            scatter(nlog2MeanG1(~lowCountGenesmG),nlog2FoldChange(~lowCountGenesmG),3,qValue(~lowCountGenesmG),'o')
            colormap(flipud(cool(256)))
            colorbar;
            ylabel('log2(Fold Change)')
            xlabel('log2(Mean of normalized counts)')
            title(['Fold change by FDR with low count genes removed for ', sExp])
        end
        
        %Multiple Testing and Adjusted P-values
        % compute the adjusted P-values (BH correction)
        padj = mafdr(tLocal.pValue,'BHFDR',true);
        % add to the existing table
        geneTableWithoutLowCounts.pvalue = tLocal.pValue(~lowCountGenesmG);
        geneTableWithoutLowCounts.padj = padj(~lowCountGenesmG);
        % create a table with significant genes
        sig = geneTableWithoutLowCounts.padj < 0.1;
        geneTableSigWithoutLowCounts = geneTableWithoutLowCounts(sig,:);
        %geneTableSig = geneTable.padj(sig,:);
        geneTableSigWithoutLowCounts = sortrows(geneTableSigWithoutLowCounts,'padj');
        %geneTableSig = sortrows(geneTableSig,'descend');
        
        tReport = geneTableSigWithoutLowCounts;
%         tReport.Properties.VariableNames = {'GeneNames','qValue','FoldChange'};   mG1 = [];
    
        
        numberSigGenes = size(geneTableSigWithoutLowCounts,1);    
        
        % find up-regulated genes
        up = geneTableSigWithoutLowCounts.log2FC > 1;
        upGenes = sortrows(geneTableSigWithoutLowCounts(up,:),'log2FC','descend');
        numberSigGenesUp = sum(up);
        
        % display the top 10 up-regulated genes
        if height(upGenes) < 10;
            top10GenesUp = upGenes(:,:)
        else         
            top10GenesUp = upGenes(1:10,:)
        end
        % find down-regulated genes
        down = geneTableSigWithoutLowCounts.log2FC < -1;
        downGenes = sortrows(geneTableSigWithoutLowCounts(down,:),'log2FC','ascend');
        numberSigGenesDown = sum(down);
        
        % find top 10 down-regulated genes
        if height(downGenes) < 10;
             top10GenesDown = downGenes(:,:)
        else         
            top10GenesDown = downGenes(1:10,:)
        end
        if strcmp(sPrint, 'on')
            figure
            scatter(log2(geneTableSigWithoutLowCounts.meanBase),geneTableSigWithoutLowCounts.log2FC,3,geneTableSigWithoutLowCounts.padj,'o')
            colormap(flipud(cool(256)))
            colorbar;
            ylabel('log2(Fold Change)')
            xlabel('log2(Mean of normalized counts)')
            title(['Fold change by FDR between ', sG1Desc,' and ', sG2Desc,' in ', sExp, ' without low count genes' ])
        end
           
end