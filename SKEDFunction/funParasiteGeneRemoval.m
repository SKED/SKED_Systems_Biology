function strucData = funParasiteGeneRemoval(strucData,sIdentifier)  

      idx = strncmp(strucData.VarNames{1},sIdentifier,length(sIdentifier)); 
      for i = 1:length(strucData.cRawCounts)
          strucData.cRawCounts{i} =  strucData.cRawCounts{i}(idx == 0,:);
           strucData.VarNames{i} = strucData.VarNames{i}(idx == 0,:); 
      end


end