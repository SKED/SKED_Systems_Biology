function funMANOVAPlot(strucData,cLabel)
    Data = [];
    Label = {};
    for i = 1:size(strucData.cRawCounts,2)
        Data = [Data strucData.cRawCounts{i}];
        for j = 1:size(strucData.cRawCounts{i},2)
            Label = [Label,cLabel{i}];
        end
    end
    nanIdx = sum(isnan(Data),2);
    Data = Data(nanIdx < 0.75*size(Data,2),:);
    nanIdx = sum(isnan(Data),1);
    [d,p,stats] = manova1(Data',Label);
    c1 = stats.canon(:,1);
    c2 = stats.canon(:,2);
    h = figure('units','normalized','outerposition',[0 0 1 1]);
    gscatter(c2,c1,Label')
    xlabel('Cannonical Variable 1');
    ylabel('Cannonical Variable 2');
    set(gca,'FontSize',22,'FontWeight','bold');
end