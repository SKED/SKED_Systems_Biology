function tProtocol = funGetProtocol(cStructures,sExp,sSubject,sDataType)
    sDataType = strrep(sDataType,'_',' ');
    
    for i = 1:length(cStructures)
        if strcmp(cStructures{i}.Data{1,1}.ExperimentID,sExp)
            idx = i; 
        end
    end
    
    strucExp = cStructures{idx}; 
    
    cSubject = {};
    cDataType = {}; 
    
    for i = 1:size(strucExp.Data,1)
            cSubject{i} = strucExp.Data{i,1}.Name;

        for j = 1:size(strucExp.Data,2)
            cDataType{i,j} = strucExp.Data{i,j}.DataType;
            cDataProtocol{i,j} = strucExp.Data{i,j}.Metadata.PROTOCOL{1};
        end
    end
    
    
    
    j = strcmp(cSubject,sSubject);
    
    if strcmp(sSubject,'Summary')
        tProtocol = unique(cDataProtocol(strcmp(cDataType,sDataType)));
    else
        tProtocol = unique(cDataProtocol(j,strcmp(cDataType(j,:),sDataType)));
        tProtocol = tProtocol';
    end

    
    tProtocol = cell2table(tProtocol,...
    'VariableNames',{[sSubject '_' strrep(sDataType,' ','_') '_' 'Protocol_Names' ]});
end