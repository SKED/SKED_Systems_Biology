function h = funPlotVariable(cStructures,sExp,sSubject,cVariable)
    sInterpreter = 'none';
    for i = 1:length(cStructures)
        if strcmp(cStructures{i}.Data{1,1}.ExperimentID,sExp)
            idx = i; 
        end
    end
    strucExp = cStructures{idx}; 
    cSubject = {};
    cDataType = {}; 
    for i = 1:size(strucExp.Data,1)
            cSubject{i} = strucExp.Data{i,1}.Name;
    end
    h = figure('units','normalized','outerposition',[0 0 1 1]);
    sFigureNmbr = ['Figure ' num2str(h.Number) '. '];
    if strcmp(sSubject,'All') == 0
        idxSubject = find(strcmp(cSubject,sSubject));
        if size(cVariable,1) == 1
            [x,t,s] = strucExp.retrieveProtocolData(idxSubject,cVariable{1,1}); 

            for i = 1:length(s)
                s{i} = s{i}(3:end-1);
            end
            idx = strcmp(s,cVariable{1,2});
            T = datenum(t);
            plot(T, x(idx,:));
            ylabel(cVariable{1,2},'Interpreter', sInterpreter);
            title([sFigureNmbr sSubject ' ' cVariable{1,2}],'Interpreter', sInterpreter)
            funFormatTimeTick(t,2); 

        elseif size(cVariable,1) == 2
            [x,t,s] = strucExp.retrieveProtocolData(idxSubject,cVariable{1,1}); 

            for i = 1:length(s)
                s{i} = s{i}(3:end-1);
            end
            idx1 = strcmp(s,cVariable{1,2});
            T = datenum(t);
            yyaxis right; 

            plot(T, x(idx,:));
            ylabel(cVariable{1,2},'Interpreter', sInterpreter);
            title([sFigureNmbr sSubject ' ' cVariable{1,2} '/' cVariable{2,2}],'Interpreter', sInterpreter)

            yyaxis left; 

            [x,t,s] = strucExp.retrieveProtocolData(idxSubject,cVariable{2,1}); 

            for i = 1:length(s)
                s{i} = s{i}(3:end-1);
            end

            idx = strcmp(s,cVariable{2,2});

            T = datenum(t);
            plot(T, x(idx,:));
            ylabel(cVariable{2,2},'Interpreter', sInterpreter);
            funFormatTimeTick(t,2); 
        else
            disp('Too many variables. Cannot display. ');
        end
    elseif strcmp(sSubject,'All')
        for k = 1:length(cSubject)
            [x,t,s] = strucExp.retrieveProtocolData(k,cVariable{1,1}); 
            
            for i = 1:length(s)
                s{i} = s{i}(3:end-1);
            end
            idx = strcmp(s,cVariable{1,2});
            T = datenum(t);
            plot(T, x(idx,:));
            hold on 
        end
            ylabel(cVariable{1,2},'Interpreter', sInterpreter);
            title([sFigureNmbr 'All Subjects' ' ' cVariable{1,2}],'Interpreter', sInterpreter);
            legend(cSubject);
            funFormatTimeTick(t,2);
    end
end