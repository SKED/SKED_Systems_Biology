function [tDataNorm, tFC ] = funDataNormFC(strucData,g1,g2,sG1Desc,sG2Desc,nCount,sExp, sSpecies,sTPDesc)
    
    mG1 = [];
    mG2 = [];
    % populate matricies for mG1 and mG2 based on selected time points
    % that were defined by g1 and g2
    
    iNumSub = size(strucData.cSubject,2);
    cLabelsG1 = {};
    cLabelsG2 = {};
           
    for i = 1:length(g1)
        nSub = rem(i,iNumSub);
        if nSub == 0
            nSub = iNumSub;
        end
            if isempty(g1{i}) == 0
                    mG1 = [mG1 strucData.cRawCounts{nSub}(:,g1{i})];
                    cTPName = strcat(sSpecies, sExp, strucData.cSubject{nSub}, {'_T'} ,num2str(g1{i}), {'_BL'});
                    cLabelsG1 = [cLabelsG1 cTPName];   
            end
    end
       
    for i = 1:length(g2)
        nSub = rem(i,iNumSub);
        if nSub == 0
            nSub = iNumSub;
        end

        if isempty(g2{i}) == 0
            mG2 = [mG2 strucData.cRawCounts{nSub}(:,g2{i})]; 
            cTPName = strcat(sSpecies , sExp, strucData.cSubject{nSub}, {'_T'} ,num2str(g2{i}), {'_'}, sTPDesc );
            cLabelsG2 = [cLabelsG2 cTPName];
        end
    end
    
     lowCountThreshold = nCount;
    
   % mG = [normCountsmG1 normCountsmG2];
    mG = [mG1 mG2];
     %normalize using normalized data from both groups 
    pseudoRefSample = geomean(mG,2);
    nz = pseudoRefSample > 0;
    ratios = bsxfun(@rdivide,mG(nz,:),pseudoRefSample(nz));
    sizeFactors = median(ratios,1);

    normCountsmG = bsxfun(@rdivide,mG,sizeFactors);
    
    figure('units','normalized','outerposition',[0 0 1 1]);
    subplot(2,1,1)
    maboxplot(log2(mG), 'orientation','horizontal','BoxPlot', {'Labels',[ cLabelsG1 cLabelsG2]  } )
    title( ['Raw read count for all subjects between ', sG1Desc,' and ', sG2Desc,' in ', sExp])
    ylabel('sample')
    xlabel('log2(counts)')

    subplot(2,1,2)
    maboxplot(log2(normCountsmG),'title',['Normalized read count for all subjects between ', sG1Desc,' and ', sG2Desc,' in ', sExp],'orientation','horizontal','BoxPlot', {'Labels',[ cLabelsG1 cLabelsG2]  })
    ylabel('sample')
    xlabel('log2(counts)')
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    lowCountGenesmG = all(normCountsmG < lowCountThreshold, 2);

    mG1norm = normCountsmG(:,1:(0.5*size(normCountsmG,2)) );
    mG2norm = normCountsmG(:,(0.5*size(normCountsmG,2))+1:end);
    
    meanG1 = mean(mG1norm,2);
    meanG2 = mean(mG2norm,2);
    
    meanBase = (meanG1 + meanG2) / 2;
    foldChange = meanG2 ./ meanG1;
    log2FC = log2(foldChange);
    
    % remove prefixes for genes

    cGeneNames = regexprep(strucData.VarNames{1},'^x_', '');
    cGeneNames = regexprep(cGeneNames,'_$', '');
    
    % the following is used to remove the prefix from genes from M
    % fasicularis , but there are ~483 (InfoCore) or ~392 (me) duplicated
    % genes
%      for i=1:length(cGeneNames)
%          %checks for genes that contain duplicates
%         if (contains(cGeneNames{i},'TRNA') | contains(cGeneNames{i},'RIOK3') | contains(cGeneNames{i},'CETN2') | contains(cGeneNames{i},'SLC33A1') | contains(cGeneNames{i},'PSMA8') | contains(cGeneNames{i},'RPA1')) == 0
%             cGeneNames{i} = regexprep(cGeneNames{i},'gene\d+_','');
%         else
%             cGeneNames{i} = cGeneNames{i};
%         end
%     end


  % save normalized data in a table
    tDataNorm = cell2table(num2cell(mG2norm));
    tDataNorm.Properties.VariableNames = cLabelsG2;
    tDataNorm.Properties.RowNames = cGeneNames;
    
    foldChange = mG2norm ./ mG1norm;
    
    tFC = cell2table(num2cell(foldChange));
    tFC.Properties.VariableNames = cLabelsG2;
    tFC.Properties.RowNames = cGeneNames;
    
%     % replace Nan with 0
%     idx = ismissing(tFC(:,:));
%     tFC{:,:}(idx) = 0;
%         idx = ismissing(tDataNorm(:,:));
%     tDataNorm{:,:}(idx) = 0;
%     
%     % replace Nan with 0
%     idx = isinf(tFC(:,:));
%     tFC{:,:}(idx) = 0;
%         idx = ismissing(tDataNorm(:,:));
%     tDataNorm{:,:}(idx) = 0;
%     
    
end