function cExperiments = funGetExperimentNames(cStructures)
    for i = 1:length(cStructures)
        cExperiments{i} = cStructures{i}.Data{1,1}.ExperimentID;
    end
end