function tVariable = funGetVariableNames(cStructures,sExp,sSubject,sProtocol)
    
    for i = 1:length(cStructures)
        if strcmp(cStructures{i}.Data{1,1}.ExperimentID,sExp)
            idx = i; 
        end
    end
    
    strucExp = cStructures{idx}; 
    cSubject = {};
    cDataType = {}; 
    
    for i = 1:size(strucExp.Data,1)
            cSubject{i} = strucExp.Data{i,1}.Name;
    end
    
    idxSubject = find(strcmp(cSubject,sSubject));
    [x,t,s] = strucExp.retrieveProtocolData(i,sProtocol); 

    for i = 1:length(s)
        s{i} = s{i}(3:end-1);
    end
    
    tVariable = cell2table(s,...
    'VariableNames',{[sSubject '_' sProtocol '_' 'Variables' ]});
end