function [pl n X,mRawDaily] = funGrassmannian(cMonkey)

%Animal Code - Database ID for E30
% RKy15 1403
% REd16	1406
%idx
% Animal Code - Database ID for E07
% 12C44��1407
% 12C53��1402
% H12C59�1404
% H12C50�1410
% H12C8� 1405
% 11C166�1409
% 11C131�1408
% 12C136�1401

% Animal Code - Database ID for E06
% RCl15 1916
% RIh16 1912
% RTe16 1915
% RUf16 1918
% cMonkey = {
%    'E07A', '1402';
%      'E07A', '1404';
%      'E07A', '1405';%
%      'E07A', '1407';
%      'E07A', '1408'; %
%      'E07A', '1409';%
%      'E07A', '1410';
%    'E07B', '1401'; 
%    'E07B', '1402'; 
%    'E07B', '1404'; 
%    'E07B', '1405'; 
%    'E07B', '1407'; 
%    'E07B', '1408'; 
%   'E07B', '1409'; 
%   'E06', '1918'; 
%    'E06', '1916'; 
%     'E06', '1915'; 
%     'E06', '1912';
%    'E30', '1403'; 
%    'E30', '1406'
%     };
%figure(2); clf; hold on; 
nFontSize = 24;
set(gca,'FontSize',nFontSize);
sLegend = {}; 
X = [];I = [];  
for i=1:size(cMonkey,1)
    try 
        [s, Y, idx, mRawDaily] = funWritheDaily(cMonkey{i,1}, cMonkey{i,2});
        sLegend = [sLegend, s];
        X = [X; Y];
        I = [I;  idx]; 
    catch err
        disp(err)
    end
end

%legend(sLegend); 
%columnlegend(4,sLegend,'NorthWest');
%figure(3); clf;
%X = [X(:,1:4),X(:,5:6)]; % remove higher order invariants
%X = (X - mean(X));
%X = (X - mean(X))./std(X);
% [V,D] = eig(X'*X); 
% Z = X*V(:,end-2:end);
% x = Z(:,end); 
% y = Z(:,end-1); 
% z = Z(:,end-2);
 sColor = ['b','g','r'];
% for i=1:3
%     idx = I == i; 
%     %plot3(x(idx), y(idx),z(idx), ['o' sColor(i)]); 
%     plot(x(idx), y(idx), ['o' sColor(i)]); 
%     hold on;
% end
% Start Grasmannian here
% figure(4); clf;

dummy={};
dummy2={};
idxGidxG = []; 

% 
% for i=1:3
%     idx=I==i;
%     for j=1:5 %nchoosek(size(X(idx),1),3)
%         dumMat=X(idx,:);
%         p=randperm(size(dumMat,1),2);
%         tempMat=[dumMat(p(1,1),:);dumMat(p(1,2),:)]'; 
%         [U,S,V]=svd(tempMat');
%         M=tempMat*U;
%         dummy{j}={[M(:,1)/S(1,1),M(:,2)/S(2,2)]};
%     end
%     dummy2{i}={dummy};
% end

% The following code uses columns 1,2,3,5,6,8 of X, while omitting columns 4 and 7.. 
% Reasons:1) Entries of the 4th column (ECG)  are identical; 2) Entries of the 7th column
%(Higherorder) are significantly larger than the values in the other columns. 
% This can be verified by implementing the following code:  X(idx, [1 2 3
% 5 6 8]) and  X(idx, [1 2 3 5 6 7 8]).
    %1 = Activity
    %2 = Blood pressure
    %3 = Temperature
    %4 = ECG
    %5 = Writhe
    %6 = AvgCrossNumbr
    %7 = Higherorder
    %8 = CumWrithe

for i=1:3
    idx=I==i;
    for j=1:500 %nchoosek(size(X(idx),1),3)
        dumMat=X(idx,[1 2 3 5 6 7]);
        p=randperm(size(dumMat,1),2); % check duplicates? 
        tempMat=[dumMat(p(1,1),:);dumMat(p(1,2),:)]'; 
        [U,S,V]=svd(tempMat');
        M=tempMat*U;
        dummy{j}={[M(:,1)/S(1,1),M(:,2)/S(2,2)]};
    end
    dummy2{i}={dummy};
end

dummy3={dummy2{1,1}{1,1}{1,:},dummy2{1,2}{1,1}{1,:},dummy2{1,3}{1,1}{1,:}};

n = size(dummy,2); 
for i=1:(n*3)
    for j=1:i
      MM=cell2mat(dummy3{1,i})'*cell2mat(dummy3{1,j});
      D(i,j)=sqrt(abs(2-trace(MM*MM')));  %chord distance
      %[U,S,V]=svd(MM);
      %theta1=acos(abs(S(1,1)));
      % theta2=acos(abs(S(2,2)));
      % D(i,j)=sqrt(theta1^2+theta2^2);
      %geodesic distance
      %D(i,j)=max(theta1,theta2);%max distance
    end
end

% a = rand(1500);
% b = triu(a) + triu(a,1)';
RR=diag(D);
BB=diag(RR);
ab=D-BB;
ab=(ab+ab')*1e3;

pl= cmdscale(ab,3);

 for i=1:3
    idx = (1:n)+(i-1)*n; 
  % plot3(x(idx), y(idx),z(idx), ['o' sColor(i)]); 
  % plot(x(idx), y(idx), ['o' sColor(i)]);
 %plot(pl(idx,1),pl(idx,2),['o' sColor(i)]); 
%plot3(pl(idx,1),pl(idx,2),pl(idx,3),['o' sColor(i)]);
%temp{i} = pl(idx,:);
 hold on; 
 end

hold off
end
     
     
     