%%%%%%%%%%Load Data%%%%%%%%%%%%%%%%%%%%%%
addpath(['..' filesep 'Data']);
addpath(['..' filesep 'SKEDClass']);
addpath(['..' filesep 'SKEDFunction']);

%uncomment this lineif there are other experiments to be loaded
%scpMultiDataAcquisitionE04;


%%%%Experiment Name For Later References
cExperiments = {'E04'}; 
%%%%
%load('E04CompleteData.mat'); 
%strucE04 = oExplorer; 
%cStructures{1} = strucE04; 

%%%%Severe/Mild Name of Monkey in E04 For Future References
sSevere = {'RMe','RFa'};
sMild = {'RIc','RSb'};
sName = [sSevere,sMild];

%%%%Load E04 Data%%%%%%
load('E04CompleteData.mat'); 
strucE04 = oExplorer; 

%%%Filter Out Un-related Monkeys
for i = 1:4 
    for j = 1:size(strucE04.Data,1)
        if isempty(strucE04.Data{j,1}) == 0
            if strcmp(sName{i},strucE04.Data{j,1}.Name(1:end-2))
                mId(i) = j;
            end
        end
    end
end

strucE04.Data = strucE04.Data(mId,:); 

%%%%%%%%%%%%Append E04 Data to cStructures
cStructures{1} = strucE04; 

clearvars -except cStructures
%%%Print Figure%%%%
%funPrintImage(h,'../Figure/MultiExperimentParasitesRBCDistribution');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


