% Call the script that loads data in RAM
if ~(exist('cStructures','var'))
    scpMultiDataLoad;
end

clc

%%%%%%%%Aggregate Clinical Data For Each Experiment%%%%%%%%%%
for i = 1:5 %For each of the four experiment
    mIdxFXGN = [6; 5; 6; 6; 6]; %%%Find Index of Whole Blood FXGN RAW Data
    
    oExplorerAggregated = clsSKEDAnalysis(); %%Generate Empty Class

    for j = 1:size(cStructures{i}.Data,1) %%For Each Monkey in Each Experiment
        %%%aggregateTimeSeries is used to align different type series by
        %%%collection type and keep the intersection of collection dates. 
        %%%For example, clinical is collected on day 1 to 100, and FXGN whole
        %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
        %%%series are from day 1 2 5 8 19. 

        oExplorerAggregated.Data{j} = cStructures{i}.aggregateTimeSeries(j,mIdxFXGN(i,:)); %Populate Class with aggregated data per monkey
    end
    
        %%%Filter out time series that contains missing data. 

        %%oExplorerAggregated = oExplorerAggregated.filterMissingData(); 

        %%%Filter out time series that do not appear for all the subjects. 

        %%%oExplorerAggregated = oExplorerAggregated.filterSharedVariables();
    
    cAggregatedStructures{i} = oExplorerAggregated; %%%Append Aggregate Data To Cell
end


%%%%%Differential Expression Analysis of E04 

mCountData = []; 
mCountData(:,1) = cAggregatedStructures{2}.Data{1}.DataPrimitive.Table(:,1);
mCountData(:,2) = cAggregatedStructures{2}.Data{2}.DataPrimitive.Table(:,1);
mCountData(:,3) = cAggregatedStructures{2}.Data{3}.DataPrimitive.Table(:,1);
mCountData(:,4) = cAggregatedStructures{2}.Data{4}.DataPrimitive.Table(:,1);
mCountData(:,5) = cAggregatedStructures{2}.Data{1}.DataPrimitive.Table(:,2);
mCountData(:,6) = cAggregatedStructures{2}.Data{2}.DataPrimitive.Table(:,2);
mCountData(:,7) = cAggregatedStructures{2}.Data{3}.DataPrimitive.Table(:,2);
mCountData(:,8) = cAggregatedStructures{2}.Data{4}.DataPrimitive.Table(:,2);

cGeneName = cAggregatedStructures{2}.Data{1}.DataPrimitive.VarNames; 
severe = [1 2;5 6];
mild = [3 4;7 8];
%%%Remove Parasite Gene %%%%%
idx = strncmp(cGeneName,'x_PCYB',6);
cGeneName = cGeneName(idx ==0); 
mCountData = mCountData(idx == 0,:); 

%%%Library Normalization%%%%

pseudoRefSample = geomean(mCountData,2);
nz = pseudoRefSample > 0;
ratios = bsxfun(@rdivide,mCountData(nz,:),pseudoRefSample(nz));
sizeFactors = median(ratios,1);

normCounts = bsxfun(@rdivide,mCountData,sizeFactors);

%%%%%%Differential Expression At T2 
lowCountThreshold = 10;
lowCountGenes = all(normCounts < lowCountThreshold, 2);

tLocalT2 = nbintest(normCounts(:,severe(2,:)),normCounts(:,mild(2,:)),'VarianceLink','LocalRegression'); 
tLocalSevere = nbintest(normCounts(:,severe(1,:)),normCounts(:,severe(2,:)),'VarianceLink','LocalRegression'); 
tLocalMild = nbintest(normCounts(:,mild(1,:)),normCounts(:,mild(2,:)),'VarianceLink','LocalRegression'); 

cGeneName = cGeneName(~lowCountGenes); 

[a T2Q] = mafdr(tLocalT2.pValue(~lowCountGenes));
[a SevereQ] = mafdr(tLocalSevere.pValue(~lowCountGenes));
[a MildQ] = mafdr(tLocalMild.pValue(~lowCountGenes));

Idx = T2Q <= 0.05 & ((SevereQ <= 0.05) | (MildQ <= 0.05));

tempCount = normCounts(~lowCountGenes,:); 
SevereUp = mean(tempCount(:,[5 6]),2) > mean(tempCount(:,[7 8]),2);
MildUp = mean(tempCount(:,[5 6]),2) < mean(tempCount(:,[7 8]),2);

SevereUpGenes = cGeneName(Idx&SevereUp);
MildUpGenes = cGeneName(Idx&MildUp); 
AllGenes = cGeneName(Idx);

%%%%%%%%%Transcriptome Data Projection%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cSubject = [];
mCountData = []; 
cGeneName = cAggregatedStructures{2}.Data{1}.DataPrimitive.VarNames;
idx = strncmp(cGeneName,'x_PCYB',6);
cGeneName = cGeneName(idx ==0); 
mCountDataT1 = [];
mCountDataT2 = [];
for i = 1:1
    for j = 1:size(cAggregatedStructures{i}.Data,2)
        [temp ia ib] = intersect(cAggregatedStructures{i}.Data{j}.DataPrimitive.VarNames,cGeneName);
        mCountDataT1 = [mCountDataT1 cAggregatedStructures{i}.Data{j}.DataPrimitive.Table(ia,1)];
        mCountDataT2 = [mCountDataT2 cAggregatedStructures{i}.Data{j}.DataPrimitive.Table(ia,2)];

        mSubjectID = {[cAggregatedStructures{i}.Data{j}.ExperimentID '-' cAggregatedStructures{i}.Data{j}.Name]};
        cSubject = [cSubject mSubjectID];
    end
end
geneName = temp;

mCountData = [mCountDataT1 mCountDataT2];
pseudoRefSample = geomean(mCountData,2);
nz = pseudoRefSample > 0;
ratios = bsxfun(@rdivide,mCountData(nz,:),pseudoRefSample(nz));
sizeFactors = median(ratios,1);
normCounts = bsxfun(@rdivide,mCountData,sizeFactors);

[temp ia ib] = intersect(geneName,AllGenes); 
normCounts = normCounts(ia,:);
lowCountThreshold = 10;
lowCountGenes = all(normCounts < lowCountThreshold, 2);

normCounts = normCounts(~lowCountGenes,:);
mDiff = normCounts(:,6:end) - normCounts(:,1:5);

%mDiff = normCounts(:,10:end) - normCounts(:,1:9);

sLinkage = 'ward';
clustergram(mDiff,'standardize',2,'ColumnLabels',cSubject,'Linkage',sLinkage)
clustergram(normCounts(:,6:end),'standardize',2,'ColumnLabels',cSubject,'Linkage',sLinkage)

%clustergram(normCounts(:,10:end),'standardize',2,'ColumnLabels',cSubject,'Linkage',sLinkage)

