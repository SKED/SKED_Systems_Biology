clear; clc; 
disp('SKED Tutorial. Examples of multiple data type retieval from the MaHPIC and HAMMER projects')
disp('Calling the script that loads data in RAM...')
if ~(exist('cStructures','var'))
    scpMultiDataLoad;
end
%%
disp('-----------------------------------------------')
disp('All downloaded data has been loaded into the variable cStructure. ')
disp('Now look at which experiments have been downloaded:')
cExperiments = funGetExperimentNames(cStructures);
display(cExperiments);

%%
disp('-----------------------------------------------')
disp('Define experiment of interest') 
sExp = 'E03';
disp('Generate Overview of Data of E03. Table shows frequency of protocols')
tExperiment = funGetExperimentSummary(cStructures,sExp);
disp(tExperiment)

%%
disp('-----------------------------------------------')
disp('Generate overview of protocol of E03 Clinical')
sSubjects = 'Summary';
sDataType = 'Clinical';
disp(funGetProtocol(cStructures,sExp,sSubjects,sDataType));

%%
disp('-----------------------------------------------')
disp('Generate overview of protocol of E03 Metabolomics of subject RCs13')
sSubjects = 'RZe13';
sDataType = 'Metabolomics';
disp(funGetProtocol(cStructures,sExp,sSubjects,sDataType));
sDataType = 'Clinical';
disp(funGetProtocol(cStructures,sExp,sSubjects,sDataType));

%%
disp('-----------------------------------------------')
disp('Explore what variable are measured for a protocol for  a subject')
disp('Display Variable Names')
sProtocol = 'ME_CBC';
display(funGetVariableNames(cStructures,sExp,sSubjects,sProtocol));
sProtocol = 'ME_parasitemia';             
display(funGetVariableNames(cStructures,sExp,sSubjects,sProtocol));

%%
disp('-----------------------------------------------')
cVariables = {'ME_CBC','hgb'}; 
h =  funPlotVariable(cStructures,sExp,sSubjects,cVariables);
disp(['Plot of time series of hemoglobin. See figure ' num2str(h.Number)] )

%%
disp('-----------------------------------------------')
cVariables = {'ME_CBC','hgb';'ME_parasitemia','parasites'}; 
h =  funPlotVariable(cStructures,sExp,sSubjects,cVariables);
disp(['Plot of hemoglobin vs. parasite count, i.e. variables from different protocols. See figure ' num2str(h.Number)] )

%%
disp('-----------------------------------------------')
sSubjects = 'All'; 
cVariables = {'ME_parasitemia','parasites'}; 
h =  funPlotVariable(cStructures,sExp,sSubjects,cVariables);
disp(['Plot of parasite counts for all subjects. See figure ' num2str(h.Number)] )

%%
disp('-----------------------------------------------')
cVariables = {'ME_CBC','hgb'}; 
h =  funPlotVariable(cStructures,sExp,sSubjects,cVariables);
disp(['Plot of hemoglobin for all subjects. See figure ' num2str(h.Number)] )

%%
disp('-----------------------------------------------')
disp('Display protocol types for transcriptomics')
sSubjects = 'RZe13';
sDataType = 'Functional_Genomics';
display(funGetProtocol(cStructures,sExp,sSubjects,sDataType));

%%
disp('-----------------------------------------------')
disp('We could display the variable names for FXGN, i.e. all gene names.')
sProtocol = 'FG_GENE_EXP_RAW2';
disp('To list gene names, uncomment the line that follows in source code.')
%display(funGetVariableNames(cStructures,sExp,sSubjects,sProtocol));

%%
disp('-----------------------------------------------')
cVariables = {'FG_GENE_EXP_RAW2','IL27RA'};
h =  funPlotVariable(cStructures,sExp,sSubjects,cVariables);
disp(['Plot of time series for gene IL27RA, one subject. See figure ' num2str(h.Number)] )

%%
disp('-----------------------------------------------')
sSubjects = 'All';
h =  funPlotVariable(cStructures,sExp,sSubjects,cVariables);
disp(['Plot of time series for gene IL27RA, all subjects. See figure ' num2str(h.Number)] )

%%
disp('-----------------------------------------------')
disp('Display protocol types for metabolomics')
sSubjects = 'RZe13';
sDataType = 'Metabolomics';
display(funGetProtocol(cStructures,sExp,sSubjects,sDataType));

%%
disp('-----------------------------------------------')
disp('We could display the variable names for all metabolites.')
sProtocol = 'MB_c18_neg';
disp('To list metabolites, uncomment the lines that follows in source code')
%display(funGetVariableNames(cStructures,sExp,sSubjects,sProtocol));

%%
disp('-----------------------------------------------')
cVariables = {'MB_c18_neg','435_259914_83_940934'};
h =  funPlotVariable(cStructures,sExp,sSubjects,cVariables);
disp(['Plot the time series of a single metabolite for a single subject. See figure ' num2str(h.Number)] )

%%
disp('-----------------------------------------------')
sSubjects = 'All';
h =  funPlotVariable(cStructures,sExp,sSubjects,cVariables);
disp(['Plot the time series of a single metabolite for a all subject. See figure ' num2str(h.Number)] )
%%
disp('-----------------------------------------------')
disp('Get functional genomic data for all subjects')
sSubjects = 'All';
sProtocol = 'FG_GENE_EXP_RAW2'; 
strucData = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol) ;

%%
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCOAH';
strucData = funParasiteGeneRemoval(strucData,sIdentifier);
%%
disp('-----------------------------------------------')
disp('Normalize count data by library size and remove genes with consistent low counts')
nCount = 10; 
strucLibSizeNormalizedData = funLibSizeNormalizationLowCountRemoval(strucData,nCount); 

%%
disp('-----------------------------------------------')
disp('Define groups for differential expression analysis')
g1 = {1, 1, 1, 1, 1};
g2 = {2, 2, 2, 2, 2};
%%
disp('-----------------------------------------------')
disp('Conduct differential expression analysis')
tReport = funDeSeq(strucLibSizeNormalizedData,g1,g2);
%%
disp('-----------------------------------------------')
disp('Display genes with the lowest q-value')
%%
tSignificantGenes = sortrows(tReport,2,'ascend');
display(tSignificantGenes(1:20,:));
%%
disp('-----------------------------------------------')
disp('Get FXGN Data For E23 and E24.')
sSubjects = 'All';
sProtocol = 'FG_GENE_EXP_RAW2'; 
sExp = {'E23'};
strucDataE23 = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
sExp = {'E24'};
strucDataE24 = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
%%
strucData = funCombineData(strucDataE23,strucDataE24);
%%
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCYB';
strucData = funParasiteGeneRemoval(strucData,sIdentifier);
%%
disp('-----------------------------------------------')
disp('Normalize count data by library size and remove genes with consistent low counts')
nCount = 10; 
strucLibSizeNormalizedData = funLibSizeNormalizationLowCountRemoval(strucData,nCount); 
%%
disp('-----------------------------------------------')
disp('Define groups for differential expression analysis')
g1 = {1, 1, 1, 1, 1,1,[],[],[],[],[]};
g2 = {[], [], [], [], [],[],1,1,1,1,1};
%%
disp('-----------------------------------------------')
disp('Conduct differential expression analysis')
tReport = funDeSeq(strucLibSizeNormalizedData,g1,g2);
%%
disp('-----------------------------------------------')
disp('Display genes with the lowest q-value')
%%
tSignificantGenes = sortrows(tReport,2,'ascend');
display(tSignificantGenes(1:20,:));
%%
%%
disp('-----------------------------------------------')
disp('Get CBC Data For E23,E24,E25')
sSubjects = 'All';
sProtocol = 'ME_CBC'; 
sExp = {'E23'};
strucDataE23 = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
sExp = {'E24'};
strucDataE24 = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
sExp = {'E04'};
strucDataE04 = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);

%%Combine Data
disp('-----------------------------------------------')
disp('Combine data from multiple experiments. ')
strucData = funCombineData(strucDataE23,strucDataE24,strucDataE04);
%%
disp('-----------------------------------------------')
disp('Define experiment ID as group label. ')
cLabel = strucData.Experiment;
disp('Conduct MANOVA and cannonical variable analysis of ')

funMANOVAPlot(strucData,strucData.Experiment);
