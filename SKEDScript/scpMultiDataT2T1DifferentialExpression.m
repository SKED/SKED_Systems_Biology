clear; clc; 
disp('Analysis of transcriptomic data for E04,E03,E23,E24,E25')
disp('Calling the script that loads data in RAM...')
if ~(exist('cStructures','var'))
    scpMultiDataLoad;
end
%%
sExp = 'E03';
sProtocol = 'FG_GENE_EXP_RAW2'; 
sSubjects = 'All';

disp('-----------------------------------------------')
disp('Get functional genomic data for all subjects')
strucData = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
strucDataParasite = funGetProtocolData(cStructures,sExp,sSubjects,'ME_parasitemia');

%%
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCOAH';
strucData = funParasiteGeneRemoval(strucData,sIdentifier);
%%
disp('-----------------------------------------------')
disp('Normalize count data by library size and remove genes with consistent low counts')
nCount = 10; 
strucLibSizeNormalizedData = funLibSizeNormalizationLowCountRemoval(strucData,nCount); 
%%
%sLabel = {'RZe13','RUn13','RCs13','RTi13','RWr13'};
%mData = [];
%for i = 1:5
%    mData(:,i) = strucLibSizeNormalizedData.cRawCounts{i}(:,2)./(strucLibSizeNormalizedData.cRawCounts{i}(:,1)+0.00001);
%end
%
%n = 100;
%
%tempVariance = var(mData,[],2);
%[temp idx] = sort(tempVariance,'descend');
%clustergram(mData(idx(1:n),:),'ColumnLabels',sLabel,'Standardize',2);
%%
sSevereSubjectE03 = 'RWr13';
sMildSubjectE03 = 'RTi13';

disp('-----------------------------------------------')
disp('Define groups for differential expression analysis')
g1 = {1, 1, 1, [], 1};
g2 = {2, 2, 2, [], 2};
%%
disp('-----------------------------------------------')
disp('Conduct differential expression analysis')
tReportE03 = funDeSeq(strucLibSizeNormalizedData,g1,g2);
%%
%%
sExp = 'E04';
sSubjects = 'All';

%%
sProtocol = 'FG_GENE_EXP_RAW2'; 
disp('-----------------------------------------------')
disp('Get functional genomic data for all subjects')
strucData = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);

%%
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCYB';
strucData = funParasiteGeneRemoval(strucData,sIdentifier);
%%
disp('-----------------------------------------------')
disp('Normalize count data by library size and remove genes with consistent low counts')
nCount = 10; 
strucLibSizeNormalizedData = funLibSizeNormalizationLowCountRemoval(strucData,nCount); 

%%
disp('-----------------------------------------------')
disp('Define groups for differential expression analysis')
g1 = {1, 1, 1, 1};
g2 = {2, 2, 2, 2};

%%
disp('-----------------------------------------------')
disp('Conduct differential expression analysis')
tReportE04 = funDeSeq(strucLibSizeNormalizedData,g1,g2);
%%
disp('-----------------------------------------------')
disp('Define groups for differential expression analysis')
g1 = {1, 1, [], []};
g2 = {2, 2, [], []};

%%
disp('-----------------------------------------------')
disp('Conduct differential expression analysis')
tReportE04S = funDeSeq(strucLibSizeNormalizedData,g1,g2);
%%
disp('-----------------------------------------------')
disp('Define groups for differential expression analysis')
g1 = {[], [], 1, 1};
g2 = {[], [], 2, 2};

%%
disp('-----------------------------------------------')
disp('Conduct differential expression analysis')
tReportE04M = funDeSeq(strucLibSizeNormalizedData,g1,g2);

%%

sExp = 'E23';

sSubjects = 'All';


%%
sProtocol = 'FG_GENE_EXP_RAW2'; 
disp('-----------------------------------------------')
disp('Get functional genomic data for all subjects')
strucData = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
strucDataParasite = funGetProtocolData(cStructures,sExp,sSubjects,'ME_parasitemia');

%%
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCYB';
strucData = funParasiteGeneRemoval(strucData,sIdentifier);
%%
disp('-----------------------------------------------')
disp('Normalize count data by library size and remove genes with consistent low counts')
nCount = 10; 
strucLibSizeNormalizedData = funLibSizeNormalizationLowCountRemoval(strucData,nCount); 

%%
disp('-----------------------------------------------')
disp('Define groups for differential expression analysis')
g1 = {1, 1, 1, 1, 1,1};
g2 = {4, 4, 4, 4, 4,4};
%%
disp('-----------------------------------------------')
disp('Conduct differential expression analysis')
tReportE23 = funDeSeq(strucLibSizeNormalizedData,g1,g2);
%%
sExp = 'E24';

sSubjects = 'All';

%%
sProtocol = 'FG_GENE_EXP_RAW2'; 
disp('-----------------------------------------------')
disp('Get functional genomic data for all subjects')
strucData = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);

%%
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCYB';
strucData = funParasiteGeneRemoval(strucData,sIdentifier);
strucDataParasite = funGetProtocolData(cStructures,sExp,sSubjects,'ME_parasitemia');

%%
disp('-----------------------------------------------')
disp('Normalize count data by library size and remove genes with consistent low counts')
nCount = 10; 
strucLibSizeNormalizedData = funLibSizeNormalizationLowCountRemoval(strucData,nCount); 

%%
disp('-----------------------------------------------')
disp('Define groups for differential expression analysis')
g1 = {1, 1, 1, 1, 1};
g2 = {2, 2, 2, 2, 2};
%%
disp('-----------------------------------------------')
disp('Conduct differential expression analysis')
tReportE24 = funDeSeq(strucLibSizeNormalizedData,g1,g2);
%%
%%
sExp = 'E25';
sSubjects = 'All';

%%
sProtocol = 'FG_GENE_EXP_RAW2'; 
disp('-----------------------------------------------')
disp('Get functional genomic data for all subjects')
strucData = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
strucDataParasite = funGetProtocolData(cStructures,sExp,sSubjects,'ME_parasitemia');

%%
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCYB';
strucData = funParasiteGeneRemoval(strucData,sIdentifier);
%%
disp('-----------------------------------------------')
disp('Normalize count data by library size and remove genes with consistent low counts')
nCount = 10; 
strucLibSizeNormalizedData = funLibSizeNormalizationLowCountRemoval(strucData,nCount); 

%%
disp('-----------------------------------------------')
disp('Define groups for differential expression analysis')
g1 = {1, 1, 1, 1, 1};
g2 = {3, 3, 3, 3, 3};
%%
disp('-----------------------------------------------')
disp('Conduct differential expression analysis')
tReportE25 = funDeSeq(strucLibSizeNormalizedData,g1,g2);
%%
disp('Generate Up-regulated gene report');
E03Up = tReportE03.GeneNames(tReportE03.qValue<=0.05&tReportE03.FoldChange<1);
E04Up = tReportE04.GeneNames(tReportE04.qValue<=0.05&tReportE04.FoldChange<1);
E04SUp = tReportE04S.GeneNames(tReportE04S.qValue<=0.05&tReportE04S.FoldChange<1);
E04MUp = tReportE04M.GeneNames(tReportE04M.qValue<=0.05&tReportE04M.FoldChange<1);
E23Up = tReportE23.GeneNames(tReportE23.qValue<=0.05&tReportE23.FoldChange<1);
E24Up = tReportE24.GeneNames(tReportE24.qValue<=0.05&tReportE24.FoldChange<1);
E25Up = tReportE25.GeneNames(tReportE25.qValue<=0.05&tReportE25.FoldChange<1);


cExpLabel = {'E03','E04','E04S','E04M','E23','E24','E25'};

cUpGenes = {E03Up,E04Up,E04SUp,E04MUp,E23Up,E24Up,E25Up};
mSharedUpGenes = []; 
for i = 1:length(cUpGenes)
    for j = 1:length(cUpGenes)
        temp = intersect(cUpGenes{i},cUpGenes{j});
        temp2 = union(cUpGenes{i},cUpGenes{j});
        mSharedUpGenes(i,j) = (length(temp2) - length(temp))/length(temp2);
    end
end

Z = linkage(mSharedUpGenes);
figure()
dendrogram(Z,'Labels',cExpLabel);
title('Up Regulated Gene Clustering');

%%
disp('Generate down-regulated gene report');
E03Down = tReportE03.GeneNames(tReportE03.qValue<=0.05&tReportE03.FoldChange>1);
E04Down = tReportE04.GeneNames(tReportE04.qValue<=0.05&tReportE04.FoldChange>1);
E04SDown = tReportE04S.GeneNames(tReportE04S.qValue<=0.05&tReportE04S.FoldChange>1);
E04MDown = tReportE04M.GeneNames(tReportE04M.qValue<=0.05&tReportE04M.FoldChange>1);
E23Down = tReportE23.GeneNames(tReportE23.qValue<=0.05&tReportE23.FoldChange>1);
E24Down = tReportE24.GeneNames(tReportE24.qValue<=0.05&tReportE24.FoldChange>1);
E25Down = tReportE25.GeneNames(tReportE25.qValue<=0.05&tReportE25.FoldChange>1);

%%
cDownGenes = {E03Down,E04Down,E04SDown,E04MDown,E23Down,E24Down,E25Down};
mSharedUpGenes = []; 
for i = 1:length(cDownGenes)
    for j = 1:length(cDownGenes)
        temp = intersect(cDownGenes{i},cDownGenes{j});
        temp2 = union(cDownGenes{i},cDownGenes{j});
        mSharedDownGenes(i,j) = length(temp)/length(temp2);
    end
end

Z = linkage(mSharedDownGenes);
figure()
dendrogram(Z,'Labels',cExpLabel);
title('Down Regulated Gene Clustering');
%%
cCommonUpGene = E03Up; 
for i = 1:length(cUpGenes)
    cCommonUpGene = intersect(cCommonUpGene,cUpGenes{i});
end
cCommonUpGene = ['Common Up Gene';cCommonUpGene];
for j = 2:8
    cCommonUpGene{1,j} = cExpLabel{j-1};
end

for j = 2:8
    for i = 1:length(cUpGenes{j-1})
            cCommonUpGene{i+1,j} = cUpGenes{j-1}{i};
            
    end
end

xlswrite('MultiDataUpRegulatedGenesT2T1',cCommonUpGene)
%%
cCommonDownGene = E03Down; 
for i = 1:length(cDownGenes)
    cCommonDownGene = intersect(cCommonDownGene,cDownGenes{i});
end
cCommonDownGene = ['Common Down Gene';cCommonDownGene];
for j = 2:8
    cCommonDownGene{1,j} = cExpLabel{j-1};
end

for j = 2:8
    for i = 1:length(cDownGenes{j-1})
            cCommonDownGene{i+1,j} = cDownGenes{j-1}{i};
            
    end
end
xlswrite('MultiDataDownRegulatedGenesT2T1',cCommonDownGene)


cCommonGene = cUpGenes{1};
for i = [2:5 7]
    cCommonGene = intersect(cCommonGene,cUpGenes{i});
end
%%
display('Do Gene Intersection');
temp = E03Up;
mNumGene(1) = length(temp);
cUpGenes = {E03Up,E04Up,E23Up,E25Up,E24Up};

for i = 2:length(cUpGenes)
    temp = intersect(temp,cUpGenes{i});
    mNumGene(i) = length(temp);
end
bar(mNumGene);
ylabel('Number of Differentially Upregulated Genes'); 
cLabel = {'E03','E03/E04','E03/E04/E23','E03/E04/E23/E25','E03/E04/E23/E25/E24'};
xticklabels(cLabel)
set(gca,'FontSize',18,'FontWeight','bold');
%%
display('Do Gene UpRegulation');
cUpGenes = {E03Up,E04Up,E23Up,E25Up,E24Up};
mNumGene = [];

for i = 1:length(cUpGenes)
    temp = cUpGenes{i};
    mNumGene(i) = length(temp);
end
bar(mNumGene);
ylabel('Number of Differentially Upregulated Genes'); 
cLabel = {'E03','E04','E23','E25','E24'};
xticklabels(cLabel)
set(gca,'FontSize',18,'FontWeight','bold');
%%
[num txt raw] = xlsread('MultiDataSharedAndUniqueUpRegulatedPathways.xlsx');
txt = txt(:,2:end);
cLabel = txt(1,:); 

temp = txt(2:end,:); 
cPathways = unique(temp);
cPathways = cPathways(2:end);
cXCoordinate = {};
for i = 1:length(cPathways)
    tempCoor = []; 
    for j = 1:size(temp,2)
        temp2 = temp(:,j);
        idx = find(strcmp(cPathways(i),temp2));
        if isempty(idx) == 0
            tempCoor = [tempCoor j];
        end
    end
    cXCoordinate{i} = tempCoor;
end

mSize = []; 
for i = 1:length(cXCoordinate)
    mSize(i) = length(cXCoordinate{i});
end

[temp idx] = sort(mSize,'descend');
cPathways = cPathways(idx); 
cXCoordinate = cXCoordinate(idx);

figure()
for i = 1:length(cPathways)
    for j = 1:length(cXCoordinate{i})
       plot(cXCoordinate{i}(j),i,'rx','MarkerSize',22);
       hold on 
    end
end
for i = 1:length(cPathways)
    cPathways{i} = cPathways{i}(5:end);
    cPathways{i} = strrep(cPathways{i},'_',' ');
end
xticks([1:length(cLabel)]);
yticks([1:length(cPathways)]);
xticklabels(cLabel);
yticklabels(cPathways(idx));
grid on 
%%
display('TF Analysis'); 
[num txt raw] = xlsread('MultiDataUpRegulatedGenesT2T1TF.xls');
E03UpTF = intersect(cellfun(@(x) x(3:end-1), E03Up, 'UniformOutput', false),txt(:,2));
E04UpTF = intersect(cellfun(@(x) x(3:end-1), E04Up, 'UniformOutput', false),txt(:,3));
E23UpTF = intersect(cellfun(@(x) x(3:end-1), E23Up, 'UniformOutput', false),txt(:,6));
E25UpTF = intersect(cellfun(@(x) x(3:end-1), E25Up, 'UniformOutput', false),txt(:,8));
E24UpTF = intersect(cellfun(@(x) x(3:end-1), E24Up, 'UniformOutput', false),txt(:,7));
cUpTFData = {E03UpTF,E04UpTF,E23UpTF,E25UpTF,E24UpTF};

numGene = []; 
for i = 1:length(cUpTFData)
    numGene(i) = length(cUpTFData{i}); 
end

bar(numGene);
ylabel('Number of Differentially Upregulated TF With Enriched Targets'); 
cLabel = {'E03','E04','E23','E25','E24'};
xticklabels(cLabel)
set(gca,'FontSize',18,'FontWeight','bold');
%%
numGene = []; 
temp = cUpTFData{1};
numGene(1) = length(temp);
cTF{1} = temp;
for i = 2:length(cUpTFData)
    temp = intersect(temp,cUpTFData{i});
    numGene(i) = length(temp);
    cTF{i} = temp; 
end

bar(numGene);
ylabel('Number of Differentially Upregulated TF With Enriched Targets'); 
cLabel = {'E03','E03/E04','E03/E04/E23','E03/E04/E23/E25','E03/E04/E23/E24'};
xticklabels(cLabel)
set(gca,'FontSize',18,'FontWeight','bold');
%%
