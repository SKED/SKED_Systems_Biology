cMonkey = {
    'E07A', '1402', '12C53';
     'E07A', '1404','H12C59';
     'E07A', '1405','H12C8';%
     'E07A', '1407', '12C44';
     %'E07A', '1408', '11C131'; % Implant turned off early due to animal
                                % being removed by clinicians due to ear access difficulty
     'E07A', '1409', '11C166';%
     'E07A', '1410', 'H12C50';
     
    'E07B', '1401', '12C136'; 
    'E07B', '1402', '12C53'; 
    'E07B', '1404', 'H12C59'; 
    'E07B', '1405', 'H12C8'; 
    'E07B', '1407', '12C44'; 
    'E07B', '1408', '11C131'; 
    'E07B', '1409', '11C166'; 
    
    'E06', '1918', 'RUf16'; 
    'E06', '1916', 'RCl15'; 
    'E06', '1915', 'RTe16'; 
    'E06', '1912', 'RIh16';
    
    'E30', '1403', 'RKy15'; 
    'E30', '1406', 'REd16'
     };
 dummy={};
 dummy2={};

 for k= 1:size(cMonkey,1)
     if k<=6
         sColor = ['b','g','w'];
     else
         sColor = ['b','g','r'];
     end
         
     [pl n X,mRawDaily ] = funGrasmann(cMonkey(k,:));
     dummy =horzcat(dummy,X);
     dummy2 =horzcat(dummy2,mRawDaily);
     set(gca,'FontSize',10)
   
     figure(1);
     subplot(4,5,k);
     set(gca,'FontSize',10)
     set(gcf,'color','w');

     %set(findall(gca,'-property','FontSize'),'FontSize',10)
     
     for i=1:3
        
      
        idx = (1:n)+(i-1)*n; 
                        % plot3(x(idx), y(idx),z(idx), ['o' sColor(i)]); 
                       %plot(x(idx), y(idx), ['o' sColor(i)]);
                       
        plot(pl(idx,1),pl(idx,2),['o' sColor(i)]); 
                        %plot3(pl(idx,1),pl(idx,2),pl(idx,3),['o' sColor(i)]);
        set(gca,'FontSize',10)
        set(gcf,'color','w');                

        hold on;  
     end
     
     title([cMonkey{k,1} ' - ' cMonkey{k,3}]);
     set(gca,'FontSize',10)
     set(gcf,'color','w');
    hold off
     
 end
 for k= 1:size(cMonkey,1)
     figure(3)
     subplot(4,5,k);
     plot(dummy2{1,k}(:,1))
 end
 for k= 1:size(cMonkey,1)
 figure(2)
 subplot(4,5,k);
     plot(dummy{1,k}(:,1))
 end
 