clear
addpath('../function'); 

load('E04WBClinImmuneDiffCorr.mat'); 
load('E04WBClinImmuneDiffCorr2.mat'); 
load('E04WBClinImmuneChiSQ.mat'); 
load('E04WBClinImmuneMPATS.mat'); 

sName = 'E04WBClinImmune'; 

P1 = strucChiSQ.Pval; 
P2 = strucDiffCorr.Pval;
P3 = strucDiffCorr2.Pval; 
P4 = strucMPATS.Pvalue;

P1(P1 == 0) = 10^-22; 

P2(P2 == 0) = 10^-22; 
P3(P3 == 0) = 10^-22; 

P4(P4 == 0) = 10^-22; 

mConsensus = -2*(log(P1)+log(P2)+log(P3) + log(P4));
mConsensus = chi2cdf(mConsensus,8,'upper');
[mConsensus] = mafdr(mConsensus,'BHFDR',1);


mEdgeScore = squareform(mConsensus); 
mEdgeScore(mEdgeScore >= 0.05) = 0;
mEdgeScore(isnan(mEdgeScore)) = 0;
%mEdgeScore(mEdgeScore ~= 0) = 1; 
sum(sum(mEdgeScore) > 0)
fileName = ['CytoscapeFiles/' sName '.txt'];

fileID = fopen(fileName,'w');

fprintf(fileID, ['Node1\tNode2\tWeight\n']);

for i = 1:size(mEdgeScore,1)
    for j = i:size(mEdgeScore,1)
        if mEdgeScore(i,j) > 0
            fprintf(fileID, [strucMPATS.Names{i} '\t' strucMPATS.Names{j} '\t' num2str(mEdgeScore(i,j)) '\n']);
        end
    end
end
fclose(fileID);

mConsensus = -2*(log(P1)+log(P2)+log(P3) + log(P4));
mConsensus = chi2cdf(mConsensus,8,'upper');
[mConsensus] = mafdr(mConsensus,'BHFDR',1);


mEdgeScore = squareform(mConsensus); 
mEdgeScore(mEdgeScore >= 0.05) = 0;
mEdgeScore(isnan(mEdgeScore)) = 0;
mEdgeScore(mEdgeScore ~= 0) = 1;
mScore = sum(mEdgeScore); 
[a b] = sort(mScore,'descend');

fileName = ['RNKFiles/' sName 'qval.rnk'];

fileID = fopen(fileName,'w');
    
for i = 1:length(a)
    fprintf(fileID, [strucMPATS.Names{b(i)} '\t' num2str(a(i)) '\n']);
end
fclose(fileID);