clear; addpath('../SKEDFunction')
% Parameters for analysis
lambda=0;
nfigure = 1;
nNorm = 8;
sDist = 'weibull'; 
bNormalize = false;
mFeatures = [3]; %[3 4 5 6 7 8 9 10 11]; %[7 9]; %[3 9 10 11]; %
cVarNames = {
    'ECGFeature_BPM_avg', %1
    'ECGFeature_AvgRPeakHeight_avg', %2
    'ActivityMean_5seconds', % 3
    'Higherorder2_avg', % 4
    'Higherorder3_avg', % 5
    'Higherorder_avg', % 6
    'Writhe_avg', % 7
    'AvgCross_avg', % 8
    'TEMPData_avg', % 9
    'BPSystolic_avg', % 10
    'BPDiastolic_avg' % 11
};

cMonkey = {
    'E07A', '1402', '12C53';
    'E07A', '1404','H12C59';
    'E07A', '1405','H12C8';%
    'E07A', '1407', '12C44';
    %'E07A', '1408', '11C131'; % Implant turned off early due to animal
                                % being removed by clinicians due to ear access difficulty
    'E07A', '1409', '11C166';%
    'E07A', '1410', 'H12C50';
    
    'E07B', '1401', '12C136'; 
    'E07B', '1402', '12C53'; 
    'E07B', '1404', 'H12C59'; 
    'E07B', '1405', 'H12C8'; 
    'E07B', '1407', '12C44'; 
    'E07B', '1408', '11C131'; 
    'E07B', '1409', '11C166'; 
    
    'E06', '1918', 'RUf16'; 
    'E06', '1916', 'RCl15'; 
    'E06', '1915', 'RTe16'; 
    'E06', '1912', 'RIh16';
    
    'E30', '1403', 'RKy15'; 
    'E30', '1406', 'REd16'
};

MSMatrix=[];
mAggregPre = [];
mAggregLiver = [];
mAggregBlood = [];
for j=1:length(cMonkey)
    sFileName = ['../Data/' cMonkey{j,1} 'FeatureMatrix_' num2str(cMonkey{j,2}) '_' cMonkey{j,3} '.mat'];
    load(sFileName);
    sID = [cMonkey{j,1} ' - ' cMonkey{j,3} ' - '];
    switch cMonkey{j,1}
        case 'E30'
            dtTP(1) = datenum('7/28/0016');
            dtTP(2) = datenum('7/31/0016');
            dtTP(3) = datenum('8/12/0016');
            dtTP(4) = datenum('8/15/0016');% Inoculation. TP numbers after this are increased by 1
            dtTP(5) = datenum('8/18/0016');% Parasitemia
            dtTP(6) = datenum('8/21/0016');
        case 'E07A'
            dtTP(1) = datenum('10/21/0016'); %Day after Implant is turned on
            dtTP(2) = datenum('10/25/0016'); 
            dtTP(3) = datenum('11/02/0016');
            dtTP(4) = datenum('11/05/0016'); % Inoculation. TP numbers after this are increased by 1
            dtTP(5) = datenum('11/12/0016'); % Parasitemia should have appeared here
            dtTP(6) = datenum('11/14/0016'); % Day before implants were turned off
        case 'E07B'
            dtTP(1) = datenum('1/11/0017'); 
            dtTP(2) = datenum('1/15/0017');
            dtTP(3) = datenum('1/21/0017');
            dtTP(4) = datenum('1/25/0017'); 
            dtTP(5) = datenum('1/28/0017'); 
            dtTP(6) = datenum('1/30/0017'); 
        case 'E06'
            dtTP(1) = datenum('3/23/0017');
            dtTP(2) = datenum('3/25/0017');
            dtTP(3) = datenum('4/03/0017');
            dtTP(4) = datenum('4/08/0017'); 
            dtTP(5) = datenum('4/10/0017'); 
            dtTP(6) = datenum('4/13/0017'); 
    end
            
    idx1=and((dtDatesFeatures)>=dtTP(1),(dtDatesFeatures)<=dtTP(2));
    idx2=and((dtDatesFeatures)>=dtTP(3),(dtDatesFeatures)<=dtTP(4));
    idx3=and((dtDatesFeatures)>=dtTP(5),(dtDatesFeatures)<=dtTP(6));
    %nHourIni = 14; nHourEnd = 18; 
    % Filter pre-infection
    Timepre=dtDatesFeatures(idx1);
    DataPre=FeatureMatrix(idx1, mFeatures);%[3 4 5 6 7 8 9 10 11]);
    [Timepre,DataPre] = funLinInterpSmooth(Timepre,DataPre,3600,lambda,bNormalize);            
    mAggregPre = [mAggregPre; DataPre];
    %dummy = datevec(dtInterpTime);   %idx = or(dummy(:,4) <=nHourIni,  %dummy(:,4) >=nHourEnd);  %Timepre = dtInterpTime(idx);  %DataPre = funHodrickPrescott( fInterpData(idx,:), 100);
    % Filter liver stage
    Timeliver=dtDatesFeatures(idx2);
    DataLiver=FeatureMatrix(idx2, mFeatures);%[3 4 5 6 7 8 9 10 11]);
    [Timeliver,DataLiver] = funLinInterpSmooth(Timeliver,DataLiver,3600,lambda,bNormalize); 
    mAggregLiver = [mAggregLiver; DataLiver];
    % Filter blood stage
    Timeblood=dtDatesFeatures(idx3);
    DataBlood=FeatureMatrix(idx3, mFeatures);%[3 4 5 6 7 8 9 10 11]);
    [Timeblood,DataBlood] = funLinInterpSmooth(Timeblood,DataBlood,3600,lambda,bNormalize);   
    mAggregBlood = [mAggregBlood; DataBlood];
    % Show histogram of all variables
    for i=1:length(mFeatures)
        figure(nfigure+5); 
        subplot(1,length(mFeatures),i);
        hist(DataPre(:,i)); title([sID 'Pre ' cVarNames{mFeatures(i)}],'Interpreter', 'none');
        figure(nfigure+6);
        subplot(1,length(mFeatures),i);
        hist(DataLiver(:,i));title([sID 'Liver ' cVarNames{mFeatures(i)}],'Interpreter', 'none');
        figure(nfigure+7);
        subplot(1,length(mFeatures),i);
        hist(DataBlood(:,i)); title([sID 'Blood ' cVarNames{mFeatures(i)}],'Interpreter', 'none');
    end    
    % Compute pairwaise distances
    DPre=pdist(DataPre,'euclidean');
    DLiver=pdist(DataLiver,'euclidean');
    DBlood=pdist(DataBlood,'euclidean');

    d1 = []; d2 = []; d3 = [];
    for i=1:length(DataPre), d1(i) = norm(DataPre(i,:),nNorm); end; DPre = d1;
    for i=1:length(DataLiver), d2(i) = norm(DataLiver(i,:),nNorm); end; DLiver = d2;
    for i=1:length(DataBlood), d3(i) = norm(DataBlood(i,:),nNorm); end; DBlood = d3;

    figure(nfigure); clf
    subplot(1,3,1);
    h = histogram(DPre,100,'Normalization','probability');
    pd1 = fitdist(DPre', sDist); 
    disp('DPre'); disp(pd1);
    title([sID 'Pre']);
    subplot(1,3,2);
    h = histogram(DLiver,100,'Normalization','probability');
    pd2 = fitdist(DLiver', sDist); 
    disp('DLiver'); disp(pd2)
    title([sID 'Liver']);
    subplot(1,3,3)
    h = histogram(DBlood,100,'Normalization','probability');
    pd3 = fitdist(DBlood', sDist); 
    disp('DBlood'); disp(pd3)
    title([sID 'Blood']);

    figure(nfigure+1); clf;
    x = min(d1): max(d1); y = pdf(pd1,x);
    hold on; plot(x,y,'b'); 
    x = min(d2): max(d2); y = pdf(pd2,x);
    hold on; plot(x,y,'g'); 
    x = min(d3): max(d3); y = pdf(pd3,x);
    hold on; plot(x,y,'r'); 
    legend('Pre', 'Liver', 'Blood');
    title(sID);
    
    figure(nfigure+2); clf
    DataAll=[DataPre;DataLiver;DataBlood];
    AllTime=[Timepre;Timeliver;Timeblood];
    Dall=pdist(DataAll,'euclidean');
    h = histogram(Dall,100,'Normalization','probability');
    title(sID);
    switch sDist 
        case 'gamma'
            MSMatrix(j,1)=pd1.a;
            MSMatrix(j,2)=pd1.b;
            MSMatrix(j,3)=pd1.mean;
            MSMatrix(j,4)=pd1.var;
            MSMatrix(j,5)=pd2.a;
            MSMatrix(j,6)=pd2.b;
            MSMatrix(j,7)=pd2.mean;
            MSMatrix(j,8)=pd2.var;
            MSMatrix(j,9)=pd3.a;
            MSMatrix(j,10)=pd3.b;
            MSMatrix(j,11)=pd3.mean;
            MSMatrix(j,12)=pd3.var;
        case 'normal'
            MSMatrix(j,1)=pd1.mu;
            MSMatrix(j,2)=pd1.sigma;
            MSMatrix(j,3)=pd1.mu;
            MSMatrix(j,4)=pd1.sigma;
            MSMatrix(j,5)=pd2.mu;
            MSMatrix(j,6)=pd2.sigma;
        otherwise
            disp('Otherwise')
    end
    figure(2)
end

figure(nfigure+8);
nVar = 1;
h = histogram(mAggregPre(:,nVar),'Normalization','probability'); title([sID 'Pre']);
h.FaceColor = 'b';
hold on; 
%figure(nfigure+9);
h = histogram(mAggregLiver(:,nVar),'Normalization','probability'); title([sID 'Liver']);
h.FaceColor = 'g';
hold on;
%figure(nfigure+10);
h = histogram(mAggregBlood(:,nVar),'Normalization','probability'); title([sID 'Blood']);
h.FaceColor = 'r';

figure(nfigure+3); clf
subplot(1,3,1)
h = histogram(MSMatrix(:,4),12,'Normalization','probability');
subplot(1,3,2)
h = histogram(MSMatrix(:,8),12,'Normalization','probability');
subplot(1,3,3)
h = histogram(MSMatrix(:,12),12,'Normalization','probability');
        
            
return            
            
        n=randperm(length(DataAll),1);
        I=[];
        vec=DataAll(n,:);
        vecclust=[];
        for i=1:length(DataAll)
            temp=sqrt(sum((vec-DataAll(i,:)).^2));
            if temp<=500
                I=[I;i];
                vecclust=[vecclust;DataAll(i,:)];
            end
        end
        vecclusttime=AllTime(I);
%           Dvecclust=pdist(vecclust,'euclidean'); 
         idx4=and((vecclusttime)>=dtTP(1),(vecclusttime)<=dtTP(2));
            idx5=and((vecclusttime)>=dtTP(3),(vecclusttime)<=dtTP(4));
             idx6=and((vecclusttime)>=dtTP(5),(vecclusttime)<=dtTP(6));
            preclust=vecclust(idx4,:);
            pleclustTime=datestr(vecclusttime(idx4));
            Liverclust=vecclust(idx5,:);
            LiverclustTime=datestr(vecclusttime(idx5));
            Bloodclust=vecclust(idx6,:);
            BloodclustTime=datestr(vecclusttime(idx6));
            m=kmeans(vecclust,3);
            kclust1=vecclust(m==1,:);
            kclustTime1=datestr(vecclusttime(m==1));
            kclustTime2=datestr(vecclusttime(m==2));
            kclustTime3=datestr(vecclusttime(m==3));
            kclust2=vecclust(m==2,:);
            kclust3=vecclust(m==3,:);
            D2=pdist(vecclust);
            pl=cmdscale(D2,3);
            figure(3)
plot3(pl(idx4,1),pl(idx4,2),pl(idx4,3), 'b.','MarkerSize',12)
hold on
 plot3(pl(idx5,1),pl(idx5,2),pl(idx5,3),'g*','MarkerSize',12)
 hold on
 plot3(pl(idx6,1),pl(idx6,2),pl(idx6,3),'rx','MarkerSize',12)
            