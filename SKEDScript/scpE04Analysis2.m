clear
addpath('../function'); 

load('E04WBFXGNClinImmuneDiffCorr.mat'); 
load('E04WBFXGNClinImmuneDiffCorr2.mat'); 
load('E04WBFXGNClinImmuneChiSQ.mat'); 
load('E04WBFXGNClinImmuneMPATS.mat'); 

sName = 'E04BMFXGN'; 

P1 = strucChiSQ.Pval; 
P2 = strucDiffCorr.Pval;
P3 = strucDiffCorr2.Pval; 
P4 = strucMPATS.Pvalue;

P1(P1 == 0) = 10^-22; 

P2(P2 == 0) = 10^-22; 
P3(P3 == 0) = 10^-22; 

P4(P4 == 0) = 10^-22; 

mConsensus = log(P1)+log(P2) + log(P3) + log(P4);
mConsensus(isnan(mConsensus)) = 0; 

mEdgeScore = squareform(-mConsensus); 

mScore = sum(mEdgeScore); 
[a b] = sort(mScore,'descend');

fileName = ['RNKFiles/' sName 'qval.rnk'];

fileID = fopen(fileName,'w');
    
for i = 1:length(a)
    fprintf(fileID, [strucMPATS.Names{b(i)}(3:end-1) '\t' num2str(a(i)) '\n']);
end
fclose(fileID);