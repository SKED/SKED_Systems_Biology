clear
addpath('../function');

load('E04CompleteData.mat'); 
cLabels = {'RMe','RFa','RIc','RSb'};

sSevere = {'RMe','RFa'};
sMild = {'RIc','RSb'};

sDate = '2013-11-20 00:00:00.0'; 
for i = 1:3
    Idx2 = find(strcmp(sDate,oExplorer.Data{i,18}.DataPrimitive.Time));
    n = length(oExplorer.Data{i,18}.DataPrimitive.Time); 
    temp = 1:n ~= Idx2; 
    oExplorer.Data{i,18}.DataPrimitive.Time = oExplorer.Data{i,18}.DataPrimitive.Time(temp);
    oExplorer.Data{i,18}.DataPrimitive.Table = oExplorer.Data{i,18}.DataPrimitive.Table(:,temp);
end

oExplorerAggregated = MDBAnalysis(); 

cDataTypes = {'Clinical','Immunology','Functional Genomics','Lipidomics'}; 
mIdxImmune = oExplorer.findDataType('Immunology');  %%%%find the index of Immunology data within oExplorer
mIdxClinical = oExplorer.findDataType('Clinical');  %%%%find the index of Clinical data within oExplorer
oExplorerAggregated = MDBAnalysis(); 

for i = 1:4 
    %%%aggregateTimeSeries is used to align different type series by
    %%%collection type and keep the intersection of collection dates. 
    %%%For example, clinical is collected on day 1 to 100, and FXGN whole
    %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
    %%%series are from day 1 2 5 8 19. 
    
    oExplorerAggregated.Data{i} = oExplorer.aggregateTimeSeries(i,[mIdxClinical(i,:) mIdxImmune(i,:),18]);
end

%%%Filter out time series that contains missing data. 

oExplorerAggregated = oExplorerAggregated.filterMissingData(); 

%%%Filter out time series that do not appear for all the subjects. 

oExplorerAggregated = oExplorerAggregated.filterSharedVariables();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   

mDate = []; 
for i = 1:4 
        temp = datenum(oExplorerAggregated.Data{i}.DataPrimitive.Time); 
        [a b] = sort(temp,'ascend');
        mDate(i,:) = a;
end 

mIntersectDate = mDate; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
oExplorerAggregated = MDBAnalysis(); 

for i = 1:4 
    %%%aggregateTimeSeries is used to align different type series by
    %%%collection type and keep the intersection of collection dates. 
    %%%For example, clinical is collected on day 1 to 100, and FXGN whole
    %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
    %%%series are from day 1 2 5 8 19. 
    
    oExplorerAggregated.Data{i} = oExplorer.aggregateTimeSeries(i,[mIdxClinical(i,:)]);
end

%%%Filter out time series that contains missing data. 

oExplorerAggregated = oExplorerAggregated.filterMissingData(); 

%%%Filter out time series that do not appear for all the subjects. 

oExplorerAggregated = oExplorerAggregated.filterSharedVariables();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
Expression = []; 
    for i = 1:length(cLabels)
            temp = datenum(oExplorerAggregated.Data{i}.DataPrimitive.Time); 
            [a b] = sort(temp,'ascend'); 
            Expression{i} = oExplorerAggregated.Data{i}.DataPrimitive.Table(:,b);
    end 

mDate = []; 
for i = 1:4 
        temp = datenum(oExplorerAggregated.Data{i}.DataPrimitive.Time); 
        [a b] = sort(temp,'ascend');
        mDate(i,:) = a;
end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sVarName = 'x_parasites_'; 
g1 = [1:2];
g2 = [3:4];
h = figure('units','normalized','outerposition',[0 0 1 1]);
nLW = 3;
idx = find(strcmp(sVarName,oExplorerAggregated.Data{1}.DataPrimitive.VarNames));

for i = 1:4 
    
    [temp ia ib] = intersect(mDate(i,:),mIntersectDate(i,:));
    ha(i) = subplot(4,1,i);
    
    temp = Expression{i}(idx,:); 
    temp = log(temp); 
    temp(temp<0)= 0;
    plot(temp,'bo-','LineWidth',2);
    hold on 
    
    plot(ia,temp(ia),'rd','MarkerSize',15,'LineWidth',2);
    
    if i == 4 
    xlabel('Day Post-Inoculation');
    end
    
    if i < 4 
        set(gca,'xtick',[]);
    end
    
    if i == 1
        title('Experimental Time Line');
    end
    set(gca,'FontSize',18)
    
end    

pos = get(ha, 'position');
dim = cellfun(@(x) x.*[1.2 1 1 0.8], pos, 'uni',0);

for i = 1:4 
    a = annotation(h, 'textbox', dim{i}, 'String', cLabels{i}, 'FitBoxToText','on');
    a.FontSize = 18; 
end

height = pos{1}(2) + pos{1}(4) - pos{4}(2);
h3=axes('position',[pos{4}(1)*0.75 pos{4}(2) pos{4}(3) height],'visible','off');
h_label=ylabel('Log 10 Parasites/ul','visible','on','FontSize',22);

funPrintImage(h,'Paper_Figure/Figure_1_A');