%% Data Primitives JSON Examples
%
%% Publish to HTML command
% To publish the following as an HTML file run the following command:
% 
% publish('scpJSONExamples1.m', 'figureSnapMethod', 'print',...
%       'outputDir', ['..' filesep 'Data' filesep 'JSON']);

%% Load the data into RAM

clear; clc; close all;

disp('Calling the script that loads data in RAM...')
if ~(exist('cStructures','var'))
    scpMultiDataLoad;
end

%% Look at loaded experiments
%
% All downloaded data has been loaded into the variable cStructure.
% Now look at which experiments have been downloaded:
%

cExperiments = funGetExperimentNames(cStructures);
fprintf('Experiments that have been loaded are:\n\t');
for j=1:length(cExperiments)
        fprintf(' %s ',cExperiments{j});
end
fprintf('\n');
%% Example of Export to Time Series JSON format
%
% This example uses transcriptomic information from Experiment E04.
%
% The following code finds the transcriptomic time series of interest.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sExp = 'E04';
sSubjects = 'All'; sTissue = 'Blood';

sProtocol = 'FG_GENE_EXP_RAW2'; 
disp('-----------------------------------------------')
disp('Get functional genomic data for all subjects')
strucData_E04 = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
% 1) RMe14  2)RFa14 3) RIc14 4) RSb14
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCYB';
strucData_E04 = funParasiteGeneRemoval(strucData_E04,sIdentifier);


%% Writing to JSON File Examples
% The following code writes transcriptomic data to external JSON files.
%
for i = 1:size(strucData_E04.cSubject,2)
            cSubject{i} = strucData_E04.cSubject{1,i}{1};
end

sOutFile = ['..' filesep 'Data' filesep 'JSON'];

%Truncated Example with two time points and two data points

funWriteJSONFile(strucData_E04,cSubject,'Small',sOutFile)

%Complete transcriptomic time series

%funWriteJSONFile(strucData_E04,cSubject,'',sOutFile)

%% Load the complete JSON file examples
clearvars strucData;
%strucData = funReadJSONTSFile(cSubject,sOutFile );
%strucData = funReadJSONTSFile(cSubject,sOutFile );

%% Differential expression analysis
sExp = 'E04';
sSubjects = 'All';
sSpecies = 'Mm';
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCYB';
strucData_E04 = funParasiteGeneRemoval(strucData_E04,sIdentifier);

for i = 1:size(strucData_E04.cSubject,2)
            disp(['Subject ' num2str(i) ': ', strucData_E04.cSubject{1,i} ])
end
% 1) RMe14  2)RFa14 3) RIc14 4) RSb14
disp('-----------------------------------------------')
disp('Define groups for differential expression analysis');
sTPDesc = 'AP'; % 1) RMe14  2)RFa14 3) RIc14 4) RSb14
g1 = {1,1,1,1}; sG1Desc = 'Baseline'; %E04 TP1 baseline
g2 = {2,2,2,2}; sG2Desc = 'Peak Parasitemia'; %E04 TP2 Peak Parasitiemia
disp('-----------------------------------------------')
disp(['This is a comparison between ', sG1Desc, ' and ', sG2Desc])

disp('-----------------------------------------------')
disp('Remove low count genes and conduct differential expression analysis')
nCount = 10; 
sPrint = 'off';
[t_DP_DataNorm_E04_AP t_DP_tFC_E04_AP ] = funDataNormFC(strucData_E04,g1,g2,sG1Desc,sG2Desc, nCount,sExp, sSpecies,sTPDesc);
t_DP_Report_E04_AP = funDeSeq_TP(strucData_E04 ,g1,g2,sG1Desc,sG2Desc, nCount,sExp, sSpecies,sTPDesc,sPrint);
[tDataNorm_E04_AP tFC_E04_AP ] = funDataNormFC(strucData_E04,g1,g2,sG1Desc,sG2Desc, nCount,sExp, sSpecies,sTPDesc);
tReport_E04_AP = funDeSeq_TP(strucData_E04 ,g1,g2,sG1Desc,sG2Desc, nCount,sExp, sSpecies,sTPDesc,sPrint);

tDataNorm_E04_AP(1:4.,1:4)
t_DP_DataNorm_E04_AP(1:4.,1:4)

t_DP_Report_E04_AP = sortrows(t_DP_Report_E04_AP, 'FoldChange', 'descend');

%% Export to Graph Data Primitive

sFileName = [sOutFile, filesep 'exampleGraphE04FCRRankOutput.json'] ;
 
funWriteGraphFile(sFileName, t_DP_Report_E04_AP);

system(['head -100 ' sFileName ] ) ;

%% functions

function funWriteGraphFile(sFileName,t_DP_Report_E04_AP)
     outFile = fopen(sFileName ,'w');
    fprintf(outFile, '{\n');

    % Add header information
    fprintf(outFile, '\t"_index": "uga_e04",\n');
    fprintf(outFile, '\t"_type": "data",\n');
    fprintf(outFile, '\t"_id": "38394171",\n');
    fprintf(outFile, '\t"_version": 6,\n');
    fprintf(outFile, '\t"_score": 1,\n');
    fprintf(outFile, '\t"_routing": "888",\n');
    fprintf(outFile, '\t"_parent": "888",\n');
    fprintf(outFile, '\t"_source": {\n');
    fprintf(outFile, '\t\t"data_primitive": {\n');
    fprintf(outFile, '\t\t\t"type": "graph",\n');

    % Add Metadata information
    fprintf(outFile, '\t\t\t"metadata": {\n');
    fprintf(outFile, '\t\t\t\t"experiment": {\n');
    fprintf(outFile, '\t\t\t\t\t"id_experiment": "%s",\n','E04');
    fprintf(outFile, '\t\t\t\t\t"experiment_input_data": "%s",\n','gene expression levels');
    fprintf(outFile, '\t\t\t\t\t"rank_based_on": "%s"\n','FoldChange');
    fprintf(outFile, '\t\t\t\t\t}\n');
    fprintf(outFile, '\t\t\t},\n'); % closes metadata
    %add graph vertex data
    fprintf(outFile, '\t\t\t"data": [\n');
    fprintf(outFile, '\t\t\t\t{\n');
    fprintf(outFile, '\t\t\t\t\t"metadata":[\n');
        fprintf(outFile, '\t\t\t\t\t{\n');
        fprintf(outFile, '\t\t\t\t\t\t"term": "%s",\n', t_DP_Report_E04_AP.Properties.VariableNames{4});
        fprintf(outFile, '\t\t\t\t\t},\n'); 
        fprintf(outFile, '\t\t\t\t\t{\n');
        fprintf(outFile, '\t\t\t\t\t\t"term": "%s",\n', 'gene_name');
        fprintf(outFile, '\t\t\t\t\t},\n');
    for i = 1:6
        cColumn = {1,2,3,5,6,7};
        fprintf(outFile, '\t\t\t\t\t{\n');
        fprintf(outFile, '\t\t\t\t\t\t"term": "%s",\n',t_DP_Report_E04_AP.Properties.VariableNames{cColumn{i}} );
        if cColumn{i} == size(t_DP_Report_E04_AP.Properties.VariableNames,2)
            fprintf(outFile, '\t\t\t\t\t}\n');
        else
            fprintf(outFile, '\t\t\t\t\t},\n'); 
        end
    end  
    fprintf(outFile, '\t\t\t\t\t],\n');% end metadata
    

    for j = 1:size(t_DP_Report_E04_AP,1)
        fprintf(outFile, '\t\t\t\t\t{\n');
        fprintf(outFile, '\t\t\t\t\t"%i": [\n', j); 
        fprintf(outFile, '\t\t\t\t\t\t["%f"], ', t_DP_Report_E04_AP{j,4} );
        fprintf(outFile, '["%s"], ', t_DP_Report_E04_AP.Properties.RowNames{j}  ); 
        fprintf(outFile, '["%f"], ', t_DP_Report_E04_AP{j,1} );         
        fprintf(outFile, '["%f"], ', t_DP_Report_E04_AP{j,2} );
        fprintf(outFile, '["%f"], ', t_DP_Report_E04_AP{j,3} );
        fprintf(outFile, '["%f"], ', t_DP_Report_E04_AP{j,5} );
        fprintf(outFile, '["%f"], ', t_DP_Report_E04_AP{j,6} );
        fprintf(outFile, '["%f"], ', t_DP_Report_E04_AP{j,7} );
        if j == size(t_DP_Report_E04_AP,1)
            fprintf(outFile, '\n\t\t\t\t\t\t\t]\n');
        else
            fprintf(outFile, '\n\t\t\t\t\t      ]\n'); 
            fprintf(outFile, '\t\t\t\t\t},\n');           
        end
        
    end
 
    fprintf(outFile, '\t\t\t\t\t]\n');% close vertex
    
    fprintf(outFile, '\t\t\t\t}\n'); 
    fprintf(outFile, '\t\t\t]\n');   % close data
    fprintf(outFile, '\t\t}\n');
    fprintf(outFile, '\t}\n');
    fprintf(outFile, '}\n');

    fclose(outFile);
end


function strucData = funReadJSONTSFile(varargin)
    cSubject = varargin{1};
    sOutFile = varargin{2};

    for m = 1: size(cSubject,2)

        sFileName = [sOutFile filesep 'exampleTimeSeriesE04' ,cSubject{m}, 'RawTranscriptomics.json'] ;
        sFiletext = fileread(sFileName);
        % read in the JSON format into a data structure
        structData=jsondecode(sFiletext);
        %initialize cell arrays to hold data  
        cRawCounts = {}; cTimeInit = {}; cTime = {}; VarNames = {};
        % store data in cell arrays
        if strcmp(structData.x_source.data_primitive.type,'time_series')
            %add reading in Metadata 

            cExperiment =  structData.x_source.data_primitive.metadata.experiment.id_experiment;
            sSubject = structData.x_source.data_primitive.metadata.subject.id_subject;

            %shows the time stamps, gene expression values and gene names
            for i = 1:size(structData.x_source.data_primitive.data)
                if i == 1
                    for j = 1: size(structData.x_source.data_primitive.data{1,1}.metadata)
                        VarNames{j} = structData.x_source.data_primitive.data{1,1}.metadata(j).term;
                    end
                else
                    for l = 2:size(structData.x_source.data_primitive.data,1)
                        sTime = fieldnames(structData.x_source.data_primitive.data{i,1});
                        cTime{l-1} = sTime{1};
                        timeStructName{1} = ['structData.x_source.data_primitive.data{', num2str(i) ,',1}.' sTime{1} ];
                    
                    for j = 1: size(eval(timeStructName{1}))
                        strucData.cRawCounts{1,m}(j,l-1) = eval([timeStructName{1} '{' num2str(j) '}'] );
                    end

                end
                end
        end
            strucData.VarNames{1,m} = VarNames';
            strucData.cTime{1,m} = cTime;
            strucData.cSubject{1,m} = sSubject;
            strucData.Experiment{1,m} = cExperiment;
    end

    end
end

function funWriteJSONFile(varargin)

    strucData_E04 = varargin{1};
    cSubject = varargin{2};
    sFileSize = varargin{3};
    sOutFile = varargin{4};

    for m = 1: size(cSubject,2)

        sFileName = [sOutFile filesep 'exampleTimeSeriesE04' ,cSubject{m}, 'RawTranscriptomics', sFileSize, '.json'] ;

        outFile = fopen(sFileName ,'w');
        fprintf(outFile, '{\n');

        % Add header information
        fprintf(outFile, '\t"_index": "uga_e04",\n');
        fprintf(outFile, '\t"_type": "data",\n');
        fprintf(outFile, '\t"_id": "38394173",\n');
        fprintf(outFile, '\t"_version": 6,\n');
        fprintf(outFile, '\t"_score": 1,\n');
        fprintf(outFile, '\t"_routing": "888",\n');
        fprintf(outFile, '\t"_parent": "888",\n');
        fprintf(outFile, '\t"_source": {\n');
        fprintf(outFile, '\t\t"data_primitive": {\n');
        fprintf(outFile, '\t\t\t"type": "time_series",\n');

        % Add Metadata information
        fprintf(outFile, '\t\t\t"metadata": {\n');
        fprintf(outFile, '\t\t\t\t"experiment": {\n');
        fprintf(outFile, '\t\t\t\t\t"id_experiment": "%s"\n',strucData_E04.Experiment{m});
        fprintf(outFile, '\t\t\t\t\t},\n');
        fprintf(outFile, '\t\t\t\t"subject": {\n');
        fprintf(outFile, '\t\t\t\t\t"id_subject": "%s"\n',cSubject{m});
        fprintf(outFile, '\t\t\t\t\t}\n'); % closes subject

        fprintf(outFile, '\t\t\t},\n'); % closes metadata

        % Add time series data

        if strcmp(sFileSize,'Small')
            maxRow = 3;
            maxTS = 2; 
        else
            maxRow = size(strucData_E04.cRawCounts{1,1},1);
            maxTS = size(strucData_E04.cTime{1,m},2);
        end
        
         fprintf(outFile, '\t\t\t"data": [\n');
         fprintf(outFile, '\t\t\t\t\t{\n'); % starts metadata bracket
         fprintf(outFile, '\t\t\t\t\t"metadata": [\n'); % starts metadata array
         
         for j = 1:maxRow
                fprintf(outFile, '\t\t\t\t\t\t{\n');
                fprintf(outFile, '\t\t\t\t\t\t\t"term": "%s"\n', strucData_E04.VarNames{1,m}{j} );
 
                if j == maxRow
                    fprintf(outFile, '\t\t\t\t\t\t}\n'); % end metadata list of term names
                    fprintf(outFile, '\t\t\t\t\t]\n');
                    fprintf(outFile, '\t\t\t\t\t},\n');% end metadata bracket
                else
                    fprintf(outFile, '\t\t\t\t\t\t\t},\n');
                end
         end
         
         for i = 1:maxTS
            fprintf(outFile, '\t\t\t\t\t{\n');% start time Series
            fprintf(outFile, '\t\t\t\t\t"%s": [\n', strucData_E04.cTime{1,m}{1} ); 
            fprintf(outFile, '\t\t\t\t\t\t' );
            for j = 1:maxRow
                fprintf(outFile, '["%i"]',strucData_E04.cRawCounts{1,m}(j,i) );
                if j == maxRow
                    fprintf(outFile, '\n\t\t\t\t\t\t]\n'); % end value
                else
                    fprintf(outFile, ', ');
                end

             end
             
            if i == maxTS
                fprintf(outFile, '\t\t\t\t\t}\n');
                
            else
                fprintf(outFile, '\t\t\t\t\t},\n'); 
                %fprintf(outFile, '\t\t\t\t\t]\n'); % end data
            end

        end
   
            fprintf(outFile, '\t\t\t]\n'); % end data
       
       
        fprintf(outFile, '\t\t}\n');
        fprintf(outFile, '\t}\n');
        fprintf(outFile, '}\n');

        fclose(outFile);

        % Examine the Time Series JSON file
        if strcmp(sFileSize,'Small')
            system(['cat -n ' sFileName ] ) ;
        end
        
    end


end
