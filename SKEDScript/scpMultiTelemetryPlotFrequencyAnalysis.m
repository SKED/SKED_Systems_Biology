% This script creates a 3 x 1 plot for each monkey in the telemetry
% experiments for temperature and activity. Where the first plot is pre-infection, 
% second is liver stage, and last plot is blood stage.
% Note: 3 days are taken from each stage from saved files of data per
%       monkey.
tic
addpath('../SKEDFunction')
addpath('../SKEDClass')
addpath('../Telemetry/TELEMETRY_DATA')
sExp = {'E30'; 'E07B'; 'E06'}; % 'E07A'; Current inclusive list of telemetry experiments
oTelemetry = clsTelemetryResults; % Define Object
% sTime = 'DD-MON-yy HH24:MI:SS'; %Specify date format for database
nFigure = 40;
lambda = 1000; % Smoothing parameter for Hodrick Prescott filter
% Initialize vectors for features from FFT
X = []; Y = []; Z = [];
% Loop through all experiments
for i = 1:size(sExp)
    %oTelemetry.Experiment = sExp(i,:);
    sExperimentID = sExp{i}; 
                    
    % Define the subjects per experiment
    switch sExp{i}
        case 'E30'
            cMonkey = {
                         % nSubjectID - Subject ID (Database ID)
                 1403, 'RKy15';
                 1406, 'REd16'
             };
        case 'E07A'  
             cMonkey = {
                         % nSubjectID - Subject ID (Database ID)
                 1402, '12C53';
                 1404, 'H12C59'; 
                 1405, 'H12C8'; 
                 1407, '12C44'; 
%                  1408, '11C131'; 
                 1409, '11C166'; 
                 1410, 'H12C50' %1410 E07A only
              };
        case 'E07B'
            cMonkey = {
                        % nSubjectID - Subject ID (Database ID)
                 %1401, '12C136'; % 1401 E07B only
                 1402, '12C53';
                 1404, 'H12C59'; 
                 1405, 'H12C8'; 
                 1407, '12C44'; 
                 1408, '11C131'; 
                 1409, '11C166'; 
            };
        nDownsamplePts = 6;
        BPValidDataThreshold = 300;
        case 'E06'
            cMonkey = {
                        % nSubjectID - Subject ID (Database ID)
                1912, 'RIh16'; 
                1915, 'RTe16'; 
                1916, 'RCl15'; 
                1918, 'RUf16'; 
            };
    end
    
    % Loop through each monkey in the specified experiment
    for j = 1:length(cMonkey)
        nSubjectID = cMonkey{j,1};
        nFigure = nFigure + 1;
        
        % Clear variables from last loop
        idxTemperaturePreinfection = [];
        idxTemperatureLiverStage = [];
        idxTemperatureBloodStage = [];
        idxActivityPreinfection = [];
        idxActivityLiverStage = [];
        idxActivityBloodStage = [];
        
        % Definition of time points for different stages
        switch sExperimentID
        case 'E30'
            dtTP(1) = datenum('7/28/2016');
            dtTP(2) = datenum('7/31/2016');
            dtTP(3) = datenum('8/12/2016');
            dtTP(4) = datenum('8/15/2016');% Inoculation. TP numbers after this are increased by 1
            dtTP(5) = datenum('8/18/2016');% Parasitemia
            dtTP(6) = datenum('8/21/2016');
            dtInnoculation = datenum('8/11/2016');
        case 'E07A'
            dtTP(1) = datenum('10/21/2016'); %Day after Implant is turned on
            dtTP(2) = datenum('10/24/2016'); 
            dtTP(3) = datenum('11/01/2016');
            dtTP(4) = datenum('11/05/2016'); % Inoculation. TP numbers after this are increased by 1
            dtTP(5) = datenum('11/11/2016'); % Parasitemia should have appeared here
            dtTP(6) = datenum('11/14/2016'); % Day before implants were turned off
            dtInnoculation = datenum('11/01/2016');
        case 'E07B'
            dtTP(1) = datenum('1/11/2017'); 
            dtTP(2) = datenum('1/14/2017');
            dtTP(3) = datenum('1/21/2017');
            dtTP(4) = datenum('1/24/2017'); 
            dtTP(5) = datenum('1/27/2017'); 
            dtTP(6) = datenum('1/30/2017'); 
            dtInnoculation = datenum('1/20/2016');
        case 'E06'
            dtTP(1) = datenum('3/23/2017');
            dtTP(2) = datenum('3/26/2017');
            dtTP(3) = datenum('4/03/2017');
            dtTP(4) = datenum('4/06/2017'); 
            dtTP(5) = datenum('4/09/2017'); 
            dtTP(6) = datenum('4/12/2017'); 
            dtInnoculation = datenum('4/03/2016');
        end
        
        % Load data for monkey
        sFileName = [sExperimentID '_Telemetry_Summary_Results_' num2str(cMonkey{j,1}) '.mat'];
        load(sFileName);
        
        % Set Indeces for different stages of infection
        idxTemperaturePreinfection = and(TemperatureTime >= dtTP(1), TemperatureTime <= dtTP(2));
        idxTemperatureLiverStage = and(TemperatureTime >= dtTP(3), TemperatureTime <= dtTP(4));
        idxTemperatureBloodStage = and(TemperatureTime >= dtTP(5), TemperatureTime <= dtTP(6));
        
        % Smooth Temperature and Activity Data using Hodrick Prescott Filter
%         HPTemperatureMeanPreinfection = funHodrickPrescott(TemperatureMean(idxTemperaturePreinfection),lambda);
%         HPTemperatureMeanLiverStage = funHodrickPrescott(TemperatureMean(idxTemperatureLiverStage),lambda);
%         HPTemperatureMeanBloodStage = funHodrickPrescott(TemperatureMean(idxTemperatureBloodStage),lambda);

        % Use linear interpolation to create a uniformly sampled vector
%         [dtInterpolatedDatesPreinfection,InterpolatedDataPreinfection,nTotalHoursPreinfection,FractionMissingPointsPreinfection] = funTelemetryInfectionStageInterpolation(TemperatureTime(idxTemperaturePreinfection),HPTemperatureMeanPreinfection);
%         [dtInterpolatedDatesLiverStage,InterpolatedDataLiverStage,nTotalHoursLiverStage,FractionMissingPointsLiverStage] = funTelemetryInfectionStageInterpolation(TemperatureTime(idxTemperatureLiverStage),HPTemperatureMeanLiverStage);
%         [dtInterpolatedDatesBloodStage,InterpolatedDataBloodStage,nTotalHoursBloodStage,FractionMissingPointsBloodStage] = funTelemetryInfectionStageInterpolation(TemperatureTime(idxTemperatureBloodStage),HPTemperatureMeanBloodStage);
        try
            [dtInterpolatedDatesPreinfection,InterpolatedDataPreinfection,nTotalHoursPreinfection] = funLinInterpSmooth(TemperatureTime(idxTemperaturePreinfection),TemperatureMean(idxTemperaturePreinfection),60,lambda,false);
            nTotalHoursPreinfection = nTotalHoursPreinfection/60;
            [dtInterpolatedDatesLiverStage,InterpolatedDataLiverStage,nTotalHoursLiverStage] = funLinInterpSmooth(TemperatureTime(idxTemperatureLiverStage),TemperatureMean(idxTemperatureLiverStage),60,lambda,false);
            nTotalHoursLiverStage = nTotalHoursLiverStage/60;
            [dtInterpolatedDatesBloodStage,InterpolatedDataBloodStage,nTotalHoursBloodStage] = funLinInterpSmooth(TemperatureTime(idxTemperatureBloodStage),TemperatureMean(idxTemperatureBloodStage),60,lambda,false);
            nTotalHoursBloodStage = nTotalHoursBloodStage/60;       
        catch ex
            disp(ex)
        end
        % Perform frequency analysis
        [FrequencyPreinfection,MagnitudeFrequencyPreinfection] = funTelemetryFrequencyAnalysis(InterpolatedDataPreinfection,nTotalHoursPreinfection);
        [FrequencyLiverStage,MagnitudeFrequencyLiverStage] = funTelemetryFrequencyAnalysis(InterpolatedDataLiverStage,nTotalHoursLiverStage);
        [FrequencyBloodStage,MagnitudeFrequencyBloodStage] = funTelemetryFrequencyAnalysis(InterpolatedDataBloodStage,nTotalHoursBloodStage);
        
        % Preinfection: Select top 8 frequencies based on magnitude
         [SortedPreinfection,idxPreinfection] = sort(MagnitudeFrequencyPreinfection,'descend');
         PreMag = SortedPreinfection(1:8);
         idxPreinfection = idxPreinfection(1:8);
         PreFrequency = FrequencyPreinfection(idxPreinfection);

         % Liver Stage: Select top 8 frequencies based on magnitude
         [SortedLiverStage,idxLiverStage] = sort(MagnitudeFrequencyLiverStage,'descend');
         LiverMag = SortedLiverStage(1:8);
         idxPreinfection = idxLiverStage(1:8);
         LiverFrequency = FrequencyLiverStage(idxLiverStage);
         
         % Blood Stage: Select top 8 frequencies based on magnitude
         [SortedBloodStage,idxBloodStage] = sort(MagnitudeFrequencyBloodStage,'descend');
         BloodMag = SortedBloodStage(1:8);
         idxBlood = idxBloodStage(1:8);
         BloodFrequency = FrequencyBloodStage(idxBloodStage);
        
        nEnd = 5; 
        X = [X; FrequencyPreinfection(2:nEnd)'];
        Y = [Y; FrequencyLiverStage(2:nEnd)'];
        Z = [Z; FrequencyBloodStage(2:nEnd)'];

        % Figure Settings
        %figure(nFigure);
        %set(gcf,'color','w');
 
        % Frequency Analysis: Plot top frequencies for temperature fft
        %polar(PreFrequency(2:8),PreMag(2:8),'b*');%,'LineWidth',2); 
        %hold on;
        %polar(LiverFrequency(2:8),LiverMag(2:8), 'g*');%),'LineWidth',2); 
        %hold on;
        %polar(BloodFrequency(2:8),BloodMag(2:8), 'r*');%,'LineWidth',2);
        %hold on;
        %set(subplot(3,1,1),'ylim', [0,100])
%         ylabel(subplot(3,1,1),({'Power'}))
%         ylabel(subplot(3,1,1),({'Frequency'}))
        %sTitle = [sExperimentID ': ' cMonkey{j,2} ' Preinfection Frequency Analysis'];
        %title(sTitle);
        %set(gca,'FontSize',16);

       
        %sFileName = [sExperimentID 'InfectionStages_FrequencyAnalysis_' num2str(cMonkey{j,1}) '_' cMonkey{j,2} '.fig'];
        %savefig(sFileName);
        %close all;
    end
end

Q = [X;Y;Z]; 
Q = (Q-mean(Q))./std(Q); 
[V,D] = eig(Q'*Q);  
R = Q*V(:,end-1:end); 
plot(R(:,1), R(:,2), 'ro');
% nExp = [1 1 2 2 2 2 2 2 3 3 3 3 4 4 5 5 5 5 5 5 6 6 6 6 7 7 8 8 8 8 8 8 9 9 9 9];
nExp = [1 1 1 1 1 1 1 1 2 2 2 2 1 1 1 1 1 1 1 1 2 2 2 2 1 1 1 1 1 1 1 1 2 2 2 2];
sColor = ['r','b','g']; 
sMarker = ['*','o','^']; 
clf
for i = 1:9 
    idx = nExp == i; 
    switch i
        case 1, sMarker = '*'; sColor = 'b';
        case 2, sMarker = 'o'; sColor = 'b';
        case 3, sMarker = '^'; sColor = 'b';
        case 4, sMarker = '*'; sColor = 'g';
        case 5, sMarker = 'o'; sColor = 'g';
        case 6, sMarker = '^'; sColor = 'g';
        case 7, sMarker = '*'; sColor = 'r';
        case 8, sMarker = 'o'; sColor = 'r';
        case 9, sMarker = '^'; sColor = 'r';
    end
    sOption = [sColor sMarker];
    plot(R(idx,1), R(idx,2), sOption); hold on
end

toc