addpath('../function'); 


[num text raw] = xlsread('DifferentialExpression\EnrichmentSummary.xlsx','SevereUp_Path');
FS = 8;
FS2 = 12;
cName ={};
for i = 1:size(text,1)
    cName{i} = strrep(text{i,1},'_',' ');
    cName{i} = strrep(cName{i},'REACTOME','');
    cName{i} = strrep(cName{i},'KEGG','');
    cName{i} = strrep(cName{i},'HALLMARK','');
    cName{i} = strrep(cName{i},'BIOCARTA','');



end

h = figure('units','normalized','outerposition',[0 0 1 0.75]);
subplot(1,2,1)
score = -log(num(:,6)); 
score = sort(score,'ascend'); 

barh(score,'r')
set(gca,'FontSize',FS);
ylim([0 length(score)+1])
ax = gca; 
ax.YTick = (1:length(score)); 
ax.YTickLabel = fliplr(cName(2:end));
xlabel('-log(q-value)'); 
set(ax,'FontSize',FS);
title('S Differential Up-Regulation','FontSize',FS2);

[num text raw] = xlsread('DifferentialExpression\EnrichmentSummary.xlsx','SevereDown_Path');
cName ={};

for i = 1:size(text,1)
    cName{i} = strrep(text{i,1},'_',' ');
    cName{i} = strrep(cName{i},'REACTOME','');
    cName{i} = strrep(cName{i},'KEGG','');
    cName{i} = strrep(cName{i},'HALLMARK','');
    cName{i} = strrep(cName{i},'BIOCARTA','');


end

subplot(1,2,2)

c = categorical(text(2:23,1));
score = -log(num(:,6)); 
score = sort(score,'ascend'); 

barh(score,'b')
ylim([0 length(score)+1])
set(gca,'FontSize',FS);

ax = gca; 
ax.YTick = (1:length(score)); 
ax.YTickLabel = fliplr(cName(2:end));
xlabel('-log(q-value)'); 
set(ax,'FontSize',FS);
title('S Differential Down-Regulation','FontSize',FS2);

funPrintImage(h,'Paper_Figure\SevereUpDownPath');

[num text raw] = xlsread('DifferentialExpression\EnrichmentSummary.xlsx','MildUp_Path');
cName ={};

for i = 1:size(text,1)
    cName{i} = strrep(text{i,1},'_',' ');
    cName{i} = strrep(cName{i},'REACTOME','');
    cName{i} = strrep(cName{i},'KEGG','');
    cName{i} = strrep(cName{i},'HALLMARK','');
    cName{i} = strrep(cName{i},'BIOCARTA','');


end

h = figure('units','normalized','outerposition',[0 0 1 0.75]);
subplot(1,2,1)
score = -log(num(:,6)); 
score = sort(score,'ascend'); 

barh(score,'r')
ylim([0 length(score)+1])
set(gca,'FontSize',FS);

ax = gca; 
ax.YTick = (1:length(score)); 
ax.YTickLabel = fliplr(cName(2:end));
xlabel('-log(q-value)'); 
set(ax,'FontSize',FS);
title('M Differential Up-Regulation','FontSize',FS2); 

[num text raw] = xlsread('DifferentialExpression\EnrichmentSummary.xlsx','MildDown_Path');
cName ={};

for i = 1:size(text,1)
    cName{i} = strrep(text{i,1},'_',' ');
    cName{i} = strrep(cName{i},'REACTOME','');
    cName{i} = strrep(cName{i},'KEGG','');
    cName{i} = strrep(cName{i},'HALLMARK','');
    cName{i} = strrep(cName{i},'BIOCARTA','');


end

subplot(1,2,2)

score = -log(num(:,6)); 
score = sort(score,'ascend'); 

barh(score,'b')
ylim([0 length(score)+1])
set(gca,'FontSize',FS);

ax = gca; 
ax.YTick = (1:length(score)); 
ax.YTickLabel = fliplr(cName(2:end));
xlabel('-log(q-value)'); 
set(ax,'FontSize',FS);
title('M Differential Down-Regulation','FontSize',FS2); 

funPrintImage(h,'Paper_Figure\MildUpDownPath');
