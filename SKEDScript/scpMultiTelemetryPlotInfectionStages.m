% This script creates a 1x3 plot for each monkey in the telemetry
% experiments for temperature and activity. Where the first plot is pre-infection, 
% second is liver stage, and last plot is blood stage.
% Note: 3 days are taken from each stage from saved files of data per
%       monkey.

addpath('../SKEDFunction')
addpath('../SKEDClass')
addpath('../Telemetry/TELEMETRY_DATA')

sExp = {'E30'; 'E07A'; 'E07B'; 'E06'}; % Current inclusive list of telemetry experiments
oTelemetry = clsTelemetryResults; % Define Object
% sTime = 'DD-MON-yy HH24:MI:SS'; %Specify date format for database
nFigure = 40;
lambda = 100000; % Smoothing parameter for Hodrick Prescott filter

% Loop through all experiments
for i = 2:size(sExp)
    %oTelemetry.Experiment = sExp(i,:);
    sExperimentID = sExp{i}; 
                    
    % Define the subjects per experiment
    switch sExp{i}
        case 'E30'
            cMonkey = {
                         % nSubjectID - Subject ID (Database ID)
                 1403, 'RKy15';
                 1406, 'REd16'
             };
         ActivityAxisMax = 70;
        case 'E07A'  
             cMonkey = {
                         % nSubjectID - Subject ID (Database ID)
                 1402, '12C53';
                 1404, 'H12C59'; 
                 1405, 'H12C8'; 
                 1407, '12C44'; 
                 %1408, '11C131'; 
                 1409, '11C166'; 
                 1410, 'H12C50' %1410 E07A only
              };
          ActivityAxisMax = 100;
        case 'E07B'
            cMonkey = {
                        % nSubjectID - Subject ID (Database ID)
                 1401, '12C136'; % 1401 E07B only
                 1402, '12C53';
                 1404, 'H12C59'; 
                 1405, 'H12C8'; 
                 1407, '12C44'; 
                 1408, '11C131'; 
                 1409, '11C166';
            };
        ActivityAxisMax = 100;
        case 'E06'
            cMonkey = {
                        % nSubjectID - Subject ID (Database ID)
                 1912, 'RIh16'; 
                 1915, 'RTe16'; 
                 1916, 'RCl15'; 
                 1918, 'RUf16'; 
            };
                 ActivityAxisMax = 70;
    end
    
    % Loop through each monkey in the specified experiment
    for j = 1%2 : length(cMonkey)
        nSubjectID = cMonkey{j,1};
        nFigure = nFigure + 1;
        
        % Clear variables from last loop
        idxTemperaturePreinfection = [];
        idxTemperatureLiverStage = [];
        idxTemperatureBloodStage = [];
        idxActivityPreinfection = [];
        idxActivityLiverStage = [];
        idxActivityBloodStage = [];
        
        % Definition of time points for different stages
        switch sExperimentID
        case 'E30'
            dtTP(1) = datenum('7/28/2016');
            dtTP(2) = datenum('7/31/2016');
            dtTP(3) = datenum('8/12/2016');
            dtTP(4) = datenum('8/15/2016');% Inoculation. TP numbers after this are increased by 1
            dtTP(5) = datenum('8/18/2016');% Parasitemia
            dtTP(6) = datenum('8/21/2016');
            dtInnoculation = datenum('8/11/2016');
        case 'E07A'
            dtTP(1) = datenum('10/21/2016'); %Day after Implant is turned on
            dtTP(2) = datenum('10/24/2016'); 
            dtTP(3) = datenum('11/01/2016');
            dtTP(4) = datenum('11/05/2016'); % Inoculation. TP numbers after this are increased by 1
            dtTP(5) = datenum('11/11/2016'); % Parasitemia should have appeared here
            dtTP(6) = datenum('11/14/2016'); % Day before implants were turned off
            dtInnoculation = datenum('11/01/2016');
        case 'E07B'
            dtTP(1) = datenum('1/11/2017'); 
            dtTP(2) = datenum('1/14/2017');
            dtTP(3) = datenum('1/21/2017');
            dtTP(4) = datenum('1/24/2017'); 
            dtTP(5) = datenum('1/27/2017'); 
            dtTP(6) = datenum('1/30/2017'); 
            dtInnoculation = datenum('1/20/2016');
        case 'E06'
            dtTP(1) = datenum('3/23/2017');
            dtTP(2) = datenum('3/26/2017');
            dtTP(3) = datenum('4/03/2017');
            dtTP(4) = datenum('4/06/2017'); 
            dtTP(5) = datenum('4/09/2017'); 
            dtTP(6) = datenum('4/12/2017'); 
            dtInnoculation = datenum('4/03/2016');
        end
        
        % Load data for monkey
        sFileName = [sExperimentID '_Telemetry_Summary_Results_' num2str(cMonkey{j,1}) '.mat'];
        load(sFileName);
        
        % Set Indeces for different stages of infection
        idxTemperaturePreinfection = and(TemperatureTime >= dtTP(1), TemperatureTime <= dtTP(2));
        idxTemperatureLiverStage = and(TemperatureTime >= dtTP(3), TemperatureTime <= dtTP(4));
        idxTemperatureBloodStage = and(TemperatureTime >= dtTP(5), TemperatureTime <= dtTP(6));
        
        idxActivityPreinfection = and(ACCTime >= dtTP(1), ACCTime <= dtTP(2));
        idxActivityLiverStage = and(ACCTime >= dtTP(3), ACCTime <= dtTP(4));
        idxActivityBloodStage = and(ACCTime >= dtTP(5), ACCTime <= dtTP(6));
        
        % Smooth Temperature and Activity Data using Hodrick Prescott Filter
        HPACCMeanPreinfection = funHodrickPrescott(ACCSD(idxActivityPreinfection),lambda);
        HPACCMeanLiverStage = funHodrickPrescott(ACCSD(idxActivityLiverStage),lambda);
        HPACCMeanBloodStage = funHodrickPrescott(ACCSD(idxActivityBloodStage),lambda);       
       
        HPTemperatureMeanPreinfection = funHodrickPrescott(TemperatureMean(idxTemperaturePreinfection),lambda);
        HPTemperatureMeanLiverStage = funHodrickPrescott(TemperatureMean(idxTemperatureLiverStage),lambda);
        HPTemperatureMeanBloodStage = funHodrickPrescott(TemperatureMean(idxTemperatureBloodStage),lambda);
              
        % Figure Settings
        figure(nFigure);
        set(gcf,'color','w');
        

            % Preinfection: Plot Temperature and Activity
            subplot(1,3,1)
            hold on;
            yyaxis(subplot(1,3,1),'left')
            plot(ACCTime(idxActivityPreinfection),HPACCMeanPreinfection,'Color', 'r','LineWidth',2);
            hold on;
            set(subplot(1,3,1),'ylim', [0,ActivityAxisMax])
            ylabel(subplot(1,3,1),({'Activity'}))
            set(subplot(1,3,1),'YColor','r');

            yyaxis(subplot(1,3,1),'right')
            plot(TemperatureTime(idxTemperaturePreinfection),HPTemperatureMeanPreinfection,'Color','b','LineStyle','-','LineWidth',2)
            hold on;
            set(subplot(1,3,1),'ylim', [34,41])
            ylabel(subplot(1,3,1),({'Temperature'}))
            set(subplot(1,3,1),'YColor','b');
            hold on;
            datetick('x','mmm-dd');
            sTitle = [sExperimentID ': ' cMonkey{j,2} ' Preinfection'];
            title(sTitle);
            set(gca,'FontSize',16);

            % Liver Stage: Plot Temperature and Activity
            subplot(1,3,2)
            hold on;
            yyaxis(subplot(1,3,2),'left')
            plot(ACCTime(idxActivityLiverStage),HPACCMeanLiverStage,'Color', 'r','LineWidth',2);
            hold on;
            set(subplot(1,3,2),'ylim', [0,ActivityAxisMax])
            ylabel(subplot(1,3,2),({'Activity'}))
            set(subplot(1,3,2),'YColor','r');


            yyaxis(subplot(1,3,2),'right')
            plot(TemperatureTime(idxTemperatureLiverStage),HPTemperatureMeanLiverStage,'Color','b','LineStyle','-','LineWidth',2)
            hold on;
            set(subplot(1,3,2),'ylim', [34,41])
            ylabel(subplot(1,3,2),({'Temperature'}))
            set(subplot(1,3,2),'YColor','b');
            hold on;
            datetick('x','mmm-dd');
            sTitle = [sExperimentID ': ' cMonkey{j,2} ' Liver Stage'];
            title(sTitle);
            set(gca,'FontSize',16);

            % Blood Stage: Plot Temperature and Activity
            subplot(1,3,3)
            hold on;
            yyaxis(subplot(1,3,3),'left')
            plot(ACCTime(idxActivityBloodStage),HPACCMeanBloodStage,'Color', 'r','LineWidth',2);
            hold on;
            set(subplot(1,3,3),'ylim', [0,ActivityAxisMax])
            ylabel(subplot(1,3,3),({'Activity'}))
            set(subplot(1,3,3),'YColor','r');

            yyaxis(subplot(1,3,3),'right')
            plot(TemperatureTime(idxTemperatureBloodStage),HPTemperatureMeanBloodStage,'Color','b','LineStyle','-','LineWidth',2)
            hold on;
            set(subplot(1,3,3),'ylim', [34,41])
            ylabel(subplot(1,3,3),({'Temperature'}))
            set(subplot(1,3,3),'YColor','b');

            hold on;
            datetick('x','mmm-dd');
            sTitle = [sExperimentID ': ' cMonkey{j,2} ' Blood Stage'];
            title(sTitle);
            set(gca,'FontSize',16);

            sFileName = [sExperimentID 'HorizontalInfectionStages_' num2str(cMonkey{j,1}) '_' cMonkey{j,2} '.fig'];
            savefig(sFileName);
             close all;
    end
end

