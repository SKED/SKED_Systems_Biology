addpath('../function');
sExpName = 'E04'; %%%Experiment of Interest is E04 

sSevere = {'RMe','RFa'};
sMild = {'RIc','RSb'};
sFileName = [sExpName 'CompleteData.mat'];%%%%Define Save Destination and File Name 
cDataTypes = {'Clinical','Immunology','Functional Genomics','Lipidomics'}; 
load(sFileName) 
mIdxImmune = oExplorer.findDataType('Immunology');  %%%%find the index of Immunology data within oExplorer
mIdxClinical = oExplorer.findDataType('Clinical');  %%%%find the index of Clinical data within oExplorer

oExplorerAggregated = MDBAnalysis(); 

for i = 1:4 
    %%%aggregateTimeSeries is used to align different type series by
    %%%collection type and keep the intersection of collection dates. 
    %%%For example, clinical is collected on day 1 to 100, and FXGN whole
    %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
    %%%series are from day 1 2 5 8 19. 
    
    oExplorerAggregated.Data{i} = oExplorer.aggregateTimeSeries(i,[mIdxClinical(i,:) mIdxImmune(i,:) 18]);
end

%%%Filter out time series that contains missing data. 

oExplorerAggregated = oExplorerAggregated.filterMissingData(); 

%%%Filter out time series that do not appear for all the subjects. 

oExplorerAggregated = oExplorerAggregated.filterSharedVariables();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cLabels = {'RMe','RFa','RIc','RSb'};


sReference = 'x_hgb_';
xRef = []; 
yRef = []; 
for i = 1:4 
    xRef(i,:) = datenum(oExplorerAggregated.Data{i}.DataPrimitive.Time);
    idx = find(strcmp(oExplorerAggregated.Data{i}.DataPrimitive.VarNames,sReference));
    yRef(i,:) = oExplorerAggregated.Data{i}.DataPrimitive.Table(idx,:);
end


%%%%%Composite Graph%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cVarName = oExplorerAggregated.Data{1}.DataPrimitive.VarNames; 

for i = 1:length(cVarName)
    h = figure('units','normalized','outerposition',[0 0 1 1]);
    Expression = []; 
    for j = 1:4 
        idx = find(strcmp(oExplorerAggregated.Data{j}.DataPrimitive.VarNames,cVarName{i}));
        Expression(j,:) = oExplorerAggregated.Data{j}.DataPrimitive.Table(idx,:);
    end

    if isempty(Expression) == 0
        subplot(2,1,1)
        plot(xRef(1,:),Expression(1,:)','ro-','linewidth',2,'MarkerSize',15); 
        hold on 
        plot(xRef(2,:),Expression(2,:)','rx-','linewidth',2,'MarkerSize',15); 
        plot(xRef(3,:),Expression(3,:)','bo-','linewidth',2,'MarkerSize',15); 
        plot(xRef(4,:),Expression(4,:)','bx-','linewidth',2,'MarkerSize',15);
        legend(cLabels);
        
        ylabel('Abundance'); 

        xlabel('Time');

        title(cVarName{i});
        set(gca,'FontSize',18);

        subplot(2,1,2)
        plot(xRef(1,:),yRef(1,:)','ro-','linewidth',2,'MarkerSize',15); 
        hold on 
        plot(xRef(2,:),yRef(2,:)','rx-','linewidth',2,'MarkerSize',15); 
        plot(xRef(3,:),yRef(3,:)','bo-','linewidth',2,'MarkerSize',15); 
        plot(xRef(4,:),yRef(4,:)','bx-','linewidth',2,'MarkerSize',15);
        xlabel('Time');
        ylabel('hgb'); 
        set(gca,'FontSize',18);

    end
    sName = ['ClinImmuneOverlay/' cVarName{i}];
    funPrintImage(h,sName);
    close all
end 
