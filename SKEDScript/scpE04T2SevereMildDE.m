clear
addpath('../function');

load('E04CompleteData.mat'); 
cLabels = {'RMe','RFa','RIc','RSb'};

sSevere = {'RMe','RFa'};
sMild = {'RIc','RSb'};

sDate = '2013-11-20 00:00:00.0'; 
for i = 1:3
    Idx2 = find(strcmp(sDate,oExplorer.Data{i,18}.DataPrimitive.Time));
    n = length(oExplorer.Data{i,18}.DataPrimitive.Time); 
    temp = 1:n ~= Idx2; 
    oExplorer.Data{i,18}.DataPrimitive.Time = oExplorer.Data{i,18}.DataPrimitive.Time(temp);
    oExplorer.Data{i,18}.DataPrimitive.Table = oExplorer.Data{i,18}.DataPrimitive.Table(:,temp);
end

oExplorerAggregated = MDBAnalysis(); 

cDataTypes = {'Clinical','Immunology','Functional Genomics','Lipidomics'}; 
mIdxImmune = oExplorer.findDataType('Immunology');  %%%%find the index of Immunology data within oExplorer
mIdxClinical = oExplorer.findDataType('Clinical');  %%%%find the index of Clinical data within oExplorer
oExplorerAggregated = MDBAnalysis(); 

for i = 1:4 
    %%%aggregateTimeSeries is used to align different type series by
    %%%collection type and keep the intersection of collection dates. 
    %%%For example, clinical is collected on day 1 to 100, and FXGN whole
    %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
    %%%series are from day 1 2 5 8 19. 
    
    oExplorerAggregated.Data{i} = oExplorer.aggregateTimeSeries(i,18);
end

%%%Filter out time series that contains missing data. 

oExplorerAggregated = oExplorerAggregated.filterMissingData(); 

%%%Filter out time series that do not appear for all the subjects. 

oExplorerAggregated = oExplorerAggregated.filterSharedVariables();


cData = []; 
for i = 1:4
    temp2 = datenum(oExplorerAggregated.Data{i}.DataPrimitive.Time);
    [a b1] = sort(temp2,'ascend');
    cData(:,i) = oExplorerAggregated.Data{i}.DataPrimitive.Table(:,b1(2)); 
end 


counts = cData; 
counts = 2.^(counts);

geneNames = oExplorerAggregated.Data{i}.DataPrimitive.VarNames;

qReport = []; 
log2SevereUpFCReport = [];
log2MildUpFCReport = [];
pReport = []; 

    severe = [1 2];
    mild = [3 4]; 
    temp1 = mean(counts(:,severe),2); 
    temp2 = mean(counts(:,mild),2); 
    
    a = temp1./temp2; 
    b = temp2./temp1; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tLocal = nbintest(counts(:,severe),counts(:,mild),'VarianceLink','LocalRegression'); 
    [adjPval q] = mafdr(tLocal.pValue);
    qReport= q; 
    pReport = tLocal.pValue;
    log2SevereUpFCReport = a;
    log2MildUpFCReport = b;

    temp = counts <= 10; 
    temp = sum(temp,2); 
    idx = find(temp==0); 

    pVal = pReport(idx); 
    [temp qVal] = mafdr(pVal); 
    FoldChange = log2SevereUpFCReport(idx);
    FilteredGeneName = geneNames(idx);
    
    h = figure('units','normalized','outerposition',[0 0 1 1]);
    scatter(log2(temp1(idx)),log2(log2SevereUpFCReport(idx)),3,temp,'o');
    colormap(flipud(cool(256)))
    colorbar;
    ylabel('log2(Fold Change)')
    xlabel('log2(Mean of normalized counts)')
    title('Fold change by FDR')
    set(gca,'FontSize',20)
    funPrintImage(h,'Paper_Figure/Fig_2_SevereMildT2');
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    filename = 'DifferentialExpression/T2SevereMildSignificant.xlsx';
    cReport = {}; 
    idx = qVal <=0.05; 
    qVal = qVal(idx);
    FoldChange = FoldChange(idx);
    FilteredGeneName = FilteredGeneName(idx); 
    [a b] = sort(FoldChange,'descend');
    for i = 1:size(FilteredGeneName,1)
        cReport{i,1} = FilteredGeneName{b(i)}(3:end-1);
        cReport{i,2} = FoldChange(b(i));
    end 
    xlswrite(filename,A)
