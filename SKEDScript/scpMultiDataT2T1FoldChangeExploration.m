clear; clc; 
disp('Analysis of transcriptomic data for E04,E03,E23,E24,E25')
disp('Calling the script that loads data in RAM...')
if ~(exist('cStructures','var'))
    scpMultiDataLoad;
end
%%
cDataFoldChange = {};
cDataChange = {}; 
cLabel ={};
cVarName = {};
cTimeSeriesData ={};
cData =[];
%%
sExp = 'E03';
sProtocol = 'FG_GENE_EXP_RAW2'; 
sSubjects = 'All';

disp('-----------------------------------------------')
disp('Get functional genomic data for all subjects')

strucData = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
strucDataParasite = funGetProtocolData(cStructures,sExp,sSubjects,'ME_parasitemia');
[temp ia ib] = intersect(strucData.cTime{1},strucDataParasite.cTime{1});
mParasite = strucDataParasite.cRawCounts{1}(1,ib);
idx = find(mParasite);
%%
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCOAH';
strucData = funParasiteGeneRemoval(strucData,sIdentifier);
%%
disp('-----------------------------------------------')
disp('Normalize count data by library size and remove genes with consistent low counts')
nCount = 10; 
strucLibSizeNormalizedData = funLibSizeNormalizationLowCountRemoval(strucData,nCount); 

%%
for i = 1:length(strucLibSizeNormalizedData.cRawCounts)

    cDataChange = [cDataChange strucLibSizeNormalizedData.cRawCounts{i}(:,ia(idx(1)))- strucLibSizeNormalizedData.cRawCounts{i}(:,1)];
    cDataFoldChange = [cDataFoldChange strucLibSizeNormalizedData.cRawCounts{i}(:,ia(idx(1)))./(strucLibSizeNormalizedData.cRawCounts{i}(:,1)+1)];
    cVarName = [cVarName strucLibSizeNormalizedData.VarNames(1)];
    cTimeSeriesData = [cTimeSeriesData  strucLibSizeNormalizedData.cRawCounts{i}];
    cData = [cData strucLibSizeNormalizedData.cRawCounts{i}(:,2)];
end

cLabel = [cLabel cellfun(@(x) ['E03' x{1}],strucLibSizeNormalizedData.cSubject,'UniformOutput', false)];
%%
sExp = 'E04';
sProtocol = 'FG_GENE_EXP_RAW2'; 
sSubjects = 'All';

disp('-----------------------------------------------')
disp('Get functional genomic data for all subjects')
strucData = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
strucDataParasite = funGetProtocolData(cStructures,sExp,sSubjects,'ME_parasitemia');

strucDataParasite = funGetProtocolData(cStructures,sExp,sSubjects,'ME_parasitemia');
[temp ia ib] = intersect(strucData.cTime{1},strucDataParasite.cTime{1});
mParasite = strucDataParasite.cRawCounts{1}(1,ib);
idx = find(mParasite);

%%
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCYB';
strucData = funParasiteGeneRemoval(strucData,sIdentifier);
%%
disp('-----------------------------------------------')
disp('Normalize count data by library size and remove genes with consistent low counts')
nCount = 10; 
strucLibSizeNormalizedData = funLibSizeNormalizationLowCountRemoval(strucData,nCount); 

%%
for i = 1:length(strucLibSizeNormalizedData.cRawCounts)

    cDataChange = [cDataChange strucLibSizeNormalizedData.cRawCounts{i}(:,ia(idx(1)))- strucLibSizeNormalizedData.cRawCounts{i}(:,1)];
    cDataFoldChange = [cDataFoldChange strucLibSizeNormalizedData.cRawCounts{i}(:,2)./(strucLibSizeNormalizedData.cRawCounts{i}(:,1)+1)];
    cVarName = [cVarName strucLibSizeNormalizedData.VarNames(1)];
    cTimeSeriesData = [cTimeSeriesData  strucLibSizeNormalizedData.cRawCounts{i}];
        cData = [cData strucLibSizeNormalizedData.cRawCounts{i}(:,2)];

end

cLabel = [cLabel cellfun(@(x) ['E04' x{1}],strucLibSizeNormalizedData.cSubject,'UniformOutput', false)];
%%
sExp = 'E23';
sProtocol = 'FG_GENE_EXP_RAW2'; 
sSubjects = 'All';

disp('-----------------------------------------------')
disp('Get functional genomic data for all subjects')
strucData = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
strucDataParasite = funGetProtocolData(cStructures,sExp,sSubjects,'ME_parasitemia');
[temp ia ib] = intersect(strucData.cTime{1},strucDataParasite.cTime{1});
mParasite = strucDataParasite.cRawCounts{1}(1,ib);
idx = find(mParasite);
%%
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCYB';
strucData = funParasiteGeneRemoval(strucData,sIdentifier);
%%
disp('-----------------------------------------------')
disp('Normalize count data by library size and remove genes with consistent low counts')
nCount = 10; 
strucLibSizeNormalizedData = funLibSizeNormalizationLowCountRemoval(strucData,nCount); 

%%
for i = 1:length(strucLibSizeNormalizedData.cRawCounts)

    cDataChange = [cDataChange strucLibSizeNormalizedData.cRawCounts{i}(:,4)- strucLibSizeNormalizedData.cRawCounts{i}(:,1)];
    cDataFoldChange = [cDataFoldChange strucLibSizeNormalizedData.cRawCounts{i}(:,4)./(strucLibSizeNormalizedData.cRawCounts{i}(:,1)+1)];
    cVarName = [cVarName strucLibSizeNormalizedData.VarNames(1)];
    cTimeSeriesData = [cTimeSeriesData  strucLibSizeNormalizedData.cRawCounts{i}];

    cData = [cData strucLibSizeNormalizedData.cRawCounts{i}(:,4)];

end

cLabel = [cLabel cellfun(@(x) ['E23' x{1}],strucLibSizeNormalizedData.cSubject,'UniformOutput', false)];
%%
sExp = 'E25';
sProtocol = 'FG_GENE_EXP_RAW2'; 
sSubjects = 'All';

disp('-----------------------------------------------')
disp('Get functional genomic data for all subjects')
strucData = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
strucDataParasite = funGetProtocolData(cStructures,sExp,sSubjects,'ME_parasitemia');
[temp ia ib] = intersect(strucData.cTime{1},strucDataParasite.cTime{1});
mParasite = strucDataParasite.cRawCounts{1}(1,ib);
idx = find(mParasite);


%%
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCYB';
strucData = funParasiteGeneRemoval(strucData,sIdentifier);
%%
disp('-----------------------------------------------')
disp('Normalize count data by library size and remove genes with consistent low counts')
nCount = 10; 
strucLibSizeNormalizedData = funLibSizeNormalizationLowCountRemoval(strucData,nCount); 

%%
for i = 1:length(strucLibSizeNormalizedData.cRawCounts)

    cDataChange = [cDataChange strucLibSizeNormalizedData.cRawCounts{i}(:,3)- strucLibSizeNormalizedData.cRawCounts{i}(:,1)];
    cDataFoldChange = [cDataFoldChange strucLibSizeNormalizedData.cRawCounts{i}(:,3)./(strucLibSizeNormalizedData.cRawCounts{i}(:,1)+1)];
    cVarName = [cVarName strucLibSizeNormalizedData.VarNames(1)];
    cTimeSeriesData = [cTimeSeriesData  strucLibSizeNormalizedData.cRawCounts{i}];
    cData = [cData strucLibSizeNormalizedData.cRawCounts{i}(:,3)];

end
cLabel = [cLabel cellfun(@(x) ['E25' x{1}],strucLibSizeNormalizedData.cSubject,'UniformOutput', false)];
%%
sExp = 'E24';
sProtocol = 'FG_GENE_EXP_RAW2'; 
sSubjects = 'All';

disp('-----------------------------------------------')
disp('Get functional genomic data for all subjects')
strucData = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol);
strucDataParasite = funGetProtocolData(cStructures,sExp,sSubjects,'ME_parasitemia');
[temp ia ib] = intersect(strucData.cTime{2},strucDataParasite.cTime{2});
mParasite = strucDataParasite.cRawCounts{2}(1,ib);
idx = find(mParasite);
%%
disp('-----------------------------------------------')
disp('Removal of parasite gene counts.')
sIdentifier = 'x_PCYB';
strucData = funParasiteGeneRemoval(strucData,sIdentifier);
%%
disp('-----------------------------------------------')
disp('Normalize count data by library size and remove genes with consistent low counts')
nCount = 10; 
strucLibSizeNormalizedData = funLibSizeNormalizationLowCountRemoval(strucData,nCount); 

%%
for i = 1:length(strucLibSizeNormalizedData.cRawCounts)

    cDataChange = [cDataChange strucLibSizeNormalizedData.cRawCounts{i}(:,2)- strucLibSizeNormalizedData.cRawCounts{i}(:,1)];
    cDataFoldChange = [cDataFoldChange strucLibSizeNormalizedData.cRawCounts{i}(:,2)./(strucLibSizeNormalizedData.cRawCounts{i}(:,1)+1)];
    cVarName = [cVarName strucLibSizeNormalizedData.VarNames(1)];
    cTimeSeriesData = [cTimeSeriesData  strucLibSizeNormalizedData.cRawCounts{i}];
    cData = [cData strucLibSizeNormalizedData.cRawCounts{i}(:,2)];

end
cLabel = [cLabel cellfun(@(x) ['E24' x{1}],strucLibSizeNormalizedData.cSubject,'UniformOutput', false)];
%%
cCommonGenes = cVarName{1};
mDataChange = [];
mDataFoldChange = [];
mData = [];
for i = 2:length(cVarName)
    cCommonGenes = intersect(cCommonGenes,cVarName{i});
end

for i = 1:length(cDataChange)
    [temp ia ib] = intersect(cCommonGenes,cVarName{i});
    mDataChange = [mDataChange cDataChange{i}(ib,:)];
    mDataFoldChange = [mDataFoldChange cDataFoldChange{i}(ib,:)];
    mData = [mData cData{i}(ib,:)]; 
end

[num txt raw] = xlsread('../Data/MultiDataUpRegulatedGenesT2T1.xls');
% Cluster by upregulated genes in E04 severe
[temp1 a1 b1] = intersect(txt(:,4),cellfun(@(x) x(3:end-1),cCommonGenes,'UniformOutput',0));
% Cluster by upregulated genes in E04 mild
[temp2 a2 b2] = intersect(txt(:,5),cellfun(@(x) x(3:end-1),cCommonGenes,'UniformOutput',0));
% Find the genes that are exclusive to severe and mild
cIntersect = intersect(temp1,temp2);
cExclusiveSevere = setxor(cIntersect,temp1);
b1 = find(ismember(temp1,cExclusiveSevere));
cExclusiveMild = setxor(cIntersect,temp2);
b2 = find(ismember(temp2,cExclusiveMild));
% Cluster by upregulated genes in E04 severe
h = clustergram(mDataFoldChange(b1,:),'standardize',2,'ColumnLabels',cLabel,'Linkage','ward');
tempVar = var(mDataChange,[],2);
[temp idx] = sort(tempVar,'descend');
% Cluster by upregulated genes in E04 mild
h = clustergram(mDataFoldChange(b2,:),'standardize',2,'ColumnLabels',cLabel,'Linkage','ward');
tempVar = var(mDataChange,[],2);
[temp idx] = sort(tempVar,'descend');


%Y = pdist(mDataChange(idx(1:2000),:)');
%Z = linkage(Y,'ward');
%dendrogram(Z,'Labels',cLabel);
%xtickangle(60);

%clustergram(mDataChange(idx(1:2000),:),'standardize',2,'ColumnLabels',cLabel,'Linkage','ward');

tempVar = var(mDataFoldChange,[],2);
[temp idx] = sort(tempVar,'descend');


%Y = pdist(mDataFoldChange(idx(1:5000),:)');
%Z = linkage(Y,'weighted');
%dendrogram(Z,'Labels',cLabel);
%xtickangle(60);

h = clustergram(mDataFoldChange(idx,:),'standardize',2,'ColumnLabels',cLabel,'Linkage','ward');
    addTitle(h,'Fold-Change')

h = clustergram(mDataChange(idx,:),'standardize',2,'ColumnLabels',cLabel,'Linkage','ward');
    addTitle(h,'Difference')

%cidx = kmeans(mDataFoldChange',3);

%[silh5,h] = silhouette(mDataFoldChange',cidx);
%mean(silh5)
%%
    [num txt raw] = xlsread('AntiViralPathways.xlsx');

for n = 1:5
    display('Pathway Specific Analysis'); 

    [temp ia ib] = intersect(cellfun(@(x) x(3:end-1),cCommonGenes,'UniformOutput',0),txt(:,n));
    h = clustergram(mDataFoldChange(ia,:),'standardize',2,'ColumnLabels',cLabel,'Linkage','ward','RowLabels',txt(ib,n));
    addTitle(h, [strrep(txt(1,n),'_',' ') ' ' 'Fold Change'])
    h = clustergram(mDataChange(ia,:),'standardize',2,'ColumnLabels',cLabel,'Linkage','ward','RowLabels',txt(ib,n));
    addTitle(h, [strrep(txt(1,n),'_',' ') ' ' 'Difference'])

end


%%
display('Pathway Specific Analysis'); 
[num txt raw] = xlsread('DIEGO_VIRAL.xlsx');
n = 1;
[temp ia ib] = intersect(cellfun(@(x) x(3:end-1),cCommonGenes,'UniformOutput',0),txt(:,n));
h = clustergram(mDataFoldChange(ia,:),'standardize',2,'ColumnLabels',cLabel,'Linkage','ward','RowLabels',txt(ib,n));
addTitle(h, ['Viral Fold Change'])
h = clustergram(mDataChange(ia,:),'standardize',2,'ColumnLabels',cLabel,'Linkage','ward','RowLabels',txt(ib,n));
addTitle(h, ['Viral Difference'])


%cColor = {'ro','bo','ko'};
%    [temp ia ib] = intersect(cellfun(@(x) x(3:end-1),cCommonGenes,'UniformOutput',0),ViralGenes.RowNodeNames);
    
%h = figure();
%for i = 1:length(cTimeSeriesData)
%    sExperiment = cLabel{i}(1:3);
%    switch sExperiment
%        case 'E23'
%            idx = [1:4];
%            x = linspace(0,2,4);
%        case 'E25'
%            idx = [1:3];
%            x = linspace(0,2,3);
%        otherwise
%            idx = [1:2];
%            x = [0 2];
%    end
%    
%    if isempty(intersect(cLabel{i},ViralG1.ColumnNodeNames)) == 0 
%        temp = cTimeSeriesData{i}(ia,idx); 
%        temp = temp-mean(temp,2);
%        temp = temp./var(temp,[],2);
%        plot(x,temp,cColor{1});
%        hold on 
%    elseif isempty(intersect(cLabel{i},ViralG2.ColumnNodeNames)) == 0 
%        temp = cTimeSeriesData{i}(ia,idx); 
%                temp = temp./var(temp,[],2);
%
%        plot(x,temp,cColor{2});
%        hold on 
%    else
%        temp = cTimeSeriesData{i}(ia,idx); 
%        temp = temp-mean(temp,2);
%        temp = temp./var(temp,[],2);

%        plot(x,temp,cColor{3});
%
%        hold on 
%    end
%end
%%
display('Heme Pathway')
[num txt raw] = xlsread('Heme_Pathway.xlsx');
for n = 1:2
    display('Pathway Specific Analysis'); 

    [temp ia ib] = intersect(cellfun(@(x) x(3:end-1),cCommonGenes,'UniformOutput',0),txt(:,n));
    h = clustergram(mDataFoldChange(ia,:),'standardize',2,'ColumnLabels',cLabel,'Linkage','ward','RowLabels',txt(ib,n));
    addTitle(h, strrep(txt(1,n),'_',' '))
end
%%
