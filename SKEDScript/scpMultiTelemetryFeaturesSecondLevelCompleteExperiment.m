% This script cycles through all telemetry experiments for all subjects and
% computes features for 5 seconds of data every hour and averages them so
% that there is one value for each feature every hour for the entire
% experiment.

tic;
addpath('../SKEDFunction')
addpath('../SKEDClass')

oTelemetry = clsSKEDTelemetry; %UPDATE
sExp = {'E30'; 'E07A'; 'E07B'; 'E06'}; % Current inclusive list of telemetry experiments

% Select one of the experiments at a time defined in variable sExp
for i = 1:size(sExp)
    oTelemetry.Experiment = sExp(i,:);
    sExperimentID = sExp{i}; 
                    
    % Define the subjects per experiment
    switch sExp{i}
        case 'E30'
            cMonkey = {
                         % nSubjectID - Subject ID (Database ID)
                 1403, 'RKy15';
                 1406, 'REd16'
             };
         nDownsamplePts = 15;
         BPValidDataThreshold = 750;
        case 'E07A'  
             cMonkey = {
                         % nSubjectID - Subject ID (Database ID)
                 1402, '12C53';
                 1404, 'H12C59'; 
                 1405, 'H12C8'; 
                 1407, '12C44'; 
                 1408, '11C131'; 
                 1409, '11C166'; 
                 1410, 'H12C50' %1410 E07A only
              };
          nDownsamplePts = 6;
          BPValidDataThreshold = 300;
        case 'E07B'
            cMonkey = {
                        % nSubjectID - Subject ID (Database ID)
                 1401, '12C136'; % 1401 E07B only
                 1402, '12C53';
                 1404, 'H12C59'; 
                 1405, 'H12C8'; 
                 1407, '12C44'; 
                 1408, '11C131'; 
                 1409, '11C166'; 
            };
        nDownsamplePts = 6;
        BPValidDataThreshold = 300;
        case 'E06'
            cMonkey = {
                        % nSubjectID - Subject ID (Database ID)
                1912, 'RIh16'; 
                1915, 'RTe16'; 
                1916, 'RCl15'; 
                1918, 'RUf16'; 
            };
        nDownsamplePts = 6;
        BPValidDataThreshold = 300;
    end
    
    % Loop through each monkey in the specified experiment
    for j = 1 : length(cMonkey)
        nSubjectID = cMonkey{j,1};
        FeatureMatrix = [];
        dtDatesFeatures = [];
        Counter = 0;
        
        % Retrieve protocol_app_id for monkey and experiment combination
        % and use get_time_series_range for monkey to get start and end date
        % Note: returned times are in YY-MM-DD HH24:MI:SS.ff3 Format (eg. 16-12-01 17:30:44.000)
        %       thus they are converted using datenum
        [dtMinDateTimeSeries, dtMaxDateTimeSeries, cProtocol_ids] = getSKEDTimeSeriesRange_Protocols(oTelemetry,sExperimentID,nSubjectID);
        dtMaxDateTimeSeries = datenum(dtMaxDateTimeSeries{1});
        dtCurrentStartDate = datenum(dtMinDateTimeSeries{1});
        
        % From start time to end time of the entire experiment
        while dtCurrentStartDate <= dtMaxDateTimeSeries % When comparing dates use datenum format
%         for i = 1:10   % used for testing 
            % Manipulate Time
            dtCurrentStartDate = addtodate(dtCurrentStartDate,1,'hour'); 
            temp = datevec(dtCurrentStartDate);
            temp(5) = 0;
            temp(6) = 0;
            dtCurrentStartDate = datenum(temp);
            dtCurrentEndDate = addtodate(dtCurrentStartDate,5,'second');
            
            % Convert time into the format that the database accepts 
            dtMinDate = datestr(dtCurrentStartDate,'dd-mmm-yy HH:MM:SS.fff PM');
            dtMaxDate = datestr(dtCurrentEndDate, 'dd-mmm-yy HH:MM:SS.fff PM');
            cDataSKED = getSKEDResults(oTelemetry,nSubjectID,dtMinDate,dtMaxDate,cProtocol_ids);
            
            % Make sure that there is data retrieved
            if size(cDataSKED,1) < 4000
                continue
            end
            
            temp = table2cell(cDataSKED(:,1));
            SKEDDates = datenum(temp,'yy-mm-dd HH:MM:SS.FFF');  

            % Retrieve the Column Names of cDataSKED
            ColumnNames = cDataSKED.Properties.VariableNames;
            nColumnNames = size(ColumnNames,2);
            
            % Identify the data types by column names of the table cDataSKED           
            for k = 1 : nColumnNames
                sColumnName = cellstr(ColumnNames(k));
                sColumnName = extractAfter(sColumnName,5);
                cColumnName = char(sColumnName);
                switch cColumnName
                    case char('ACCX')
                        ACCX_Index = k-1;
                    case char('ACCY')
                        ACCY_Index = k-1;
                    case char('ACCZ')
                        ACCZ_Index = k-1;
                    case char('BP')
                        BP_Index = k-1;
                    case char('APR')
                        APR_Index = k-1;
                    case char('TEMP')
                        TEMP_Index = k-1;
%                     case char('ACT')
%                         ACT_Index = k-1;
                    case char('ECG')
                        ECG_Index = k-1;
                end % End switch statement

            end % End loop for Column Names to indexes
             
            % Create a matrix of the data without time vector
            TimeSeriesDataDimensions = size(cDataSKED);
            nTimeSeriesColumns = TimeSeriesDataDimensions(1,2);
            nTimeSeriesRows = TimeSeriesDataDimensions(1,1);
            TimeSeriesData = table2cell(cDataSKED(:,2:nTimeSeriesColumns));
            nDataColumns = size(TimeSeriesData,2);
            nDataRows = size(TimeSeriesData,1);

            % For TimeSeriesData: remove 'null' values and replace with NaN
            for m = 1 : nDataColumns
                for n = 1 : nDataRows
                    output = [];
                    s1 = 'null';
                    s2 = TimeSeriesData(n,m);
                    output = strcmp(s1,s2);
                    if output == 1
                        TimeSeriesData(n,m) = {NaN};
                    end
                end
            end

            mTimeSeriesData = cell2mat(TimeSeriesData);

           % Check to see if Data is complete enough for analysis
            ACCXMissingPoints = find(isnan(mTimeSeriesData(:,ACCX_Index)));
            ACCYMissingPoints = find(isnan(mTimeSeriesData(:,ACCY_Index)));
            ACCZMissingPoints = find(isnan(mTimeSeriesData(:,ACCZ_Index)));
            idxX = ~isnan(mTimeSeriesData(:,ACCX_Index));
            idxY = ~isnan(mTimeSeriesData(:,ACCY_Index));
            idxZ = ~isnan(mTimeSeriesData(:,ACCZ_Index));
            idxComposite = and(idxX,and(idxY, idxZ));
            
            % Call function to separate the 5 seconds of data into
            % individual seconds
            [idxStartSecond] = funTelemetrySeparate5Seconds(SKEDDates,mTimeSeriesData);
            
            if length(idxStartSecond)==5%Jessica Had to do this because the size of idxStartSecond came back as 4 when counter was at 32
                nActivityValidPoints =[];
                nECGValidPoints = [];
                nBPValidPoints = [];

                for m = 1:5
                        StartIndexRange = idxStartSecond(m);
                        if m ~= 5
                            EndIndexRange = idxStartSecond(m+1)-1;
                            % Activity
                            CompositeLocations = find(idxComposite(StartIndexRange:EndIndexRange,1));
                            ACCValidPoints = length(CompositeLocations);
                            nActivityValidPoints = [nActivityValidPoints; ACCValidPoints];

                            % ECG
                            ECGVector = mTimeSeriesData(StartIndexRange:EndIndexRange,ECG_Index);
                            ECG_dummy = ~isnan(ECGVector);
                            ECGValidPoints = sum(ECG_dummy);
                            nECGValidPoints = [nECGValidPoints; ECGValidPoints];

                            % Blood Pressure
                            BPVector = mTimeSeriesData(StartIndexRange:EndIndexRange,BP_Index);
                            BP_dummy = ~isnan(BPVector);
                            nBPPoints = mTimeSeriesData(BP_dummy,BP_Index); 
                            nBPValidPointIndex = ~isnan(nBPPoints);
                            BPValidPoints = sum(nBPValidPointIndex);
                            nBPValidPoints = [nBPValidPoints; BPValidPoints];

                        else
                            sizeDate = length(SKEDDates);
                            EndIndexRange = sizeDate;

                            % Activity
                            CompositeLocations = find(idxComposite(StartIndexRange:EndIndexRange,1));
                            ACCValidPoints = length(CompositeLocations);
                            nActivityValidPoints = [nActivityValidPoints; ACCValidPoints];                        

                            % ECG
                            ECGVector = mTimeSeriesData(StartIndexRange:EndIndexRange,ECG_Index);
                            ECG_dummy = ~isnan(ECGVector);
                            ECGValidPoints = sum(ECG_dummy);
                            nECGValidPoints = [nECGValidPoints; ECGValidPoints];

                            % Blood Pressure
                            BPVector = mTimeSeriesData(StartIndexRange:EndIndexRange,BP_Index);
                            BP_dummy = ~isnan(BPVector);
                            nBPPoints = mTimeSeriesData(BP_dummy,BP_Index); 
                            nBPValidPointIndex = ~isnan(nBPPoints);
                            BPValidPoints = sum(nBPValidPointIndex);
                            nBPValidPoints = [nBPValidPoints; BPValidPoints];                         

                        end
                end     

                TEMPMissingPoints = find(isnan(mTimeSeriesData(:,TEMP_Index)));
                nDataSize = size(mTimeSeriesData,1);
                TEMPValidPointsParameter = nDataSize - size(TEMPMissingPoints,1); % Required 1 data point per second

                bValid = nActivityValidPoints(1) >= 7 && ... % Check to see if we have 7 data points per second
                    nActivityValidPoints(2) >= 7 && ... 
                    nActivityValidPoints(3) >= 7 && ... 
                    nActivityValidPoints(4) >= 7 && ... 
                    nActivityValidPoints(5) >= 7 && ... 
                    ...% Check to see if we have 450 data points per second for all 
                    ...% experiments except for E30 in which the threshold value for missing data points if 900     
                    nBPValidPoints(1) >=  BPValidDataThreshold && ...
                    nBPValidPoints(2) >=  BPValidDataThreshold && ...
                    nBPValidPoints(3) >=  BPValidDataThreshold && ...
                    nBPValidPoints(4) >=  BPValidDataThreshold && ...
                    nBPValidPoints(5) >=  BPValidDataThreshold && ...
                    ... % Check to see if we have 750 points for ECG per second 
                    nECGValidPoints(1) >= 750 && ...
                    nECGValidPoints(2) >= 750 && ...
                    nECGValidPoints(3) >= 750 && ...
                    nECGValidPoints(4) >= 750 && ...
                    nECGValidPoints(5) >= 750 && ...
                    ... % Check to see if we have have temperature reading per second
                    TEMPValidPointsParameter >= 3;

                if bValid
                    % Call function to separate the 5 seconds of data into
                    % individual seconds
                    [idxStartSecond] = funTelemetrySeparate5Seconds(SKEDDates,mTimeSeriesData);

                    ECGFeatureBPM_5seconds = 0;
                    ECGFeature_AvgRPeakHeight_5seconds = 0;
                    Higherorder2_5seconds = [];
                    Higherorder3_5seconds = [];
                    Higherorder_5seconds = [];
                    Writhe_5seconds = [];
                    AvgCross_5seconds = [];
                    Activity_5seconds = [];
                    ActivityMean_5seconds = [];
                    TEMPData_5seconds = [];
                    BPSystolic_5seconds = [];
                    BPDiastolic_5seconds = [];
                
                    % Compute features per second
                    for p = 1 : 5
                        StartIndexRange = idxStartSecond(p);
                        if p ~= 5
                            EndIndexRange = idxStartSecond(p+1)-1;
                        else
                            sizeDate = length(SKEDDates);
                            EndIndexRange = sizeDate;
                        end
                        Time = SKEDDates(StartIndexRange:EndIndexRange,:);
                        mData = mTimeSeriesData(idxStartSecond(p):EndIndexRange,:);

                        % Remove NaNs from Blood Pressure Data
                        BP_dummy = ~isnan(mData(:,BP_Index));
                        BPData = mData(BP_dummy,BP_Index);
                        
                        switch sExperimentID
                            case 'E30'
                                APRValidPoints = ~isnan(mTimeSeriesData(:,APR_Index)); 
                                APRValue = mTimeSeriesData(APRValidPoints,2);
                                if length(APRValue) > 0
                                    BPData = BPData - (sum(APRValue)/length(APRValue));
                                else 
                                    APRValue = 738.8685817;
                                    BPData = BPData - APRValue;
                                end
                            case 'E07A'
                                APRValue = 738.5951587;
                                BPData = BPData - APRValue;
                            case 'E07B'
                                nSubjectIDAPR = 1401;
                                cProtocol_id_single = '218';
                                cDataAPR = getSKEDSingleResult(oTelemetry,nSubjectIDAPR,dtMinDate,dtMaxDate,cProtocol_id_single);  
                                APRLength = size(cDataAPR,1)
                                if APRLength > 1
                                    APRValueCell = table2cell(cDataAPR(:,2));
                                    APRValue = cell2mat(APRValueCell);
                                    APRValue = sum(APRValue)/length(APRValue);
                                    BPData = BPData - APRValue;
                                else
                                    APRValue = 738.3217356;
                                    BPData = BPData - APRValue;
                                end
                            case 'E06'
                                nSubjectIDAPR = 1915;
                                cProtocol_id_single = '199';
                                cDataAPR = getSKEDSingleResult(oTelemetry,nSubjectIDAPR,dtMinDate,dtMaxDate,cProtocol_id_single);  
                                APRLength = size(cDataAPR,1);
                                if APRLength > 1
                                    APRValueCell = table2cell(cDataAPR(:,2));
                                    APRValue = cell2mat(APRValueCell);
                                    APRValue = sum(APRValue)/length(APRValue);
                                    BPData = BPData - APRValue;
                                else
                                    APRValue = 738.826705;
                                    BPData = BPData - APRValue;
                                end                                    
                        end 

                        % Compute ECG Features
                        ECG_dummy = ~isnan(mData(:,ECG_Index));
                        ECGData = mData(ECG_dummy,ECG_Index);
                        [ECGFeature_AvgRPeakHeight, ECGFeatureBPM, ECGDetrended] = funTelemetryECGSecondFeatures(Time, ECGData);
                        ECGFeature_AvgRPeakHeight_5seconds  = [ECGFeature_AvgRPeakHeight_5seconds; ECGFeature_AvgRPeakHeight];  
                        ECGFeatureBPM_5seconds= [ECGFeatureBPM_5seconds;  ECGFeatureBPM];

                        % Compute l2 norm of Accelerometer
                        % readings
                        [ActivityTime,Activity] = funTelemetryComputeActivityl2norm(Time,mData(:,ACCX_Index),mData(:,ACCY_Index),mData(:,ACCZ_Index));
                        ActivityMean = mean(Activity); 
                        Activity_5seconds = [Activity_5seconds;Activity];
                        ActivityMean_5seconds = [ActivityMean_5seconds; ActivityMean];

                        % Compute Geometric Features
                        [Higherorder2,Higherorder3,Higherorder,Writhe,AvgCross] = funComputeGeometricFeaturesSecond(Activity, ECGDetrended,BPData,BPValidDataThreshold, nDownsamplePts);
                        Higherorder2_5seconds = [Higherorder2_5seconds; Higherorder2];
                        Higherorder3_5seconds = [Higherorder3_5seconds; Higherorder3];
                        Higherorder_5seconds = [Higherorder_5seconds; Higherorder];
                        Writhe_5seconds = [Writhe_5seconds; Writhe];
                        AvgCross_5seconds = [AvgCross_5seconds; AvgCross];

                        % Compute Average of Temperature
                        Temperature_dummy = ~isnan(mData(:,TEMP_Index));
                        TEMPData = mData(Temperature_dummy,TEMP_Index);
                        TEMPData_5seconds = [TEMPData_5seconds; TEMPData];

                        % Compute BP Features
                        [BPSystolicAvg, BPDiastolicAvg] = funTelemetryBloodPressureFeatures(BPData);
                        BPSystolic_5seconds = [BPSystolic_5seconds; BPSystolicAvg];
                        BPDiastolic_5seconds = [BPDiastolic_5seconds; BPDiastolicAvg];

                    end % End loop for 5 seconds of one hour
                end
                % Average features from each second to get
                % one value representative of the hour
                ECGFeature_BPM_avg = sum(ECGFeatureBPM_5seconds)/5; % 1
                ECGFeature_AvgRPeakHeight_avg = sum(ECGFeature_AvgRPeakHeight_5seconds)/5; % 2
                ActivityMean_5seconds = sum(ActivityMean_5seconds)/5; % 3
                Higherorder2_avg = sum(Higherorder2_5seconds)/5; % 4 
                Higherorder3_avg = sum(Higherorder3_5seconds)/5; % 5
                Higherorder_avg = sum(Higherorder_5seconds)/5; % 6
                Writhe_avg = sum(Writhe_5seconds)/5; % 7
                AvgCross_avg = sum(AvgCross_5seconds)/5; % 8
                TEMPData_avg = sum(TEMPData_5seconds)/length(TEMPData_5seconds); % 9
                BPSystolic_avg = sum(BPSystolic_5seconds)/5; % 10
                BPDiastolic_avg = sum(BPDiastolic_5seconds)/5; % 11

                CurrentFeatureVector = [ECGFeature_BPM_avg, ECGFeature_AvgRPeakHeight_avg,...
                                ActivityMean_5seconds, Higherorder2_avg, Higherorder3_avg,...
                                Higherorder_avg, Writhe_avg, AvgCross_avg,...
                                TEMPData_avg, BPSystolic_avg, BPDiastolic_avg];
                FeatureMatrix = [FeatureMatrix; CurrentFeatureVector];
                dtDatesFeatures = [dtDatesFeatures; dtCurrentStartDate]; 
                Counter = Counter +1;
                display('Counter = ',num2str(Counter))
            end 
        end % End while statement
        
        cMonkey{j,1}
        sFileName = [sExperimentID 'FeatureMatrix_' num2str(cMonkey{j,1}) '_' cMonkey{j,2} '.mat'];
        save(sFileName, 'dtDatesFeatures', 'FeatureMatrix');
        toc;

    
    end % End loop for Monkeys in that Experiment
end % End loop for Experiments

