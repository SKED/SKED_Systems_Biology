% This script cycles through all telemetry experiments for all subjects and
% computes features for 5 seconds of data every hour and averages them so
% that there is one value for each feature every hour for the entire
% experiment.

addpath('../function')  
addpath('../SKEDFunction')
addpath('../SKEDClass')

oTelemetry = clsTelemetry; %UPDATE
sExp = ['E30'; 'E07A'; 'E07B'; 'E06']; % Current inclusive list of telemetry experiments

% Select one of the experiments at a time defined in variable sExp
for i = 1:size(sExp)
    oTelemetry.Experiment = sExp(i,:);
                    
    % Define the subjects per experiment
    switch sExp(i,:)
        case 'E30'
            cMonkey = {
                         % nSubjectID - Subject ID (Database ID)
                 1403; % RKy15 - 1403
                 1406  % REd16 - 1406
             };
        case 'E07A'  
             cMonkey = {
                         % nSubjectID - Subject ID (Database ID)
                 1402; % 12C53  - 1402
                 1404; % H12C59 - 1404
                 1405; % H12C8  - 1405
                 1407; % 12C44  - 1407
                 1408; % 11C131 - 1408
                 1409; % 11C166 - 1409
                 1410  % H12C50 - 1410 E07A only
              };
        case 'E07B'
            cMonkey = {
                        % nSubjectID - Subject ID (Database ID)
                1401; % 12C136 - 1401 E07B only
                1402; % 12C53  - 1402
                1404; % H12C59 - 1404
                1405; % H12C8  - 1405
                1407; % 12C44  - 1407
                1408; % 11C131 - 1408
                1409  % 11C166 - 1409
            };
        case 'E06'
            cMonkey = {
                        % nSubjectID - Subject ID (Database ID)
                1912; % RIh16 - 1912
                1915; % RTe16 - 1915
                1916; % RCl15 - 1916
                1918; % RUf16 - 1918
            };
    end
    
    % Loop through each monkey in the specified experiment
    for j = 1 : length(cMonkey)
        nSubjectID = cMonkey(j);
        
        % Retrieve protocol_app_id for monkey and experiment combination
        % and use get_time_series_range for monkey to get start and end date
        % Note: returned times are in YY-MM-DD HH24:MI:SS.ff3 Format (eg. 16-12-01 17:30:44.000)
        %       thus they are converted using datenum
        [MinDateTimeSeries, MaxDateTimeSeries, cProtocol_ids] = getSKEDTimeSeriesRange_Protocols(oTelemetry,sExperimentID,nSubjectID);
        temp = cell2mat(MinDateTimeSeries);
        MinDateTimeSeries = datenum(temp);
        temp = cell2mat(MaxDateTimeSeries);
        MaxDateTimeSeries = datenum(temp);
        CurrentDate = MinDateTimeSeries;
        
        % From start time to end time of the entire experiment
        if CurrentDate <= MaxDateTimeSeries % When comparing dates use datenum format
            
            % Manipulate Time
            CurrentDate = addtodate(CurrentDate,1,'hour'); 
            
            cDataSKED = getSKEDResults(oTelemetry,sExperimentID,nSubjectID,sDataType,dtMinDate,dtMaxDate)
            temp = table2cell(cDataSKED(:,1));
            SKEDDates = datenum(temp,'yy-mm-dd HH:MM:SS.FFF')  
            
            
            % Retrieve All Telemetry Data for Specified Time Range
            TimeSeriesData = getSKEDResults(obj,nSubjectID,dtMinDate,dtMaxDate,cProtocol_ids)


            TimeSeriesDataDimensions = size(TimeSeriesData);
            nTimeSeriesColumns = TimeSeriesDataDimensions(1,2);
            nTimeSeriesRows = TimeSeriesDataDimensions(1,1);
            TimeSeriesData = table2cell(cDataSKED(:,2:nTimeSeriesColumns)); 
            
            % For TimeSeriesData: remove 'null' values and replace with NaN
            for i = 1 : nTimeSeriesColumns
                for j = 1 : nTimeSeriesRows
                    output = [];
                    s1 = 'null';
                    s2 = TimeSeriesData(j,i);
                    output = strcmp(s1,s2)
                    if output == 1
                        TimeSeriesData(j,i) = {NaN};
                    end
                end
            end
        
        
            %DD-MON-YY HH:MI:SS.ff3 AM/PM format for input to get time series

            dtMinTime
            dtMaxTime
            dtCurrentStartTime
            dtCurrentEndTime

        
        

             % Save feature matrix in .mat file for monkey per experiment   
             switch sExp(i,:)
                case 'E30'
                    switch nSubjectID
                                 % nSubjectID - Subject ID (Database ID)
                        case 1403; % RKy15 - 1403
                            sFileName = 'E30FeatureMatrix_1403.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1406  % REd16 - 1406
                            sFileName = 'E30FeatureMatrix_1406.mat';
                            save(sFileName, 'FeatureMatrix');
                    end
                case 'E07A'  
                    switch nSubjectID
                                 % nSubjectID - Subject ID (Database ID)
                        case 1402 % 12C53  - 1402
                            sFileName = 'E07AFeatureMatrix_1402.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1404 % H12C59 - 1404
                            sFileName = 'E07AFeatureMatrix_1404.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1405 % H12C8  - 1405
                            sFileName = 'E07AFeatureMatrix_1405.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1407 % 12C44  - 1407
                            sFileName = 'E07AFeatureMatrix_1407.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1408 % 11C131 - 1408
                            sFileName = 'E07AFeatureMatrix_1408.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1409 % 11C166 - 1409
                            sFileName = 'E07AFeatureMatrix_1409.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1410 % H12C50 - 1410 E07A only
                            sFileName = 'E07AFeatureMatrix_1410.mat';
                            save(sFileName, 'FeatureMatrix');
                    end
                case 'E07B'
                    switch nSubjectID
                                % nSubjectID - Subject ID (Database ID)
                        case 1401 % 12C136 - 1401 E07B only
                            sFileName = 'E07BFeatureMatrix_1401.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1402 % 12C53  - 1402
                            sFileName = 'E07BFeatureMatrix_1402.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1404 % H12C59 - 1404
                            sFileName = 'E07BFeatureMatrix_1404.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1405 % H12C8  - 1405
                            sFileName = 'E07BFeatureMatrix_1405.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1407 % 12C44  - 1407
                            sFileName = 'E07BFeatureMatrix_1407.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1408 % 11C131 - 1408
                            sFileName = 'E07BFeatureMatrix_1408.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1409 % 11C166 - 1409
                            sFileName = 'E07BFeatureMatrix_1409.mat';
                            save(sFileName, 'FeatureMatrix');
                    end
                case 'E06'
                    switch nSubjectID
                                     % nSubjectID - Subject ID (Database ID)
                        case 1912 % RIh16 - 1912
                            sFileName = 'E06FeatureMatrix_1912.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1915 % RTe16 - 1915
                            sFileName = 'E06FeatureMatrix_1915.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1916 % RCl15 - 1916
                            sFileName = 'E06FeatureMatrix_1916.mat';
                            save(sFileName, 'FeatureMatrix');
                        case 1918 % RUf16 - 1918
                            sFileName = 'E06FeatureMatrix_1918.mat';
                            save(sFileName, 'FeatureMatrix');
                    end
             end
        end % End loop for one individual monkey analysis
    end % End loop for Monkeys
end % End loop for Experiments

