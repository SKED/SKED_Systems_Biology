clear
if ~(exist('cStructures','var'))
    scpMultiDataLoad;
end
% Display summary of data collected per experiment
clc

%%%%%%%%Aggregate Clinical Data For Each Experiment%%%%%%%%%%
for i = 1:5 %For each of the four experiment
    mIdxClinical = cStructures{i}.findDataType('Clinical'); %%%Find Index of Clinical Data
    
    oExplorerAggregated = clsSKEDAnalysis(); %%Generate Empty Class

    %%%For E24, ME_istat only has 3 time points, thus it is removed. 
    if i == 3||i==1
        mIdxClinical = mIdxClinical(:,[1 2 4 5]);
    end
    
    for j = 1:size(cStructures{i}.Data,1) %%For Each Monkey in Each Experiment
        %%%aggregateTimeSeries is used to align different type series by
        %%%collection type and keep the intersection of collection dates. 
        %%%For example, clinical is collected on day 1 to 100, and FXGN whole
        %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
        %%%series are from day 1 2 5 8 19. 

        oExplorerAggregated.Data{j} = cStructures{i}.aggregateTimeSeries(j,mIdxClinical(j,:)); %Populate Class with aggregated data per monkey
    end
    
        %%%Filter out time series that contains missing data. 

        %%oExplorerAggregated = oExplorerAggregated.filterMissingData(); 

        %%%Filter out time series that do not appear for all the subjects. 

        %%%oExplorerAggregated = oExplorerAggregated.filterSharedVariables();
    
    cAggregatedStructures{i} = oExplorerAggregated; %%%Append Aggregate Data To Cell
end

cCommonVar = cAggregatedStructures{1}.Data{1}.DataPrimitive.VarNames; %%%Unique Variable Name for Monkey 1 in Exp 4

%%%% Find common varibale shared across all monkey in all experiments 
for i = 1:5 %all expriments
    for j = 1:size(cAggregatedStructures{i}.Data,2) %all monkey
        cCommonVar = intersect(cCommonVar,cAggregatedStructures{i}.Data{j}.DataPrimitive.VarNames);
    end
end
%%%%Define Variable of Interest
nIdx = strncmp(cCommonVar,'x_mpss',6);
cCommonVar = cCommonVar(nIdx == 0); 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sVar2 = 'x_parasites_';
mX = [];
mDerivX = [];
mNX = [];
mDerivNX = [];
mRef =[];
cSubjectName =[];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%Grab Time Series of Interest of the Variables of Interest 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c = 1;
for i = 1:5  %%For Each Experiment
    for j = 1:size(cAggregatedStructures{i}.Data,2) %For Each Monkey
        
        nId2 = strcmp(sVar2,cAggregatedStructures{i}.Data{j}.DataPrimitive.VarNames); %% Find row # of Var2 (Parasite)
        mParasites = cAggregatedStructures{i}.Data{j}.DataPrimitive.Table(nId2,:); %Store time series of Var2
        
        
        mDiffParasites = find(mParasites>0);
        nIdx = find(diff(find(mParasites>0)) > 10);
        if i ~= 1 
            if isempty(nIdx) == 0
                nDate = mDiffParasites(nIdx(1)); 
            else 
                nDate = 0;
            end
        else
            nDate = find(mParasites == max(mParasites));
        end
        
        [temp ia ib] = intersect(cAggregatedStructures{i}.Data{j}.DataPrimitive.VarNames,cCommonVar);
        mData = cAggregatedStructures{i}.Data{j}.DataPrimitive.Table(ia,:);
        %for k = 1:size(mData,1)
            %temp = isnan(mData(k,:)); 
            %mData(k,temp) = 0;
            %mData(k,:) = funHodrickPrescott(mData(k,:),10);
        %end
        mDiffData = diff(mData,[],2); 
        mDiffData = [zeros(size(mData,1),1) mDiffData];
        
        mNData =[];
        
        for k = 1:size(mData,1)
            mNData(k,:) = mData(k,:) - mData(k,1); 
        end
        
        mDiffNData = diff(mNData,[],2); 
        mDiffNData = [zeros(size(mNData,1),1) mDiffNData];
        %%%%%%%%%%%%%%%%%%%%%
        idx = find(mParasites); %%%Keep data where parasitemia > 0
        mData = mData(:,idx);
        mData = mData';
        mDiffData = mDiffData(:,idx);
        mDiffData = mDiffData'; 
        mNData = mNData(:,idx); 
        mNData = mNData'; 
        mDiffNData = mDiffNData(:,idx); 
        mDiffNData = mDiffNData';
        
        mSubjectID = {}; %%%Generate Subject Label
        if i == 1
            c = j+10;
        elseif i == 2
            c = j + 20;
        else
            c = j + 30; 
        end
        mSubjectID(1:size(mData,1),1) = {[cAggregatedStructures{i}.Data{j}.ExperimentID '-' cAggregatedStructures{i}.Data{j}.Name]};

        mExpID = []; %%%%
        mExpID(1:size(mData,1),1) = i;
        mInfectionID = []; %%%%
        if nDate == 0
            mInfectionID(1:size(mData,1),1) = 1;
        else
            mInfectionID(idx <= nDate(1),1) = 1;
            mInfectionID(idx > nDate(1),1) = 2;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        tempRef = [mExpID mInfectionID];
        cSubjectName = [cSubjectName;mSubjectID];
        mRef = [mRef;tempRef];
        mX= [mX;mData]; %%%Append Var2
        mDerivX = [mDerivX;mDiffData];
        mNX = [mNX;mNData];
        mDerivNX = [mDerivNX;mDiffNData];
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cExpNameList = {'E03','E04','E23','E24','E25'};
cInfection = {'Primary','Non-Primary'};

for i = 1:5
    cExpName(find(mRef(:,1)==i)) = cExpNameList(i);
end

for i = 1:2 
    cInfectionCondition(find(mRef(:,2)==i)) = cInfection(i);
end


%%%%%%%%%%%%%MANOVA on RAW Data %%%%%%%%%%%%%%%
idx = find(sum(isnan(mX))<100);
tempData = mX(:,idx); 
idx = sum(isnan(tempData),2);
Data = tempData(idx == 0,:);
cLabel1 = cExpName(idx == 0);
cLabel2 = cInfectionCondition(idx == 0);
cLabel3 = cSubjectName(idx ==0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[d,p,stats] = manova1(Data,cLabel1);
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
h = figure('units','normalized','outerposition',[0 0 1 1]);
gscatter(c2,c1,cLabel1',[],'oxs^*')
xlabel('Cannonical Variable 1');
ylabel('Cannonical Variable 2');
set(gca,'FontSize',22,'FontWeight','bold');
funPrintImage(h,'../figure/MultiExpCannonicalVariableRawDataExperimentGroup');

[d,p,stats] = manova1(Data,cLabel2);
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
h = figure('units','normalized','outerposition',[0 0 1 1]);
gscatter(c2,c1,cLabel2','br','ox')
xlabel('Cannonical Variable 1');
ylabel('Cannonical Variable 2');
set(gca,'FontSize',22,'FontWeight','bold');
funPrintImage(h,'../figure/MultiExpCannonicalVariableRawDataInfectionGroup');

[d,p,stats] = manova1(Data,cLabel3);
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
h = figure('units','normalized','outerposition',[0 0 1 1]);
gscatter(c2,c1,cLabel3)
xlabel('Cannonical Variable 1');
ylabel('Cannonical Variable 2');
set(gca,'FontSize',12,'FontWeight','bold');
funPrintImage(h,'../figure/MultiExpCannonicalVariableRawDataSubject');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%MANOVA on RAW DerivativeData %%%%%%%%%%%%%%%
idx = find(sum(isnan(mDerivX))<100);
tempData = mDerivX(:,idx); 
idx = sum(isnan(tempData),2);
Data = tempData(idx == 0,:);
cLabel1 = cExpName(idx == 0);
cLabel2 = cInfectionCondition(idx == 0);
cLabel3 = cSubjectName(idx ==0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[d,p,stats] = manova1(Data,cLabel1);
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
h = figure('units','normalized','outerposition',[0 0 1 1]);
gscatter(c2,c1,cLabel1',[],'oxs^*')
xlabel('Cannonical Variable 1');
ylabel('Cannonical Variable 2');
set(gca,'FontSize',22,'FontWeight','bold');
funPrintImage(h,'../figure/MultiExpCannonicalVariableDerivDataExperimentGroup');

[d,p,stats] = manova1(Data,cLabel2);
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
h = figure('units','normalized','outerposition',[0 0 1 1]);
gscatter(c2,c1,cLabel2','br','ox')
xlabel('Cannonical Variable 1');
ylabel('Cannonical Variable 2');
set(gca,'FontSize',22,'FontWeight','bold');
funPrintImage(h,'../figure/MultiExpCannonicalVariableDerivDataInfectionGroup');

[d,p,stats] = manova1(Data,cLabel3);
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
h = figure('units','normalized','outerposition',[0 0 1 1]);
gscatter(c2,c1,cLabel3)
xlabel('Cannonical Variable 1');
ylabel('Cannonical Variable 2');
set(gca,'FontSize',12,'FontWeight','bold');
funPrintImage(h,'../figure/MultiExpCannonicalVariableDerivDataSubject');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%MANOVA on Normalized Data %%%%%%%%%%%%%%%
idx = find(sum(isnan(mNX))<100);
tempData = mNX(:,idx); 
idx = sum(isnan(tempData),2);
Data = tempData(idx == 0,:);
cLabel1 = cExpName(idx == 0);
cLabel2 = cInfectionCondition(idx == 0);
cLabel3 = cSubjectName(idx ==0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[d,p,stats] = manova1(Data,cLabel1);
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
h = figure('units','normalized','outerposition',[0 0 1 1]);
gscatter(c2,c1,cLabel1',[],'oxs^*')
xlabel('Cannonical Variable 1');
ylabel('Cannonical Variable 2');
set(gca,'FontSize',22,'FontWeight','bold');
funPrintImage(h,'../figure/MultiExpCannonicalVariableNormalizeDataExperimentGroup');

[d,p,stats] = manova1(Data,cLabel2);
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
h = figure('units','normalized','outerposition',[0 0 1 1]);
gscatter(c2,c1,cLabel2','br','ox')
xlabel('Cannonical Variable 1');
ylabel('Cannonical Variable 2');
set(gca,'FontSize',22,'FontWeight','bold');
funPrintImage(h,'../figure/MultiExpCannonicalVariableNormalizeDataInfectionGroup');

[d,p,stats] = manova1(Data,cLabel3);
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
h = figure('units','normalized','outerposition',[0 0 1 1]);
gscatter(c2,c1,cLabel3)
xlabel('Cannonical Variable 1');
ylabel('Cannonical Variable 2');
set(gca,'FontSize',12,'FontWeight','bold');
funPrintImage(h,'../figure/MultiExpCannonicalVariableNormalizeDataSubject');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%MANOVA on Normalized DerivativeData %%%%%%%%%%%%%%%
idx = find(sum(isnan(mDerivNX))<100);
tempData = mDerivNX(:,idx); 
idx = sum(isnan(tempData),2);
Data = tempData(idx == 0,:);
cLabel1 = cExpName(idx == 0);
cLabel2 = cInfectionCondition(idx == 0);
cLabel3 = cSubjectName(idx ==0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[d,p,stats] = manova1(Data,cLabel1);
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
h = figure('units','normalized','outerposition',[0 0 1 1]);
gscatter(c2,c1,cLabel1',[],'oxs^*')
xlabel('Cannonical Variable 1');
ylabel('Cannonical Variable 2');
set(gca,'FontSize',22,'FontWeight','bold');
funPrintImage(h,'../figure/MultiExpCannonicalVariableDerivNormalizeDataExperimentGroup');

[d,p,stats] = manova1(Data,cLabel2);
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
h = figure('units','normalized','outerposition',[0 0 1 1]);
gscatter(c2,c1,cLabel2','br','ox')
xlabel('Cannonical Variable 1');
ylabel('Cannonical Variable 2');
set(gca,'FontSize',22,'FontWeight','bold');
funPrintImage(h,'../figure/MultiExpCannonicalVariableDerivNormalizeDataInfectionGroup');

[d,p,stats] = manova1(Data,cLabel3);
c1 = stats.canon(:,1);
c2 = stats.canon(:,2);
h = figure('units','normalized','outerposition',[0 0 1 1]);
gscatter(c2,c1,cLabel3)
xlabel('Cannonical Variable 1');
ylabel('Cannonical Variable 2');
set(gca,'FontSize',12,'FontWeight','bold');
funPrintImage(h,'../figure/MultiExpCannonicalVariableDerivNormalizeDataSubject');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%for i = 1:size(Data,2)
    %figure(i)
    %p = anova1(Data(:,i),Ref(:,2),'off');
    %boxplot(Data(:,i),Ref(:,2));
    %title([cName(i) 'p = ' num2str(p)]);
%end

%%%%%%%%%%%%%%%%%%%%%%%%%
idx = find(sum(isnan(mX))<100);
tempData = mX(:,idx); 
cName = cCommonVar(idx);
idx = sum(isnan(tempData),2);
Data = tempData(idx == 0,:);
cLabel1 = cExpName(idx == 0);
cLabel2 = cInfectionCondition(idx == 0);
cLabel3 = cSubjectName(idx ==0);

for i = 1:size(Data,2)
    
    [~,~,stats] = anovan(Data(:,i),{cLabel2,cLabel3},'model','interaction',...
        'varnames',{'Infection','Subject'},'display','off');
    h = figure('units','normalized','outerposition',[0 0 1 1]);
    results = multcompare(stats,'Dimension',[1,2]);
    title(cName{i});
end