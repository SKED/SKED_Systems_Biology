clear
addpath('../function'); 

load('E04WBClinImmuneDiffCorr.mat'); 
load('E04WBClinImmuneDiffCorr2.mat'); 
load('E04WBClinImmuneChiSQ.mat'); 
load('E04WBClinImmuneMPATS.mat'); 

sName = 'E04WBClinImmune'; 

P1 = strucChiSQ.Pval; 
P2 = strucDiffCorr.Pval;
%P3 = strucDiffCorr2.Pval; 
P4 = strucMPATS.Pvalue;

P1(P1 == 0) = 10^-22; 

P2(P2 == 0) = 10^-22; 
%P3(P3 == 0) = 10^-22; 

P4(P4 == 0) = 10^-22; 

mConsensus = -2*(log(P1)+log(P2) + log(P4));
mConsensus = chi2cdf(mConsensus,6,'upper');
[mConsensus] = mafdr(mConsensus,'BHFDR',1);

mScore = squareform(mConsensus);

temp = sum(mScore); 
[a b] = sort(temp,'ascend');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath('../function');
sExpName = 'E04'; %%%Experiment of Interest is E04 

sSevere = {'RMe','RFa'};
sMild = {'RIc','RSb'};
sFileName = [sExpName 'CompleteData.mat'];%%%%Define Save Destination and File Name 
cDataTypes = {'Clinical','Immunology','Functional Genomics','Lipidomics'}; 
    load(sFileName) 
mIdxImmune = oExplorer.findDataType('Immunology');  %%%%find the index of Immunology data within oExplorer
mIdxClinical = oExplorer.findDataType('Clinical');  %%%%find the index of Clinical data within oExplorer
%oExplorer = setMeanVarTimeSeries(oExplorer,[]); 

oExplorerAggregated = MDBAnalysis(); 

for i = 1:4 
    %%%aggregateTimeSeries is used to align different type series by
    %%%collection type and keep the intersection of collection dates. 
    %%%For example, clinical is collected on day 1 to 100, and FXGN whole
    %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
    %%%series are from day 1 2 5 8 19. 
    
    oExplorerAggregated.Data{i} = oExplorer.aggregateTimeSeries(i,[mIdxClinical(i,:) mIdxImmune(i,:)]);
end

%%%Filter out time series that contains missing data. 

oExplorerAggregated = oExplorerAggregated.filterMissingData(); 

%%%Filter out time series that do not appear for all the subjects. 

oExplorerAggregated = oExplorerAggregated.filterSharedVariables();

cLabels = {'RMe','RFa','RIc','RSb'};

[num text raw] = xlsread('cell_cytokine.xlsx');

cFolderName = {'eDiNAE04PreSelectedSevere/','eDiNAE04PreSelectedMild/'};

cNames = {}; 
for k = 1:2
    temp = {};
    for j = 1:size(text,1)
        if isempty(text{j,k}) == 0
            temp = vertcat(temp,text{j,k});
        end
    end
    cNames{k} = temp;
end

for k = 1:2
    for n = 1:length(cNames{k})
        tThreshold = 0.005;
        sName = cNames{k}{n};
        idx = find(strcmp(sName,strucMPATS.Names));
        if isempty(idx) == 0
            tempNames = strucMPATS.Names(find(mScore(idx,:) <= tThreshold));
            qVal = mScore(idx,:)<= tThreshold;
            qVal = mScore(idx,qVal);
            x1 = []; 
            x2 = [];
            for i = 1:2 
                xID = find(strcmp(sName,oExplorerAggregated.Data{i}.DataPrimitive.VarNames));
                x1 = [x1 oExplorerAggregated.Data{i}.DataPrimitive.Table(xID,:)];
            end

            for i = 3:4 
                xID = find(strcmp(sName,oExplorerAggregated.Data{i}.DataPrimitive.VarNames));
                x2 = [x2 oExplorerAggregated.Data{i}.DataPrimitive.Table(xID,:)];
            end

            for i = 1:length(find(mScore(idx,:) <= tThreshold))
                y1 = [];
                y2 = [];
                for j = 1:2 
                    xID = find(strcmp(tempNames{i},oExplorerAggregated.Data{j}.DataPrimitive.VarNames));
                    y1 = [y1 oExplorerAggregated.Data{j}.DataPrimitive.Table(xID,:)];
                end

                for j = 3:4 
                    xID = find(strcmp(tempNames{i},oExplorerAggregated.Data{j}.DataPrimitive.VarNames));
                    y2 = [y2 oExplorerAggregated.Data{j}.DataPrimitive.Table(xID,:)];
                end

                h = figure('units','normalized','outerposition',[0 0 1 1])
                subplot(4,4,[1 2 5 6])
                plot(log(x1(1:7)),log(y1(1:7)),'ro','MarkerSize',15)
                hold on 
                plot(log(x1(7:end)),log(y1(7:end)),'rx','MarkerSize',15)

                xlabel(sName);
                ylabel(tempNames{i});
                title('Severe'); 
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                subplot(4,4,[3 4 7 8])
                plot(log(x2(1:7)),log(y2(1:7)),'bo','MarkerSize',15)
                hold on 
                plot(log(x2(7:end)),log(y2(7:end)),'bx','MarkerSize',15)
                xlabel(sName);
                ylabel(tempNames{i});
                title('Mild'); 
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                subplot(4,4,[13:16])
                plot(log(x1(1:7)),'ro-','linewidth',2,'MarkerSize',15);
                hold on 
                plot(log(x1(8:end)),'ro-','linewidth',2,'MarkerSize',15);
                plot(log(x2(8:end)),'bx-','linewidth',2,'MarkerSize',15);
                plot(log(x2(1:7)),'bx-','linewidth',2,'MarkerSize',15);
                title(sName)

                subplot(4,4,[9:12])

                plot(log(y1(1:7)),'ro-','linewidth',2,'MarkerSize',15);
                hold on 
                plot(log(y1(8:end)),'ro-','linewidth',2,'MarkerSize',15);
                plot(log(y2(8:end)),'bx-','linewidth',2,'MarkerSize',15);
                plot(log(y2(1:7)),'bx-','linewidth',2,'MarkerSize',15);
                title([tempNames{i} num2str(qVal(i))])
                sFileName = [cFolderName{k} sName '_' tempNames{i}];
                funPrintImage(h,sFileName);
                close all
            end
        end
    end
end