%%%%%%%%%%Load Data%%%%%%%%%%%%%%%%%%%%%%
addpath(['..' filesep 'Data']);
addpath(['..' filesep 'SKEDClass']);
addpath(['..' filesep 'SKEDFunction']);

scpMultiDataAcquisition;
%%%%Experiment Name For Later References
cExperiments = {'E03','E04R','E23R','E24','E25'}; 
%%%%
load('E03CompleteData.mat'); 
strucE03 = oExplorer; 
cStructures{1} = strucE03; 

%%%%Severe/Mild Name of Monkey in E04 For Future References
sSevere = {'RMe','RFa'};
sMild = {'RIc','RSb'};
sName = [sSevere,sMild];

%%%%Load E04 Data%%%%%%
load('E04CompleteData.mat'); 
strucE04 = oExplorer; 

%%%Filter Out Un-related Monkeys
for i = 1:4 
    for j = 1:size(strucE04.Data,1)
        if isempty(strucE04.Data{j,1}) == 0
            if strcmp(sName{i},strucE04.Data{j,1}.Name(1:end-2))
                mId(i) = j;
            end
        end
    end
end

strucE04.Data = strucE04.Data(mId,:); 

%%%%%%%%%%%%Append E04 Data to cStructures
cStructures{2} = strucE04; 

load('E23CompleteData.mat');  %%%Load Data
strucE23 = oExplorer; 

cStructures{3} = strucE23; %%%Append

load('E24CompleteData.mat');  %%%Load Data
strucE24 = oExplorer;

cStructures{4} = strucE24; %%%Append

load('E25CompleteData.mat'); %%%Load Data 
strucE25 = oExplorer;

cStructures{5} = strucE25; %%%Append

clearvars -except cStructures
%%%Print Figure%%%%
%funPrintImage(h,'../Figure/MultiExperimentParasitesRBCDistribution');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


