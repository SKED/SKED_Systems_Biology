classdef clsSKEDSubject < clsSKEDBase 
    %------------------------------
    %clsSKEDSubject class object
    
    properties (SetAccess = public)
        ExperimentID %ID of the experiment where the subject belong
        Data % container having a hash and a Data object
        DataTypes %Data Types collected for the subject, 'FXGN' et al
    end
    %------------------------------
    methods
        % Constructor class
        %Function: clsSKEDSubject(nSubjectID,oExp); 
        %Purpose: Construct clsSKEDSubject Class. 
        %Input: Numerical - Subject ID, clsSKEDExperiment Object. 
        %Output: clsSKEDSubject Object. 
        %Example: oExperiment = clsSKEDSubject(1,oExp); 
        
        function o = clsSKEDSubject(nSubjectID,oExp)
            %o.DBConnection = oExp.DBConnection;
            o.ID = nSubjectID; %Assing Data to Field 
            o.ExperimentID = oExp.ID; %Assign Experiment ID to Field 
            cData = getSubject(o,nSubjectID); %Query Data Base To Get Subject Data
            o.Name = cData.NAME{1}; %Assign Subject Name to Field
            o.ExternalID = cData.NAME{1}; %Assign External ID to Field 
            o = getMetadata(o); %Get All Subject Specific Metadata 
            if strcmp(o.DataTypes{1},'No Data') == 0
                o.Data = containers.Map;  %Generate Container Map
                for i = 1:size(o.DataTypes,1)
                    %Each Entry In Container Map is populated with a clsSKEDData
                    %object for a specific data type. 
                    o.Data(o.DataTypes{i}) = clsSKEDData(o,o.DataTypes(i,:)); 
                end
            end
        end
        %------------------------------
        % Load metadata as an independent task
        
    end
    
    methods(Access = private)
        
        %%% Get Meta Data of Subject 
        
        function o = getMetadata(o) 
           sSQL = ['begin sked.get_protocols(subject_id =>',num2str(o.ID),...
                ',experiment_id => ''',o.ExperimentID,''');end;'];
           temp = getRecordset(o,sSQL); %fetch(getDBConnection(o),sSQL);
           if strcmp(temp,'No Data') == 0
               o.Metadata = temp; 
               o.DataTypes = unique(temp.DATA_TYPE);
           else
               o.Metadata = {'No Data'};
               o.DataTypes = {'No Data'};
           end
        end
       
        %Get Description of Subject 
        
        function cData = getSubject(o,nSubjectID)
            sSQL = ['begin sked.get_subjects(subject_id =>',num2str(nSubjectID),...
                ',experiment_id => ''',o.ExperimentID,''');end;'];
            cData = getRecordset(o,sSQL); %fetch(getDBConnection(o),sSQL);
        end
        
        %Get Data Type of Subject 
        %function cData = getDataTypes(o)
           %sSQL = ['begin mahpicdb2.protocols((subject_id =>',num2str(o.ID),...
                %',experiment_id => ''',o.ExperimentID,''');end;'];
           %temp = getRecordset(o,sSQL); %fetch(getDBConnection(o),sSQL);
           %cData = unique(temp(:,3));
        %end
    end
end
      