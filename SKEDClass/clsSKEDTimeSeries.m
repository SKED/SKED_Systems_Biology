classdef clsSKEDTimeSeries
    %%%% Object With 4 Fields 
    properties (SetAccess = public)
        Type %%Data Primitive Type e.g. Time Series
        Time %%Column Label, Each Column represent a distinct Time
        VarNames  %Row Lable, Each Row Represent a Variable 
        Table %nxm Matrix, n Rows corresponds with Row Name, m Column Corresponds with m Time Points
    end
     
     methods
         %%%Purpose: MDBTimeSeries object Constructor 
         %%%Input: Table (nxm Matrix), Time and Variable Name
         %%%Output: MDBTimeSeries Object
         %%%Example: o = MDBTimeSeries(Time,VarName,Table); 
         
         function o = clsSKEDTimeSeries(Time,VarNames,Table)
             %o.DBConnection = oSubject.DBConnection;
             o.Type = 'TimeSeries'; %Assign input to field 
             o.Time = Time; %Assign input to field 
             o.VarNames = VarNames;%Assign input to field 
             o.Table = Table; %Assign input to field 

         end
     end
end