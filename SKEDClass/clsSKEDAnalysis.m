classdef clsSKEDAnalysis
    properties
        Data % Cell array of tables where each element maps MBDData.Data
    end
    
    methods
        %Graphical Output 
        %Function: plotTimeSeries(o,cGroups,cVariables); 
        %Purpose: Produce Graphics of Time Series 
        %Input: cGroups, cell indicating which data object to group by,
        %cVariables, cell of names of variables. 
        %Output: Figure. 
        %Example: oSKEDAnalysis.plotTimeSeries({[1 2 3],[4 5 6]},'IL5'); 
        
        function out = plotTimeSeries(o,cGroups,cVariables)
            numVar = length(cVariables); 
            for i = 1:length(o.Data)
                mIDs(i) = o.Data{i}.ID; 
            end 
            figure(1);
            for i = 1:length(cVariables) 
                
                subplot(1,numVar,i) 
                hold on 
                for j = 1:length(cGroups) 
    
                    for k = 1:length(cGroups{j})
                        x = [];
                        y = [];
                        idx = find(mIDs == cGroups{j}(k));
                        
                        idx2 = find(strcmp(o.Data{idx}.Table.RowNames,cVariables{i})); 
                        for m = 1:length(o.Data{idx}.Table.ColNames)
                            x = [x datenum(o.Data{idx}.Table.ColNames(m))];
                        end
                        y = table2array(o.Data{idx}.Table.Data(idx2,2:end));
                        [a b] = sort(x); 
                        
                        plot(x(b),y(b),'o-');
                    end
                end
                title(cVariables{i})
                hold off 
            end
        end
        
        %Filter Time Series based on Intensity
        %Function: filterTimeSeries(o,mIdx,fThreshold,nOccurence); 
        %Purpose: Filter out time series with value below fThereshold
        %Input: mIdx, indicating which data object to filter, fThreshold
        %the threshold, nOccurence, number of occurent of value below
        %threshold allowed 
        %Output: oSKEDAnalysis Object 
        %Example: oSKEDAnalysis.filterTimeSeries([1 2 3],5,0); 
        
        function o = filterTimeSeries(o,mIdx,fThreshold,nOccurence)
            if isempty(mIdx) == 1
                for i = 1:length(o.Data)
                    temp = sum(o.Data{i}.Table.Data <= fThreshold,2);
                    idx = temp <= nOccurence; 
                    o.Data{i}.DataPrimitive.Table = o.Data{i}.DataPrimitive.Table(idx,:);
                    o.Data{i}.DataPrimitive.VarNames = o.Data{i}.DataPrimitive.VarNames(idx,:);
                end
            else 
                for i = 1:size(mIdx,1)
                    for j = 1:size(mIdx,2)
                        temp = sum(o.Data{i,mIdx(i,j)}.DataPrimitive.Table <= fThreshold,2);
                        idx = temp <= nOccurence; 
                        o.Data{i,mIdx(i,j)}.DataPrimitive.Table = o.Data{i,mIdx(i,j)}.DataPrimitive.Table(idx,:);
                        o.Data{i,mIdx(i,j)}.DataPrimitive.VarNames = o.Data{i,mIdx(i,j)}.DataPrimitive.VarNames(idx);
                    end
                end
            end
        end
        
        %Filter Time Series based on missing data
        %Function: filterMissingData(o); 
        %Purpose: Filter out time series with missing data
        %Input: none
        %Output: oSKEDAnalysis Object 
        %Example: oSKEDAnalysis.filterMissingData; 
        
        function o = filterMissingData(o)

            for i = 1:length(o.Data) 
                temp = sum(isnan(o.Data{i}.DataPrimitive.Table),2); 
                ia = temp == 0;
                o.Data{i}.DataPrimitive.Table = o.Data{i}.DataPrimitive.Table(ia,:); 
                o.Data{i}.DataPrimitive.VarNames = o.Data{i}.DataPrimitive.VarNames(ia,:);
            end
        end
        
        %Standardize Time Series to have mean 0 variance 1
        %Function: setMeanVarTimeSeries(o,mIdx); 
        %Purpose: Standardize Time Series
        %Input: mIdx, index of data object
        %Output: oSKEDAnalysis Object 
        %Example: oSKEDAnalysis.setMeanVarTimeSeries([1 2]); 
        
        function o = setMeanVarTimeSeries(o,mIdx)
            if isempty(mIdx) == 1
                for i = 1:size(o.Data,1)
                    for k = 1:size(o.Data,2)
                        for j = 1:size(o.Data{i,k}.DataPrimitive.Table,1)
                            temp1 = mean(o.Data{i,k}.DataPrimitive.Table(j,:)); 
                            temp2 = std(o.Data{i,k}.DataPrimitive.Table(j,:));
                            o.Data{i,k}.DataPrimitive.Table(j,:) = o.Data{i,k}.DataPrimitive.Table(j,:) - temp1; 
                            o.Data{i,k}.DataPrimitive.Table(j,:) = o.Data{i,k}.DataPrimitive.Table(j,:)/temp2;
                        end
                    end
                end
            else
                for i = 1:size(mIdx,1)
                    for k = 1:size(mIdx,2)
                        for j = 1:size(o.Data{i,mIdx(i,k)}.DataPrimitive.Table,1)
                            temp1 = mean(o.Data{i,mIdx(i,k)}.DataPrimitive.Table(j,:)); 
                            temp2 = std(o.Data{i,mIdx(i,k)}.DataPrimitive.Table(j,:));
                            o.Data{i,mIdx(i,k)}.DataPrimitive.Table(j,:) = o.Data{i,mIdx(i,k)}.DataPrimitive.Table(j,:) - temp1; 
                            o.Data{i,mIdx(i,k)}.DataPrimitive.Table(j,:) = o.Data{i,mIdx(i,k)}.DataPrimitive.Table(j,:)/temp2;
                        end
                    end
                end
            end
            
        end
        
        %Find index of specific data type
        %Function: findDataType(o,sDataType); 
        %Purpose: Find index of spefcific data type
        %Input: sDataType, String. 
        %Output: oSKEDAnalysis Object 
        %Example: Idx = oSKEDAnalysis.findDataType('FXGN'); 
        
        function out = findDataType(o,sDataType)
            
            Idx = [];
            for j = 1:size(o.Data,1)
                for i = 1:length(o.Data)
                   if strcmp(o.Data{j,i}.DataType,sDataType) == 1
                       Idx(j,i) = i;
                   end
                end
            end
            
            for j = 1:size(o.Data,1)
                out(j,:) = find(Idx(j,:)>0);
            end
        end
        
        %Find index of specific data type
        %Function: findDataType(o,sDataType); 
        %Purpose: Find index of spefcific data type
        %Input: sDataType, String. 
        %Output: oSKEDAnalysis Object 
        %Example: Idx = oSKEDAnalysis.findDataType('FXGN'); 
        
        function [x,t,s] = retrieveProtocolData(o, nSubject, sProtocol)            
            for k = 1:size(o.Data,2)
                if strcmp(o.Data{nSubject,k}.Metadata.PROTOCOL, sProtocol)
                    x = o.Data{nSubject,k}.DataPrimitive.Table;
                    s = o.Data{nSubject,k}.DataPrimitive.VarNames';
                    t = o.Data{nSubject,k}.DataPrimitive.Time'; 
                end
            end
        end
        
        %Aggregate Multiple clsSKEDDataObject 
        %Function: aggregateTimeSeries(o,mIdx); 
        %Purpose: Aggregate Multiple clsSKEDDataObject by its shared time
        %points
        %Input: Idx of data object to aggregate
        %Output: oSKEDData Object 
        %Example: oSKEDAnalysis.Data{1} = oSKEDAnalysis.aggregateTimeSeries([1 2 3]); 
        
        function p = aggregateTimeSeries(o,nRow,mIdx)
            p = o.Data{nRow,mIdx(1)};
            p.DataType = {p.DataType};
            for i = 2:length(mIdx)
                p2 = o.Data{nRow,mIdx(i)}; 
                p.DataType = vertcat(p.DataType,{p2.DataType}); 
                p.DataAssociations = []; 
                p.Metadata = vertcat(p.Metadata,p2.Metadata); 
                T1 = p.DataPrimitive.Time;
                T2 = p2.DataPrimitive.Time; 
                [T ia ib] = intersect(T1,T2);  
                M1 = p.DataPrimitive.Table(:,ia);
                M2 = p2.DataPrimitive.Table(:,ib); 
                V1 = p.DataPrimitive.VarNames; 
                V2 = p2.DataPrimitive.VarNames; 
                p.DataPrimitive =  clsSKEDTimeSeries(T,horzcat(V1,V2),[M1;M2]);
            end
            p.DataPrimitive.VarNames = p.DataPrimitive.VarNames';
        end
        
        %Filter out variables that are not shared by all the data object
        %Function: filterSharedVariables(o)
        %Purpose:Filter out variables that are not shared by all the data object
        %Input: clsSKEDAnalysisObject
        %Output: clsSKEDAnalysisObject 
        %Example: clsSKEDAnalysis.filterSharedVariables(); 
        
        function o = filterSharedVariables(o)
            cVar = o.Data{1}.DataPrimitive.VarNames; 
            for i = 2:length(o.Data) 
                cVar = intersect(o.Data{i}.DataPrimitive.VarNames,cVar); 
            end 
            for i = 1:length(o.Data) 
                [temp ia ib] = intersect(o.Data{i}.DataPrimitive.VarNames,cVar); 
                o.Data{i}.DataPrimitive.Table = o.Data{i}.DataPrimitive.Table(ia,:); 
                o.Data{i}.DataPrimitive.VarNames = o.Data{i}.DataPrimitive.VarNames(ia,:);
            end
        end
        
        %Discretize Continuous Variable by pooling several clsSKEDDataObject
        %Together to Estimate A proper Gaussian Kernel Fit. 
        %Function: discretizeByGroup(o,cG,nMaxDiscrete)
        %Purpose:Discretize Continous Variable
        %Input: cG, Idx of clsSKEDDataClasses to be discretized, nMaxDiscrete
        %max number of discretization level
        %Output: clsSKEDAnalysisObject 
        %Example: clsSKEDAnalysis.discretizeByGroup([1 2 3],5); 
        
        function o = discretizeByGroup(o,cG,nMaxDiscrete)
            for l = 1:length(cG)
                mG1 = cG{l};
                M1 = []; 
                mLabel = [];
                for i = 1:length(mG1)

                    M1 = [M1 o.Data{mG1(i)}.DataPrimitive.Table];
                    
                    mLabel2 = [];
                    
                    mLabel2(1:size( o.Data{mG1(i)}.DataPrimitive.Table,2)) = mG1(i);

                    mLabel = [mLabel mLabel2];

                end

                %%%%%%%%%%%Discretization %%%%%%%%%%%%%%%
                n1 = size(M1,1); 

                display(['Discretizing Group ' num2str(l)]);
                M1n = clsSKEDAnalysis.EstimateN(M1,nMaxDiscrete);


                for i = 1:size(M1,1)
                    if M1n(i) == 1 
                        M1n(i) = 3;
                    end
                    temp = kmeans(M1(i,:)',M1n(i)); 
                    M1(i,:) = temp';
                end

                for i = 1:length(mG1)
                    o.Data{mG1(i)}.DataPrimitive.Table = M1(:,mLabel == mG1(i));
                end
            end 
        end
        
        %Conduct MPATS analysis
        %Function: MPATS(o,mG1,mG2,nEpsilon)
        %Purpose:Produce p value comparing l_1 distance between time series
        %Input: mG1: Index of group 1, mG2: Index of Group2, nEpsilon:
        %Parameter for MPATS, standardized data 500 is good. 
        %Output: strucMPATS 
        %Example: strucMPATS = clsSKEDAnalysis.MPATS([1 2 3],[4 5 6],500); 
        
        function strucMPATS = MPATS(o,mG1,mG2,nEpsilon)
            N1 = length(mG1); 
            N2 = length(mG2); 
            
            TemporalRelation = []; 
            G1 = {}; 
            for i = 1:N1 
                G1{i} = o.Data{mG1(i)}.DataPrimitive.Table;
            end
 
                TemporalRelation = [TemporalRelation;clsSKEDAnalysis.funPairZDist(G1,nEpsilon)];
            
                G1 ={};
            for i = 1:N2 
                G1{i} = o.Data{mG2(i)}.DataPrimitive.Table;
            end
                TemporalRelation = [TemporalRelation;clsSKEDAnalysis.funPairZDist(G1,nEpsilon)];
            

            Pvalue = zeros(1,size(TemporalRelation,2));
            TStats = zeros(1,size(TemporalRelation,2));
            for i = 1:size(TemporalRelation,2)
                
                t1 = TemporalRelation(1:N1,i); 
                t2 = TemporalRelation(N1+1:N1+N2,i); 
                [h p ci stats] = ttest2(t1,t2);
                Pvalue(i) = p; 
                TStats(i) = stats.tstat; 
            end
                                   
            strucMPATS.Pvalue = Pvalue;
            strucMPATS.TStats = TStats;
            strucMPATS.Names = o.Data{1}.DataPrimitive.VarNames;
            
        end
        
        %Conduct Calculate Difference in pairwise l_1 distance between 2
        %groups
        %Function: DiffL1(o,mG1,mG2)
        %Purpose:Produce p value comparing l_1 distance between time series
        %Input: mG1: Index of group 1, mG2: Index of Group2, 
        %Output: strucMPATS 
        %Example: strucMPATS = clsSKEDAnalysis.DiffL1([1 2 3],[4 5 6],500); 
        
        function strucMPATS = DiffL1(o,mG1,mG2)
            N1 = length(mG1); 
            N2 = length(mG2); 
            
            TemporalRelation = []; 
            for i = 1:N1 
                TemporalRelation = [TemporalRelation; pdist(o.Data{mG1(i)}.DataPrimitive.Table,'cityblock')];
            end
 
            
            for i = 1:N2 
                TemporalRelation = [TemporalRelation; pdist(o.Data{mG2(i)}.DataPrimitive.Table,'cityblock')];
            end
                               
            strucMPATS.AbsDiff = abs(TemporalRelation(1,:) - TemporalRelation(2,:));
            strucMPATS.Names = o.Data{1}.DataPrimitive.VarNames;
            
        end
        
        %Conduct Differential Correlation Analysis between two groups
        %Function: DiffCorr(o,mG1,mG2,sCorrType)
        %Purpose:Produce p value comparing correlation distance between time series
        %Input: mG1: Index of group 1, mG2: Index of Group2,sCorrType,
        %correlation type can be either speearman or pearson
        %Output: strucDiffCorr 
        %Example: strucDiffCorr = clsSKEDAnalysis.DiffCorr([1 2 3],[4 5 6],'spearman'); 
        
        function strucDiffCorr = DiffCorr(o,mG1,mG2,sCorrType) 
            M1 = o.Data{mG1(1)}.DataPrimitive.Table; 
            M2 = o.Data{mG2(1)}.DataPrimitive.Table;
            for i = 1:length(mG1)
                M1 = [M1 o.Data{mG1(i)}.DataPrimitive.Table];
            end
            for i = 1:length(mG2)
                M2 = [M2 o.Data{mG2(i)}.DataPrimitive.Table];
            end
            CorrM1 = corr(M1','type',sCorrType);
            j = size(CorrM1,1);
            CorrM1(1:j+1:j*j) = 0; 
            mDiffCorr(1,:) = squareform(CorrM1); 
            
            CorrM2 = corr(M2','type',sCorrType);
            j = size(CorrM2,1);
            CorrM2(1:j+1:j*j) = 0; 
            mDiffCorr(2,:) = squareform(CorrM2);
            
            zTransform = @(x) 0.5*(log((1+x)/(1-x)));
            mZ = arrayfun(@(x) zTransform(x),mDiffCorr);
            a = size(M1,2); 
            b = size(M2,2); 

            for i = 1:size(mDiffCorr,2)
                out(i) = (mZ(1,i) - mZ(2,i))/sqrt(1/(a-3)+1/(b-3));
                absDiff(i) = mDiffCorr(1,i) - mDiffCorr(2,i); 
            end

            pVal = 2*normcdf(-abs(out));
            strucDiffCorr.Pval = pVal; 
            strucDiffCorr.Names = o.Data{mG1(1)}.DataPrimitive.VarNames; 
            strucDiffCorr.absDiff = absDiff; 
            strucDiffCorr.zStats = out; 
            strucDiffCorr.Type = sCorrType; 
        end
        
        %Conduct Pearson's Test of Independence between two groups
        %Function: Chi2(o,mG1,mG2,sChi2Type)
        %Purpose:Produce p value comparing joint distribution between time series
        %Input: mG1: Index of group 1, mG2: Index of Group2,sChi2Type,
        %correlation type can be either pearson or funChi2
        %Output: strucChi2 
        %Example: strucDiffCorr = clsSKEDAnalysis.Chi2([1 2 3],[4 5 6],'pearson'); 
        
        function strucChi2 = Chi2(o,mG1,mG2,sChi2Type)
            
            M1 = []; 
            M2 = [];
            for i = 1:length(mG1)
                M1 = [M1 o.Data{mG1(i)}.DataPrimitive.Table];
            end
            for i = 1:length(mG2)
                M2 = [M2 o.Data{mG2(i)}.DataPrimitive.Table];
            end
            
                n1 = size(M1,1); 
                n2 = size(M2,1); 

            M1PVal = zeros(n1,n1);
            M2PVal = zeros(n2,n2);
            M1EffectSize = M1PVal; 
            M2EffectSize = M2PVal; 
            display('Start Analysis');
            switch sChi2Type 
                case 'funChiSQ'
                    display('Start G1 Analysis'); 
                     for i = 1:n1
                        display(['Entity ' num2str(i) '/' num2str(n1-1)]);
                        tic 
                        for j = 1:n1
                            if j~= i
                            %display(['Column ' num2str(j)]);
                            %tic;
                            temp1 = M1(i,:); 
                            temp2 = M1(j,:); 
                            cTable = clsSKEDAnalysis.ContigencyTable(temp1,temp2); 
                            %tic
                            [pval V] = clsSKEDAnalysis.funChi2(cTable); 
                            %toc
                            M1PVal(i,j) = pval; 
                            M1EffectSize(i,j) = V; 
                            %toc;
                            end
                        end
                        toc
                    end
 

                case 'Pearson'
                    for i = 1:(n1-1)
                        display(['Entity ' num2str(i) '/' num2str(n1-1)]);

                        for j = i+1:n1 
                            temp1 = [M1(i,:) M2(i,:)]; 
                            temp2 = [M1(j,:) M2(j,:)]; 
                            a = max(temp1); 
                            b = max(temp2); 
                            temp1 = M1(i,:); 
                            temp2 = M1(j,:);
                            cTemp1 = clsSKEDAnalysis.ContigencyTable(temp1,temp2,a,b); 
                            
                            temp1 = M2(i,:); 
                            temp2 = M2(j,:);
                            cTemp2 = clsSKEDAnalysis.ContigencyTable(temp1,temp2,a,b); 
                            cTable = [cTemp1(:) cTemp2(:)];
                            idx = sum(cTable,2) > 0;
                            cTable = cTable(idx,:);
                            [pval V] = clsSKEDAnalysis.funPearsonChi2(cTable); 
                            M1PVal(i,j) = pval; 
                            M1EffectSize(i,j) = V; 
                        end
                    end
                    M1PVal = M1PVal + M1PVal'; 
                    M1EffectSize = M1EffectSize + M1EffectSize';
                    
                otherwise
                    display('Wrong Input'); 
            end
            strucChi2.Pval = squareform(M1PVal); 
            strucChi2.EffectSize = squareform(M1EffectSize); 
            strucChi2.VarNames = o.Data{mG1(1)}.DataPrimitive.VarNames; 
            strucChi2.Type = sChi2Type; 
            
        end
    end
    
    methods(Static)
        
        %Conduct Estimate Optimal Discretization Number 
        %Function: EstimateN(M1,nMaxDiscrete)
        %Purpose:Estimate Optimal Discretization Number 
        %Input: M1: n X m matrix, each row is a variable, each column is a
        %observation. nMaxDiscrete: max discretization level 
        %Output: n 
        %Example: n = clsSKEDAnalysis.EstimateN([1 2 3;4 5 6],2); 
        
        function n = EstimateN(M1,nMaxDiscrete) 
                k = 1:nMaxDiscrete;
                nK = numel(k);
                RegularizationValue = 0.01;
                options = statset('MaxIter',10000);
                gm = cell(1,nK);
                aic = zeros(1,nK);
                bic = zeros(1,nK);

            n = [];
            for l = 1:size(M1,1) 
                X = M1(l,:); 
            
                % Preallocation

                % Fit all models

                        for i = 1:nK
                            gm{i} = fitgmdist(X',k(i),...
                                'RegularizationValue',RegularizationValue,...
                                'CovarianceType','full',...
                                'SharedCovariance',true,...
                                'Options',options);
                            aic(i) = gm{i}.AIC;
                            bic(i) = gm{i}.BIC;
                        end
                    


  
                nAIC = find(aic == min(aic));

                nBIC = find(bic == min(bic));

                n(l) = min([nAIC nBIC]); 
            end
            
        end
        
        %Conduct Pearson's test of indepence using Conigency Table
        %Function: funPearsonChi2(X)
        %Purpose:Conduct pearson's test of indpendence
        %Input: X: n X m matrix, each row is a variable, each column is a
        %condiiton. 
        %Output: pval, p Value and V, Chi2 statistics
        %Example: n = clsSKEDAnalysis.funPearsonChi2([1 2 3;4 5 6]); 
        
        function [pval V]= funPearsonChi2(X) 
            df = (size(X,1)-1)*(size(X,2)-1);
            if df == 0
                df = 1; 
            end
            Total = sum(sum(X));
            for j = 1:size(X,2)
                for i = 1:size(X,1)
                    temp = (sum(X(:,j))*sum(X(i,:)))/Total;
                    chi2 = (X(i,j) - temp)^2;
                    chi2 = chi2/temp;
                    X2(i,j) = chi2; 
                end
            end
            X2 = sum(sum(X2));
            pval = 1 - chi2cdf(X2,df);
            V = sqrt(X2/(Total*df));
        end
        
        %Generate Contigency Table 
        %Function: ContigencyTable(A,B,a,b)
        %Purpose:Generate Contigency Table 
        %Input: X: A,Observation of Variable 1, B, observation of variable
        %B, a max(A), b max(b). 
        %Output: ConTable axb contigency table
        %Example: ConTable = clsSKEDAnalysis.ContigencyTable([1 2 3],[4 5 6],3,6); 
        
        function ConTable = ContigencyTable(A,B,a,b)
             %a = unique(A); 
             %b = unique(B); 
             ConTable = zeros(a,b); 
             for i = 1:a
                  for j = 1:b
         
                          ConTable(i,j) = sum(A == i & B == j);                   
                  end
             end
        end
        
        %Conduct functionalChi2 using Conigency Table
        %Function: funChi2(X)
        %Pearson:Conduct functionalChi2 using Conigency Table
        %Input: X: n X m matrix, each row is a variable, each column is a
        %condiiton. 
        %Output: pval, p Value and X2, Chi2 statistics
        %Example: n = clsSKEDAnalysis.funChi2([1 2 3;4 5 6]); 
        
        function [pVal X2]= funChi2(X) 

            r = size(X,1); 
            s = size(X,2); 
            n = sum(sum(X));
            df = (r-1)*(s-1);
            tempSum = sum(X,2)/s;
             
            A = zeros(r,s);
            for i = 1:r
                for j = 1:s
                    A(i,j) = ((X(i,j) - tempSum(i))^2)/tempSum(i);
                end
            end
            
            A = sum(sum(A));
            B = zeros(1,s);
            tempSum = sum(X);
            holder = n/s; 
            
            for j = 1:s 
                 
                B(j) = ((tempSum(j) - holder)^2)/holder; 
            end
            B = sum(B); 

            X2 = A - B; 
            pVal = 1 - chi2cdf(X2,df);
            V = sqrt(X2/(n*df));
        end
        
        %Produce Z stats for MPATS analysis
        %Function: funPairZDist(X)
        %Pearson:Produce Z stats for MPATS analysis
        %Input: G1 cell of TimeSeriesMatrix, c a Constant
        %Output: out, linearized matrix contaning pairwise Z stat. 
        %Example: out = clsSKEDAnalysis.funPairZDist(G1,500); 
        
        function out = funPairZDist(G1,c)
            n = length(G1);
                numT = size(G1{1},2);
                out = []; 
                for j = 1:size(G1{1},1)
                    for i = 1:n
                        t1(i,:) = G1{i}(j,:);
                    end
                    for k = j+1:size(G1{1},1)

                        for i = 1:n
                            t2(i,:) = G1{i}(k,:);
                        end

                        temp = []; 

                        for m = 1:numT 
                            a = abs(t1(:,m) - t2(:,m) + c); 
                            b = abs(t2(:,m) - t1(:,m) + c); 
                            if mean(a) >= mean(b)
                                temp(:,m) = a; 
                            else
                                temp(:,m) = b; 
                            end
                        end

                        temp = sum(temp,2);
                        out =  [out temp];  
                    end
                end
        end
    end
end

