classdef clsSKEDTelemetry
    properties
        Experiment
    end    
    
    methods         
        function obj = clsSKEDTelemetry(s)   
            
        end
    
        function sConn = ConnString(obj)            
            javaaddpath('../Java/ojdbc7.jar'); 
           sConn = funCredentials();
        end
  
        function [MinDate, MaxDate, cProtocol_ids] = getSKEDTimeSeriesRange_Protocols(obj,sExperimentID,nSubjectID)
            % This function retrieves the max and min time for telemetry
            % data for a specified monkey in a specific experiment
            % and fetches all telemetry protocol_ids and puts them in a
            % comma-separated string cProtocol_ids
            sSQL = strcat('BEGIN sked.get_protocols(experiment_id => ''',sExperimentID,''', ',...
                          ' subject_id => ',num2str(nSubjectID),',' ,...
                          ' fields => ''protocol_app_id'', data_type => ''Telemetry''); END;');         
            cursor = exec(obj.ConnString,sSQL);
            cursor = fetchmulti(cursor);   
            cDataSKEDProtocolInfo = cursor.Data{2}; 
            
            protocol_ids = strjoin(string(table2cell(cDataSKEDProtocolInfo)), ',');
            cProtocol_ids = char(protocol_ids);
            FirstTelemetry_Protocol_id = cProtocol_ids(1:3);
            
            sSQL = strcat('BEGIN sked.get_time_series_range(protocol_app_id => ''', FirstTelemetry_Protocol_id,'''',...
                            ', subject_id => ',num2str(nSubjectID),'); END;');
            cursor = exec(obj.ConnString,sSQL);
            cursor = fetchmulti(cursor);   
            cDataSKED = cursor.Data{2};
            
            cDataSKEDCell = table2cell(cDataSKED);
            MinDate = cDataSKEDCell(1);
            MaxDate = cDataSKEDCell(2);
        end
        
        % Use sked.get_time_series to retrieve all telemetry raw data for a specified subject and time range      
        function cDataSKED = getSKEDResults(obj,nSubjectID,dtMinDate,dtMaxDate,cProtocol_ids)   
           sSQL = strcat('BEGIN sked.get_time_series(protocol_app_id => ''',cProtocol_ids,''', ',...
                      ' subject_id => ',num2str(nSubjectID),...
                      ' ,min_date =>  ''',dtMinDate,'''',...
                      ' ,max_date => ''',dtMaxDate,''');end;');
         
           cursor = exec(obj.ConnString,sSQL);
           cursor = fetchmulti(cursor);   
           cDataSKED = cursor.Data{2};

        end
        
        %Use sked.get_time_series to retrieve one type of telemetry data
        %for a specified subject and experiment
        function cDataSKED = getSKEDSingleResult(obj,nSubjectID,dtMinDate,dtMaxDate,cProtocol_id)   
           sSQL = strcat('BEGIN sked.get_time_series(protocol_app_id => ''',cProtocol_id,''', ',...
                      ' subject_id => ',num2str(nSubjectID),...
                      ' ,min_date =>  ''',dtMinDate,'''',...
                      ' ,max_date => ''',dtMaxDate,''');end;');
         
           cursor = exec(obj.ConnString,sSQL);
           cursor = fetchmulti(cursor);   
           cDataSKED = cursor.Data{2};
        end
    end
end
           



             